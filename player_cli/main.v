module main

import os
import flag
import time
import math
//import sokol.audio

import player as p
import common as c
import player_backend as pb

[inline]
fn sintone(periods int, frame int, num_frames int) f32 {
	//println("periods: $periods, frame: $frame, num_frames: $num_frames")
	return math.sinf(f32(periods) * (2 * math.pi) * f32(frame) / f32(num_frames))
}

/*
fn my_audio_stream_callback(buffer &f32, num_frames int, num_channels int) {
	println("callback: num_frames: $num_frames, num_channels: $num_channels")

	//ms := sw.elapsed().milliseconds() - sw_start_ms
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
			//idx := frame * num_channels + ch
			//idx := frame * num_channels
			idx := frame
			//for ch := 0; ch < num_channels; ch++ {
			//	idx := frame * num_channels + ch
				//if ms < 250 {
				//	soundbuffer[idx] = 0.5 * sintone(20, frame, num_frames)
				//} else if ms < 300 {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//} else if ms < 1500 {
				//	soundbuffer[idx] *= sintone(22, frame, num_frames)
				//} else {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//}
				
			//soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
			soundbuffer[idx] = 0.5 * sintone(100, frame, num_frames)
			//}
		}
	}
}
*/

fn main() {
	println("main()")

	mut fp := flag.new_flag_parser(os.args)
	fp.application("player_cli")

	fp.skip_executable()

	//f_res_path := fp.string("res_path", 0, default_res_path, "set custom res_path")
	f_out_file := fp.string("out", `o`, "/tmp/cli_out.raw", "set out file")
	f_help := fp.bool("help", `h`, false, "show help")
	f_render := fp.bool("render", `r`, false, "render to file")

	if f_help {
		println(fp.usage())
		exit(0)
	}

	additional_args := fp.finalize() or {
		println(err)
		println(fp.usage())
		return
	}
	_ = additional_args
				
	//println("f_out_file: $f_out_file")
	//println(additional_args.join_lines())

	if os.args.len < 2 {
		println("error: no file specified for playing")
		return
	}

	// XXX init
	mut ctx := p.new_context()

	mut l := p.new_module_loader(ctx)
	
	//m := l.load("testdata/files/new_file3/module.json")
	path := os.args.last()
	println("path: $path")
	m := l.load(path)
	_ = m

	/*
	mut s := p.new_module_saver(ctx)
	s.save(m, "/tmp/out")

	panic(2)
	*/

	// XXX TODO: move to player/util ?
	if true {
		println("player_cli: dumping instruments...") 
		for i, inst in m.instruments {
			println("-> $i")
			xpath := "/tmp/wt__player_cli_dump_inst_${i:02}.raw"
			inst.render_to_file(xpath, 44100*1)
		}
		
		//panic(2)
	}


	mut pl := p.new_player(mut ctx)
	pl.set_module(m)

	// XXX open audio
	if true && !f_render {
		println("player_cli: init audio backed...")
	
		//ok := pb.init_backend(mut pl)
		ok := pb.init_backend2(
			pb.BackendConfig{
				//num_frames: 1024,
				//num_frames: 2048,
				num_frames: 8192,
			})
		if !ok {
			panic("init backend error?")
		}
	}
	
	//{ panic(2) }

	pl.play()
	//pl.cb_done = true
	pl.cb_done = false

	println(m.patterns[0].tracks[0].cells[1].inst)

	if f_render {
		//panic("3")
		//pl.render_to_file("/tmp/cli_render_out.raw")
		pl.render_to_file(f_out_file)
	} else {

		buf_size := 1024
		_ = buf_size

		if false {
			//buf_size := 1024
			//buf_size := 2048
			//buf_size := 4096
			//out := pl.render_buf(buf_size)
			//_ = out
			//eprint(out)
			//panic("2")

			//time.wait(2000 * time.millisecond)
			//time.wait(5000 * time.millisecond)
			//time.sleep(5000 * time.millisecond)
			//audio.shutdown()

			///*
			//for pl.playing {
			for !pl.cb_done {
				// do some stuff here
				// XXX should it be called at 50hz or not? not tested
				time.sleep(16 * time.millisecond)
				//time.sleep(5000 * time.millisecond)
			}
			// Finally
			println("audio shutdown...")
			//audio.shutdown()
			pb.deinit_backend()
			//*/
		}

		// XXX push version ?
		if true {
			for pl.playing {
				println("player step")

				//num_frames := pb.get_buffer_frames2()
				num_frames := pb.expect2()

				println("num_frames: $num_frames")
				
				if num_frames > 0 {
			
					out, ret := pl.render_buf(num_frames)
					_ = ret

					pb.push_audio2(out, num_frames)
				
					// XXX should it be called at 50hz or not? not tested
					//time.sleep(16 * time.millisecond)
					//time.sleep(100 * time.millisecond)
				}

				//// XXX main loop delay function
				// XXX should it be called at 50hz or not? not tested
				//time.sleep(16 * time.millisecond) // 60 fps ?
				time.sleep(20 * time.millisecond) // 50 fps ?
			}
			//for i in 0..1000 {
			//	time.sleep(16 * time.millisecond)
			//}
		}

		if false {

			outf := "/tmp/r.raw"
			c.touch(outf)

			for pl.playing {
				// XXX should be same size / static size buffer
				//out := pl.render_buf(buf_size)
				out, _ := pl.render_buf(10000)
				_ = out

				c.dump_raw_append(outf, out)
				//panic("3")
			}
		}
		
	}

	println("done")
}
