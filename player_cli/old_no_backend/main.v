module main

import os
import flag
import time
import math
import sokol.audio

import player as p
import common as c

[inline]
fn sintone(periods int, frame int, num_frames int) f32 {
	//println("periods: $periods, frame: $frame, num_frames: $num_frames")
	return math.sinf(f32(periods) * (2 * math.pi) * f32(frame) / f32(num_frames))
}

/*
fn my_audio_stream_callback(buffer &f32, num_frames int, num_channels int) {
	println("callback: num_frames: $num_frames, num_channels: $num_channels")

	//ms := sw.elapsed().milliseconds() - sw_start_ms
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
			//idx := frame * num_channels + ch
			//idx := frame * num_channels
			idx := frame
			//for ch := 0; ch < num_channels; ch++ {
			//	idx := frame * num_channels + ch
				//if ms < 250 {
				//	soundbuffer[idx] = 0.5 * sintone(20, frame, num_frames)
				//} else if ms < 300 {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//} else if ms < 1500 {
				//	soundbuffer[idx] *= sintone(22, frame, num_frames)
				//} else {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//}
				
			//soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
			soundbuffer[idx] = 0.5 * sintone(100, frame, num_frames)
			//}
		}
	}
}
*/


// XXX TODO debug:
// XXX dump buffer with zero padded data at the end to disk
// XXX trim it to a specific size (of the rendered file) then compare file checksums
// XXX TODO make this a function for reuse in the player? like xmp_play_buffer(?)
//fn my_audio_stream_callback1(buffer &f32, num_frames int, num_channels int, user_data voidptr) {
fn my_audio_stream_callback1(buffer &f32, num_frames int, num_channels int, mut pl p.Player) {
	println("callback1: num_frames: $num_frames, num_channels: $num_channels")

	/*
	println("pl:")
	mut pl := &p.Player(user_data)
	*/
	println("pl.tick:")
	println(pl.tick)
	//panic("3")

	/*
	if !pl.playing {
		println("callback: not playing, exiting...")
		return
	}
	*/

	/*
	if pl.cb_done {
		println("cb callback: done, returning...")
		return
	}
	*/

	//println("pl.cb_done: $pl.cb_done")
	
	if pl.cb_done {
		println("cb callback: done, returning...")
		return
	}
	

	mut out := []f32{}
	mut ret := false
	_ = ret

	//if ret == false {
	if !pl.playing {
		if pl.render_buffer_rest.len != 0 {
			println("DERP")
			println("overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")
			println("len rest pl.render_buffer_rest: ${pl.render_buffer_rest.len}")

			n := pl.render_buffer_rest.len / num_frames
			println("n: $n")
			
			//out << pl.render_buffer_rest
			//pl.render_buffer_rest = []

			if n > 0 {
				// Feed some
				out << pl.render_buffer_rest[..num_frames]
				// ltrim
				pl.render_buffer_rest = pl.render_buffer_rest[num_frames..]
			} else {
				//out << pl.render_buffer__rest
				mut buf := pl.render_buffer_rest.clone()
				if buf.len < num_frames {
					// XXX pad buffer with zeroes... (?)
					diff := num_frames - buf.len
					println("diff: $diff")
					buf << p.silence(diff)
				}
				println("len buf: $buf.len")
				out << buf
				pl.render_buffer_rest = []
			}
			
			//panic("2")
		} else {
			println("HERP")
			//panic("2")
			out = p.silence(num_frames) // ?
			pl.cb_done = true
			//return
		}
	} else {

		//println("?0 pos pl.sample_pos: ${pl.sample_pos}")
		//println("?0 overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")

		out, ret = pl.render_buf(num_frames)

		//println("ret: $ret")
	}

	/*
	//if out.len == 0 {
	if !pl.playing {
		pl.cb_done = false
	}
	*/

	//println("x len out: ${out.len}")
	//panic("3")

	//println("pl.playing: ${pl.playing}")
	//println("pl.cb_done: ${pl.cb_done}")
	//println("?pos pl.sample: ${pl.sample_pos}")
	//println("?overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")
	

	///*
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
		
			idx := frame
			
			soundbuffer[idx] = out[idx]
		}
	}
	//*/

	// XXX not used
	pl.cb_pos += out.len
	//println("pl.cb_pos: $pl.cb_pos")

	//if !pl.playing {
	//		println("callback: not playing, exiting...")
	//		return
	//}
}



fn main() {
	println("main()")

	mut fp := flag.new_flag_parser(os.args)
	fp.application("player_cli")

	fp.skip_executable()

	//f_res_path := fp.string("res_path", 0, default_res_path, "set custom res_path")
	f_out_file := fp.string("out", `o`, "/tmp/cli_out.raw", "set out file")
	f_help := fp.bool("help", `h`, false, "show help")
	f_render := fp.bool("render", `r`, false, "render to file")

	if f_help {
		println(fp.usage())
		exit(0)
	}

	additional_args := fp.finalize() or {
		println(err)
		println(fp.usage())
		return
	}
	_ = additional_args
				
	//println("f_out_file: $f_out_file")
	//println(additional_args.join_lines())

	if os.args.len < 2 {
		println("error: no file specified for playing")
		return
	}

	// XXX init
	mut ctx := p.new_context()

	mut l := p.new_module_loader(ctx)
	
	//m := l.load("testdata/files/new_file3/module.json")
	path := os.args.last()
	println("path: $path")
	m := l.load(path)
	_ = m

	mut pl := p.new_player(mut ctx)
	pl.set_module(m)
	

	// XXX open audio
	if true && !f_render {
		audio.setup(
			num_channels: 1,
			 // XXX bad value: not PoT
			//buffer_frames: 6000,
			//buffer_frames: 4096,
			buffer_frames: 8192,
			//stream_cb: my_audio_stream_callback

			stream_userdata_cb: my_audio_stream_callback1,
			user_data: pl,
		)
	}

	pl.play()
	//pl.cb_done = true
	pl.cb_done = false

	println(m.patterns[0].tracks[0].cells[1].inst)

	if f_render {
		//panic("3")
		//pl.render_to_file("/tmp/cli_render_out.raw")
		pl.render_to_file(f_out_file)
	} else {

		buf_size := 1024
		_ = buf_size

		if true {
			//buf_size := 1024
			//buf_size := 2048
			//buf_size := 4096
			//out := pl.render_buf(buf_size)
			//_ = out
			//eprint(out)
			//panic("2")

			//time.wait(2000 * time.millisecond)
			//time.wait(5000 * time.millisecond)
			//time.sleep(5000 * time.millisecond)
			//audio.shutdown()

			///*
			//for pl.playing {
			for !pl.cb_done {
				// do some stuff here
				// XXX should it be called at 50hz or not? not tested
				time.sleep(16 * time.millisecond)
				//time.sleep(5000 * time.millisecond)
			}
			// Finally
			println("audio shutdown...")
			audio.shutdown()
			//*/
		}

		if false {

			outf := "/tmp/r.raw"
			c.touch(outf)

			for pl.playing {
				// XXX should be same size / static size buffer
				//out := pl.render_buf(buf_size)
				out, _ := pl.render_buf(10000)
				_ = out

				c.dump_raw_append(outf, out)
				//panic("3")
			}
		}
		
	}

	println("done")
}
