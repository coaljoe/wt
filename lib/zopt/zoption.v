module zopt

// XXX Temporary work around
pub struct ZOption<T> {
pub mut:
	// Option value
	val T
	// Value set
	val_set bool
}

/*
pub fn new_zoption<T>(obj T) ZOption<T> {
	opt := ZOption<T>{obj: obj}

	return opt
}
*/


/*
pub fn (mut opt ZOption<T>) test_zset() {
	opt.val_set = true
}
*/


/*
pub fn (opt &ZOption) str() string {
	return "ZOption<>"
}
*/
