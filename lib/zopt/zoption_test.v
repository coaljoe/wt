module zopt

pub struct Obj {
pub mut:
	x int
}

pub struct XTest {
pub mut:
	z ZOption<Obj>
}

fn test_option() {
	
	x := Obj{x: 1}
	//t := new_zoption<Obj>(x)

	t := ZOption<Obj>{val: x}
	//t := ZOption<Obj>{}

	mut x_test := &XTest{}

	//x_test.z.zset()
	//x_test.z.set = true
	//x_test.z.set = true
	x_test.z.val_set = true
	//x_test.z.val_set = false
	println(x_test.z.val_set)
	//x_test.z.zset<ZOption>()
	//x_test.z<ZOption>.zset()

	// Value get

	v := t.val
	println("v:")
	println(v)

	println(t)
	println(x_test)
}
