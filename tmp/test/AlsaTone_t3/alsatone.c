#include <alsa/asoundlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_LEN 48000
//#define BUFFER_LEN (48000 * 4)
//#define BUFFER_LEN 96000
#define DEFAULT_FREQ 600
#define DEFAULT_DURATION 3

const int c_sr = 48000;

float last_phase = 0.0;

//float* gen_sine(int inc, float freq, float freq_inc, int nsamples) {
void gen_sine(float inc_phase_pos, float f0, float f1, int nsamples, float *buf) {
	printf("gen_size: inc_phase_pos: %f, f0: %f, f1: %f, nsamples %d\n",
		inc_phase_pos, f0, f1, nsamples);

	// Phase position
	//float phase = phase_pos;
	float phase = last_phase;

	// Phase delta?
	//float phase_d = (float)freq / (float)c_sr;

	//float *buffer = malloc(nsamples);
	//float *buffer = calloc(1, nsamples);
	
	//float freq_inc = -0.1;
	//float freq_inc = -0.001;

	float freq = f0;

	float f_start = f0;
	float f_end = f1;
	int n_steps = nsamples;

	float interval = nsamples / c_sr;

	float phase_d = (float)freq / (float)c_sr;

	printf("interval: %f\n", interval);

	for (int i = 0; i < n_steps; ++i) {
        double delta = i / (float)n_steps;
        double t = interval * delta;
        double vz = 2 * M_PI * t * (f_start + (f_end - f_start) * delta / 2);
        while (vz > 2 * M_PI) vz -= 2 * M_PI; // optional
        //printf("%f %f %f", t, vz * 180 / M_PI, 3 * sin(vz));

		//float v = phase;
		//float v = sin(vz * 2*M_PI); // Creating the sinusoid
		float v = sin(vz);

        buf[i] = v;

        phase += phase_d;

        if (phase >= 1.0)
			phase -= 1.0;
    }

	/*
	
	for (int k=0 ; k<nsamples ; k++) {
		//freq += freq_inc;
		float phase_d = (float)freq / (float)c_sr;
		
		//printf("freq: %f\n", freq);
		//buffer[k] = sin(2*M_PI * freq / BUFFER_LEN * k); // Creating the sinusoid
		//buffer[k] = sin(2*M_PI * freq * (inc / c_sr)); // Creating the sinusoid
		//float d = (float)inc / (float)c_sr;
		float v = sin(phase * 2*M_PI); // Creating the sinusoid
		//printf("-> v %f\n", v);
		buf[k] = v;
		phase += phase_d;

		if (phase >= 1.0)
			phase -= 1.0;
	}
	*/

	last_phase = phase;

	//return buffer;
}

int main(int argc, char *argv[]) {
	const static char *device = "default";
	snd_output_t *output = NULL;
	float buffer[BUFFER_LEN];
	int err;
	int k;

	snd_pcm_t *handle;
	snd_pcm_sframes_t frames;

	if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		fprintf(stderr, "AlsaTone: Playback open error: %s\n", snd_strerror(err));
		return EXIT_FAILURE;
	}

	if ((err = snd_pcm_set_params(handle,
			SND_PCM_FORMAT_FLOAT,
			SND_PCM_ACCESS_RW_INTERLEAVED,
			1,
			BUFFER_LEN,
			1,
			500000)) < 0) {	 
		fprintf(stderr, "AlsaTone: Playback open error: %s\n", snd_strerror(err));
		return EXIT_FAILURE;
	}

	// SINE WAVE
	float freq = (argc > 1) ? strtol(argv[1], NULL, 10) : DEFAULT_FREQ;
	if (freq == 0) {
		fprintf(stderr, "AlsaTone: Invalid frequency.\n");
		return EXIT_FAILURE;
	}
	int duration = (argc > 2) ? strtol(argv[2], NULL, 10) : DEFAULT_DURATION;
	if (duration == 0) {
		fprintf(stderr, "AlsaTone: Invalid duration.\n");
		return EXIT_FAILURE;
	}
	printf("Sine tone at %fHz during %d seconds.\n", freq, duration);

	int mem_inc = 0;
	//int step_size = 1000;
	int step_size = 48000;
	float freq_inc = 0.0;
	//float freq_inc = 0.001;
	//float freq_inc = 0.1;
	int buffer_k = 0;

	float xfreq = freq;

	///*
	for (int i = 0; i < BUFFER_LEN / step_size; i++) {
		xfreq += 50;

		float b1[step_size];
		//gen_sine(mem_inc, freq, freq_inc, step_size, b1);
		//gen_sine(0.0, freq, freq_inc, step_size, b1);
		//gen_sine(0.0, xfreq, freq_inc, step_size, b1);
		//gen_sine(0.0, xfreq, freq, step_size, b1);
		//gen_sine(0.0, 500, 1000, step_size, b1);
		gen_sine(0.0, 100, 5000, step_size, b1);
		//gen_sine(0, freq, freq_inc, step_size, b1);
		mem_inc += step_size;

		//if (mem_inc > 360)
		//	mem_inc = -360;

		if (0) {
		for(int loop = 0; loop < step_size; loop++)
      		printf("%f ", b1[loop]);
      	printf("\n");
		}
		
		printf("buffer_k: %d\n", buffer_k);
		if (buffer_k > 48000 - 2) {
			//break;
		}
		for (int j = 0; j < step_size -2 ; j++) {
			buffer[buffer_k] = b1[j];
			buffer_k++;
		}

		//break;
	}
	
	
	for (k=0 ; k < duration ; k++)
		frames = snd_pcm_writei(handle, buffer, BUFFER_LEN); // Sending the sound

	snd_pcm_close(handle);

	printf("done\n");
	return EXIT_SUCCESS;
}
