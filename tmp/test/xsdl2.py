"""Simple example for using sdl2 directly."""
import struct
import os
import sys
import ctypes
import sdl2
import g2
import numpy as np

def gen_chunks(freq):

    chunk_length = 1024

    f = freq

    chunks = g2.generate_sine2(f, chunk_length)

    for i in range(0,4):
    #for i in range(0,24):
        chunks = np.concatenate((chunks, g2.generate_sine2(f, chunk_length)))
  
    return chunks
    
    
def dump_raw(file, data):
	#print("file:", file)
	for v in data:
		v_bin = struct.pack("<f", v)
		file.write(v_bin)

def run():
    sdl2.SDL_Init(sdl2.SDL_INIT_VIDEO)
    window = sdl2.SDL_CreateWindow(b"Hello World",
                                   sdl2.SDL_WINDOWPOS_CENTERED,
                                   sdl2.SDL_WINDOWPOS_CENTERED,
                                   592, 460, sdl2.SDL_WINDOW_SHOWN)
    fname = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                         "resources", "hello.bmp")
    image = sdl2.SDL_LoadBMP(fname.encode("utf-8"))
    windowsurface = sdl2.SDL_GetWindowSurface(window)
    sdl2.SDL_BlitSurface(image, None, windowsurface, None)
    sdl2.SDL_UpdateWindowSurface(window)
    sdl2.SDL_FreeSurface(image)
    
    freq = 80
    freq = 1000
    freq_step = 0.5
    #freq_step = 20
    out_f = open("/tmp/out_f.raw", mode="w+b")
    out_f2 = os.fdopen(sys.stdout.fileno(), "wb", closefd=False)

    running = True
    event = sdl2.SDL_Event()
    while running:
        data = gen_chunks(freq)
        #print("len data:", len(data))
        dump_raw(out_f, data)
        dump_raw(out_f2, data)
    
        while sdl2.SDL_PollEvent(ctypes.byref(event)) != 0:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
            
            if event.type == sdl2.SDL_KEYDOWN:
                if event.key.keysym.sym == sdl2.SDLK_UP:
                    freq += freq_step
                    print("new_freq:", freq, file=sys.stderr)
                elif event.key.keysym.sym == sdl2.SDLK_DOWN:
                    freq -= freq_step
                    print("new_freq:", freq, file=sys.stderr)
                    
            freq_s = str(freq)
            title = b"freq: " + bytes(freq_s, "utf8")
            sdl2.SDL_SetWindowTitle(window, title)
            #window.title = "freq: " + str(freq)
        #sdl2.SDL_Delay(10)
        sdl2.SDL_Delay(50)

    sdl2.SDL_DestroyWindow(window)
    sdl2.SDL_Quit()
    out_f.close()
    return 0


if __name__ == "__main__":
    sys.exit(run())
