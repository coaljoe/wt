import matplotlib.pyplot as plt
import numpy as np
import pyaudio
import math

#-----------------------
CHUNK = 1024
RATE =  44100
CHANNELS = 2
FORMAT = pyaudio.paFloat32
#-----------------------

samples = int(CHUNK)
t = np.arange(samples) / RATE
con = 0.0
phase_last = 0.0
n_last = 0.0

"""
# cont version
def gen_sine2(freq: float, t: float) -> float:
    a = 0.5
    sine = math.sin(2.0 * math.pi * freq * t + con)
    return a * sine
"""

# t = inc?
def gen_sine2(freq: float, phase_t: float) -> float:
    a = 0.5
    sine = math.sin(2.0 * math.pi * freq * phase_t)
    return a * sine

def gen_sine2a(freq: float, t: float) -> float:
    a = 0.5
    sine = math.sin(2.0 * math.pi * freq * t)
    return a * sine

def gen_sine2b(freq: float, inc: int) -> float:
    a = 0.5
    #sine = math.sin(2.0 * math.pi * freq / float(RATE) * inc)
    sine = math.sin(2.0 * math.pi * freq * (float(inc) / float(RATE)))
    return a * sine

def generate_sine2(freq: float, nsamples: int):

    global con
    global phase_last
    global n_last
    
    a = 0.5
    data = []
    
    phase = phase_last
    phase_d = math.pi*2*freq/float(RATE)
    inc = n_last
    
    for i in range(0, nsamples):
        #t = float(i) / RATE
                
        #"""
        sine = math.sin(phase)
        phase += phase_d
        
        if phase > math.pi:
            phase -= 2 * math.pi
        #"""
        
        #sine = gen_sine2b(freq, inc)
        #inc += 1

		# sine_gen
        #sine = a * math.sin(2.0 * math.pi * freq * t + con)
        #sine = gen_sine2(freq, t)
        #sine = gen_sine2(freq, t + con)
        #con = 2.0 * np.pi * f * CHUNK/RATE + con
        data.append(sine)
        
    phase_last = phase
    n_last = inc
    con = math.fmod(2.0 * np.pi * freq * CHUNK/RATE + con, 2.0 * np.pi)    
    
    #print("sine:", sine)
    #print("list sine:", list(sine))
    #print("con:", con)

    return data

def generate_sine(a: float = 0.5, freq: float = 440.0):

    global con

    if True:
        sine = a * np.sin(2.0 * np.pi * freq * t + con)
        #con = 2.0 * np.pi * f * CHUNK/RATE + con
        con = math.fmod(2.0 * np.pi * freq * CHUNK/RATE + con, 2.0 * np.pi)

    if False:    
        sine = a * np.sin(2.0 * np.pi * (freq * t + con))
        con = math.modf(f * CHUNK/RATE + con)[0]
    
    #print("sine:", sine)
    #print("list sine:", list(sine))
    #print("con:", con)

    # get the angle of the wave

    #phase = np.angle(np.fft.fft(sine))

    # update ref var to generate subsequent sines
    # begining where the last ended

    #con = phase[-1]

    return sine


def play_sine(data):

    pa = pyaudio.PyAudio()

    stream = pa.open(format=FORMAT,
                         channels=CHANNELS,
                         rate=RATE,
                         input=False,
                         output=True,
                         frames_per_buffer=CHUNK)

    stream.write(np.array(data).astype(np.float32).tostring())

    stream.close()

if __name__ == '__main__':

    #print("samples:", samples)
    #print("t:", t)
    #print("list t:", list(t))
    #exit()

    f = 80

    chunks = generate_sine(freq=f)

    for i in range(0,4):
    #for i in range(0,24):

        chunks = np.concatenate((chunks, generate_sine(freq=f)))

    #for i in range(0,10):

    play_sine(chunks)

    plt.plot(chunks)

    plt.show()
