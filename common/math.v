module common

[inline]
pub fn lerp(a f32, b f32, t f32) f32 {
	return (1.0-t)*a + t*b
}
