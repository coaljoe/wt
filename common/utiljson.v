module common

import x.json2

pub fn js_mustint(v json2.Any) int {
	//println("v: $v")
	//println("v:1 ${v.str()}")
	//z := v.int()
	//println("z: $z")
	
	//println(typeof(z))
	//println(typeof(z).name)

	// XXX bug?
	/*
	match v {
		int {
			return z
		}
		else {
			panic("js_mustint error v: $v")
		}
	}
	*/

	// XXX workaround?
	match v {
		json2.Null { // ??? not tested
			panic("js_mustint error v: $v")
		}
		else {
			//return z
			return v.int()
		}
	}
}

pub fn js_muststring(v json2.Any) string {
	z := v.str()

	match v {
		string {
			return z
		}
		else {
			panic("js_muststrng error v: $v")
		}
	}
}

pub fn js_mustbool(v json2.Any) bool {
	z := v.bool()

	// XXX fixme
	//println("-> ${typeof(v).name}")
	//println("-> ${v is bool}")
	//println("-> ${v is int}")
	//println("-> ${v is string}")

	match v {
		bool {
			return z
		}
		else {
			panic("js_mustbool error v: $v")
		}
	}
}

pub fn js_mustint_k(d map[string]json2.Any, k string) int {
	if !(k in d) {
		panic("no suck key k: $k")
	}

	return js_mustint(unsafe{d[k]})
}

pub fn js_muststring_k(d map[string]json2.Any, k string) string {
	if !(k in d) {
		panic("no suck key k: $k")
	}

	return js_muststring(unsafe{d[k]})
}

pub fn js_mustbool_k(d map[string]json2.Any, k string) bool {
	if !(k in d) {
		panic("no suck key k: $k")
	}

	return js_mustbool(unsafe{d[k]})
}

// k or default
pub fn js_int_k_or(d map[string]json2.Any, k string, default int) int {
	if !(k in d) {
		return default
	}

	return js_mustint(unsafe{d[k]})
}

// k or default
pub fn js_string_k_or(d map[string]json2.Any, k string, default string) string {
	if !(k in d) {
		return default
	}

	return js_muststring(unsafe{d[k]})
}

// k or default
pub fn js_bool_k_or(d map[string]json2.Any, k string, default bool) bool {
	if !(k in d) {
		return default
	}

	return js_mustbool(unsafe{d[k]})
}
