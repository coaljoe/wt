module common

//import encoding.binary
import os
//import math
//import strconv
//import regex

pub fn dump_raw(path string, data []f32) {
	data_len := data.len
	println("len_data: $data_len")
	println("dump_raw: path: ${path} len data: ${data.len}")
	

	mut f := os.create(path) or {
		println("cant create file: $path")
		return
	}

	//println("f: $f")

	//f.write("derp")
	//f.write("derp2")
	//f.close()
	//pp(2)

	
	//for v in data {
	//	f.write_bytes(v, 4)
	//}


	//println(data)
	//pp(2)

	write_raw_to_file(mut f, data)

	f.close()
}

pub fn dump_raw_append(path string, data []f32) {
	println("dump_raw_append: path: ${path} len data: ${data.len}")

	mut f := os.open_append(path) or {
		panic("cant open file: $path for append")
	}
	//mut f := os.open(path) or {
	//mut f := os.create(path) or {
	//	panic("cant create file: $path")
	//}

	
	for v in data {
		unsafe {
			f.write_ptr(v, 4)
		}
	}

	//f.write_string("test") or {
	//	panic(err)
	//}

	//println("f.tell: ${f.file_size()}")

	//os.flush_stdout()
	f.flush()	

	//write_raw_to_file(mut f, data)

	f.close()

	//f.flush()
	//os.flush_stdout()

	//os.write_file("/tmp/1.txt", "test") or {
	//	panic(err)
	//}

	//println(data)
	//panic("2")
}

pub fn dump_raw_append_file(mut f os.File, data []f32) {
	println("dump_raw_append_file: f: ${f} len data: ${data.len}")
	
	//for v in data {
	//	f.write_bytes(v, 4)
	//}

	//f.write("test")
	
	write_raw_to_file(mut f, data)

	//println(data)
	//pp(2)
}



pub fn write_raw_to_file(mut f os.File, data []f32) {
	//eprintln("dump_raw: path: ${path} len data: ${data.len}")
	println("write_raw_to_flie: len data: ${data.len}")
	//println("f: $f")

	//f.write("derp3")
	//pp(3)

	for v in data {
		unsafe {
			f.write_ptr(v, 4)
		}
	}

	//for i in 0..4 {
	//	println("-> ${data[i]}")
	//}
	//pp(5)

	/*

	for v in data {
		size := 4
		mut b := [byte(0)].repeat(size)
		//mut b := []byte
		//v := data[0]
		//ptr := &v
		//vu := u32(*ptr)
		//mut vb := [4]byte
		//vb = *vu
		vb2 := math.f32_bits(v)
		println("vb2: $vb2")
		binary.little_endian_put_u32(mut b, vb2)
		s := tos(&b[0], b.len)
		f.write(s)
		//f.close()

		f.write("derp4")

		println("s.len: $s.len")
		println("->")
		println("s0: 0x${s[0]:x}")
		println("s1: 0x${s[1]:x}")
		println("s2: 0x${s[2]:x}")
		println("s3: 0x${s[3]:x}")
		//strconv.v_printf("%x\n", s[0])
		pp(4)
		
		println("v: $v")
		println("b: $b, len b: $b.len")
		println("s: $s, len s: $s.len")
		pp(3)
	}
	*/
	//pp(2)

	/*
	for v in data {
		//f.write_bytes(v, 4)
		C.fwrite(v, 1, 4, f.cfile)
	}
	*/

	//f.close()
}

pub fn touch(path string) {
	println("touch path: $path")
	
	if os.exists(path) {
		os.rm(path) or {
			panic(err)
		}
	}

	os.create(path) or {
		panic(err)
	}
}

// Load and filter json data
pub fn load_json_file_string(path string) string {
	println("util: load_json_file_string: path $path")

	data := os.read_file(path) or {
		println("file read error:")
		panic(err)
		//return
	}

	//println("data: $data")
	println("len data: ${data.len}")
	//panic("2")

	mut safe := data

	println("len safe: ${safe.len}")
	//panic("2")

	if true {
		//safe = regex.replace_all_string(data, "\n")
	}

	// XXX fixme?
	if true {
		// XXX quick fix for new json parser
		// fixme?
		mut new_text := ""

		// Remove comments
		for s in safe.split_into_lines() {
			//mut x := s.clone()
			//mut x := s.trim_space()
			mut x := s.normalize_tabs(4)
			//println("-> s: $s")
			//println("-> x: $x")
			{
				//pos := x.index("//") or {
				//	return -1
				//}

				pos := x.index_after("//", 0)

				//println("pos: $pos")
				//	return -1
				 //or {
					//return s
					//return none
				//}
   				if pos >= 0 {
      				//return s.substr(0, pos)
      				x = x.substr(0, pos)

      				//println("x: $x")

      				//panic(3)
   				}
   			}

			// Not a whitespace string
			if x.trim_space().len != 0 {
   				new_text += x
   				new_text += "\n"
   			}

			/*
			//new_text += s.trim_right(" ")
			new_text += x.trim_right(" ")
			new_text += "\n"
			*/
		}

		{
		//panic(2)
		}

		safe = new_text

		println("")

		/*
		// Trim right spaces
		for s in safe.split_into_lines() {
			println(".")
			new_text += s.trim_right(" ")
			new_text += "\n"
		}

		safe = new_text
		*/
	}
		
	safe += "\n"
	println("len safe: ${safe.len}")

	//println("safe:")
	//println(safe)

	//panic(2)

	if true {
		mut f := os.create("/tmp/test") or {
			println("cant test create file")
			panic("2")
		}
		f.write_string(safe) or {
			panic("write error")
		}
		f.close()
	}

	//panic("2")

	return safe
}
