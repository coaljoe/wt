module main

import math
import nsauzede.vsdl2

fn main() {
    //SDL_Init(SDL_INIT_AUDIO)
    C.SDL_Init(C.SDL_INIT_AUDIO)
	C.atexit(C.SDL_Quit)


	c_sr := 44100

	/*
    // the representation of our audio device in SDL:
    C.SDL_AudioDeviceID audio_device

    // opening an audio device:
    SDL_AudioSpec audio_spec
    SDL_zero(audio_spec)
    audio_spec.freq = 44100
    audio_spec.format = AUDIO_S16SYS
    audio_spec.channels = 1
    audio_spec.samples = 1024
    audio_spec.callback = NULL
	*/

   // audio_device = C.SDL_OpenAudioDevice(
   //     NULL, 0, &audio_spec, NULL, 0)

	mut flag := u32(0)
	//for f in flags {
	//	flag = flag | u32(f)
	//}
	mut desired_spec := C.AudioSpec{
		freq: c_sr,
		format: 0,
		channels: 1,
		silence: 0,
		samples: 1000,
		size: 1000,
		//...spec
		//callback: play
		//userdata: device
	}
	mut obtained := C.AudioSpec{}

	/*
	ptr := C.SDL_OpenAudioDevice(device.name.str, 0, &desired_spec, &obtained, flag)
	if ptr == 0 {
		panic('Unable to open audio device')
	}
	*/

	//C.SDL_OpenAudio(&want, &have)
	C.SDL_OpenAudio(&desired_spec, &obtained)

   // printf("dev: %d\n", audio_device)

	//C.SDL_PauseAudioDevice(audio_device, 0)
	//C.SDL_PauseAudioDevice(ptr, 0)

    // pushing 3 seconds of samples to the audio buffer:
    mut x := f64(0)
    //for i := 0; i < audio_spec.freq * 3; i++ {
    for i := 0; i < c_sr * 3; i++ {
        x += .010

        // SDL_QueueAudio expects a signed 16-bit value
        // note: "5000" here is just gain so that we will hear something
        //int16_t sample = sin(x * 4) * 5000;
        //int16_t sample = sin(x * 16) * 10000;
        sample := math.sin(x * 16) * 5000

        sample_size := 2 * 1
        //C.SDL_QueueAudio(ptr, &sample, sample_size)
    }

    // unpausing the audio device (starts playing):
    //SDL_PauseAudioDevice(audio_device, 0);

    C.SDL_Delay(6000)

    //C.SDL_CloseAudioDevice(ptr)
    C.SDL_Quit()
}
