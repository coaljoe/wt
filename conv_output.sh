#!/bin/bash

FFMPEG="ffmpeg -v quiet"

$FFMPEG -y -f f32le -ac 1 -i "$*" /tmp/`basename $1`.wav
$FFMPEG -y -f f32le -ac 1 -i "$*" /tmp/`basename $1`.ogg
$FFMPEG -y -f f32le -ac 1 -i "$*" /tmp/`basename $1`.flac

#echo "done"
