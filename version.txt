future:
- effects


useful:
- load and save modules
- load and save instruments
- simple editor / gui


v0.0.1-alpha1:
- test microui
- test simple playback gui (read only)
- code cleanup
- repo cleanup/reorg

v0.0.1-alpha:
- [DONE] test module load/playback from file
- [DONE] play order
- [DONE] cli player/play.sh
- few test songs
- [DONE] simple square gen example instrument file
- multi-channel mixing

unsort:
- ? fix pitch
- settings in file
- test patters on different length
