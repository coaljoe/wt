#!/bin/bash

TMP_DIR=/tmp/audacity_temp

rm -rf "$TMP_DIR"
mkdir -p "$TMP_DIR"
ffmpeg -v quiet -y -f f32le -ac 1 -i "$*" $TMP_DIR/`basename $1`.wav && audacity $TMP_DIR/`basename $1`.wav > /dev/null 2>&1 &
