module main

//import strconv
//import p.debug { pp }
//import wt.debug { pp }
//import debug { pp }

//import math
//import util
import player as p
import common as c


const (
	c_sr = 44100
)

fn main() {
	println('main()')

	//ctx := p.Context{}
	mut ctx := p.new_context()

	//p.init_context()


	println(p.g_notes)
	/*
	println(p.get_note("C-4").freq)
	c.pp(2)
	*/

	//s := sine_wave(440, 1.0, 44100)
	//s := p.square_wave(440, 1.0, 44100)
	s := p.square_wave(6.48914, 1.0, 44100)
	//println("len s: ${s.len}")
	//println("s: ${s}")

	c.dump_raw("/tmp/out.raw", s)

	//c.pp(2)

	println("t1")

	mut m := p.new_module(mut ctx, 4)

	println("bpm: {m.bpm()}")
	println("tick_size: {m.tick_size()}")
	println("tick_time_sec: {m.tick_time_sec()}")
	//panic("2")

	// Add pattern
	mut pt := m.add_new_pattern()


	mut t := pt.tracks[0] 
	t.cells[0].note = p.get_note_opt("C-4")
	t.cells[2].note = p.get_note_opt("C-5")
	t.cells[4].note = p.get_note_opt("C-4")
	t.cells[0].inst = 1
	t.cells[2].inst = 1
	t.cells[4].inst = 1

	//t.cells[0].note = p.get_note("C-4")
	t.cells[0].vol = 32

	//println(t.cells[0].has_note())
	//println(t.cells[1].has_note())
	//println(t.cells[2].has_note())
	//c.pp(2)

	//mut i := p.new_instrument()
	mut i := m.add_new_instrument()

	//m.remove_instrument_num(1)
	//panic("2")

	mut data := []p.EnvPoint{}
	for j in 0..8 {
		data << p.EnvPoint{j, p.c_max_vol / 2}
	}
	data = [
		//p.EnvPoint{0, 0}, // p0 : 0
		//p.EnvPoint{1, 5}, // p1 : 0 -> 1
		//p.EnvPoint{2, 10}, // p2 : 1 -> 2

		/*
		p.EnvPoint{0, 0}, // p0 : 0
		p.EnvPoint{1, 5}, // p1 : 0 -> 1
		p.EnvPoint{2, 10}, // p2 : 1 -> 2
		//p.EnvPoint{2, 5}, // p2 : 1 -> 2
		//p.EnvPoint{5, 5}, // p2 : 1 -> 2
		*/

		/*
		p.EnvPoint{0, 32}
		//p.EnvPoint{1000, 32}
		p.EnvPoint{100, 32}
		*/

		/*
		// Idle vol
		p.EnvPoint{0, 64}
		p.EnvPoint{10, 64}
		*/

		///*
		p.EnvPoint{0, 0}
		//p.EnvPoint{10, 64}
		p.EnvPoint{2, 64}
		//*/
	]

	println("t2")

	mut vol_env := i.env_vol
	// XXX
	vol_env.enable()
	vol_env.set_data(data)
	//panic("2")
	//i.envelopes["volume"].data = data
	println("data: {data}")
	println("vol_env.data: {i.env_vol.data()}")
	
	//println(vol_env.get_value_at_time(0))
	//println(vol_env.get_value_at_time(1))
	//println(vol_env.get_value_at_time(8))
	//println(vol_env.get_value_at_time(16))
	println(vol_env.get_value_at_time(16+8))
	//println(vol_env.get_value_at_time(32))
	//println(vol_env.get_value_at_time(31))

	//c.pp(2)

	//c.pp(9)
	//panic("9")

	
	data = []
	/*
	for j in 0..8 {
		data << f32(j) * 0.15
	}
	*/

	//data = [0., 0., 0., 0., 0., 0., 0., 0., 0.]
	//data = [0.5, 0.5, 1., 1., 1., 1., 1., 1., 1.]
	//data = [0.5, 1.0, 0.5, 1.0, 0.5, 1.0, 0.5, 1.0, 0.5]
	//data = [f32(0.5), 1.0, 0.5, 1.0, 0.5, 1.0, 0.5, 1.0, 0.5]
	//data = [f32(0.5), 1.0, 0.5, 1.0, 0.5, 1.0, 0.5, 1.0, 0.5]
	data = [
		/*
		p.EnvPoint{0, 0},
		p.EnvPoint{1, 0}
		*/

		/*
		p.EnvPoint{0, 32}, // Center
		//p.EnvPoint{0, 33},
		p.EnvPoint{1, 16},
		//p.EnvPoint{2, 0},
		*/

		/*
		p.EnvPoint{0, 32},
		//p.EnvPoint{0, 128},
		p.EnvPoint{100, 32+16},
		*/

		/*
		//p.EnvPoint{0, 0},
		p.EnvPoint{0, 32},
		p.EnvPoint{10, 32},
		p.EnvPoint{2756, 0},
		*/

		/*
		// y 2x XXX ??? fixme?
		// XXX this will multiply range (num points) or y resolution (to 128 points)
		p.EnvPoint{0, 64}, // 64 = center
		p.EnvPoint{10, 64},
		p.EnvPoint{2756, 0},
		*/

		/*
		p.EnvPoint{0, 0},
		p.EnvPoint{10, 0},
		*/

		/*
		// Sweep down
		p.EnvPoint{0, 32},
		p.EnvPoint{48, 0},
		*/
		
		/*
		// Sweep down2
		p.EnvPoint{0, 32},
		//p.EnvPoint{16, 8},
		p.EnvPoint{32, 24},
		p.EnvPoint{48, 16},
		*/
		
		/*
		// Sweep down3
		p.EnvPoint{0, 32},
		p.EnvPoint{10, 24},
		p.EnvPoint{48, 24},
		*/

		// Idle env.
		p.EnvPoint{0, 32},
		p.EnvPoint{10, 32},
	]
	
	mut pitch_env := i.env_pitch
	pitch_env.enable()
	pitch_env.set_data(data)
	//panic("2")
	pitch_env.d_name = "pitch_env"

	//pitch_env.data_range = p.c_env_default_data_range * 2
	println(pitch_env)
	//panic("2")

	//panic("2")

	println("t3")
	
	//i.envelopes["pitch"].data = data
	println("data: {data}")
	println("pitch_env data: {i.env_pitch.data()}")

	//panic("2")

	if true {
		mut g1 := i.add_new_gen()


		g1.typ = .square
		//g1.typ = .test_sine
		//g1.typ = .noise_square
		//g1.freq = 440  // A4
		// Default: C4: 261.62
		//g1.freq = 270

		// ^-- XXX?

		//println(g1.freq)
		//println(g1)
		//panic("2")


		// XXX note shift
		g1.base_note = p.get_note("C-5")

		//g1.freq = 1000
		println(g1.freq)


		println("g1.base_note: {g1.base_note}")
		println("g1.base_note.freq: {g1.base_note.freq}")
	}

	/*
	//data2 := i.gens[0].gen_render_wave(1.0, 44100)
	//data2 := i.gens[0].gen_render_wave(0.5, 44100)
	data2 := i.gens[0].gen_render_wave_freq(220, 44100)

	c.dump_raw("/tmp/dump.raw", data2)

	panic("2")
	*/

	println("t4")

	i.render_to_file("/tmp/inst_out.raw", 44100)

	/*
	
	// Inst player test
	mut q := p.new_instrument_player(i)
	ret := q.render_buf_cont(1.0, 44100)
	c.dump_raw("/tmp/q.raw", ret)
*/
///*
	mut q2 := p.new_instrument_player(i)
	ret2 := q2.render_buf_cont(1.0, 44100)
	//ret2 := q2.render_buf_cont(1.0, 45)
	c.dump_raw("/tmp/q2.raw", ret2)	
//*/
	//panic("2")

	mut q3 := p.new_instrument_player(i)

	//gen_player := q3.get_instrument_gen_player_for_gen_num(0)
	gen_player := q3.get_player_for_gen_num(0)

	println("q3 sine_t {gen_player.gen.sine_t}")

	//panic("3")
	
	println("s0:")
	ret3 := q3.render_buf_cont(1.0, 22050)
	//ret3 := q3.render_buf_cont(1.0, 22)
	c.dump_raw("/tmp/q3a.raw", ret3)
	//panic("3")
	println("q3 sine_t {gen_player.gen.sine_t}")
	//panic("3")


	//saved_sine_t := q3.gen_players[0].gen.sine_t
	//saved_sine_t := q3.gen_player.gen.sine_t
	saved_sine_t := gen_player.gen.sine_t
	//saved_sine_t := g3.gen_players[0].cont_gen_sine_t
	println("saved_sine_t: {saved_sine_t}")
	//panic("3")


	println("s1:")
	//q3.gen_players[0].cont_gen_sine_t = saved_sine_t
	//q3.gen_player.gen.sine_t = saved_sine_t
	ret4 := q3.render_buf_cont(1.0, 22050)
	//ret4 := q3.render_buf_cont(1.0, 23)
	c.dump_raw("/tmp/q3b.raw", ret4)
	//panic("3")

	//panic("2")

	println("t5")

	mut l := p.new_module_loader(ctx)

	mut pl := p.new_player(mut ctx)
	pl.set_module(m)

	m.orders << 0
	//m.orders << 0
	//m.orders << 1
	//m.orders << 1
	
	//pl.get_pattern_by_pos_idx(0)
	//pl.get_pattern_by_pos_idx(63)
	//pl.get_pattern_by_pos_idx(64)
	//pl.get_pattern_by_pos_idx(65)
	//panic(2)
	


	// XXX replace pattern
	//m.remove_pattern_num(0)
	//m.remove_pattern(pt)
	m.remove_pattern(pt)

	/*
	println("list:")
	for z, x in m.patterns {
		println("-> $z")
		println("x: ${x}")
		println(strconv.v_sprintf("xp: %p", x))
	}
	panic("2")
	*/

	//pt2 := l.load_pattern("testdata/pattern/test1.json")
	//pt2 := l.load_pattern("testdata/pattern/test1_ch_mem.json")
	//pt2 := l.load_pattern("testdata/pattern/test1_note_off.json")
	pt2 := l.load_pattern("testdata/pattern/test1_c1.json")
	//pt2 := l.load_pattern("testdata/pattern/test1_c2.json")
	//pt2 := l.load_pattern("testdata/pattern/test1_levels.json")

	/*
	println("note:")
	note := pt2.tracks[0].cells[2].note.val
	//note := pt2.tracks[0].cells[10].note.val
	println(note)
	//panic("2")
	*/

	m.add_pattern(pt2)

	// XXX update BPM
	m.set_bpm(170)
	
	new_inst := l.load_instrument("testdata/inst/test_inst4.json")
	//new_inst := l.load_instrument("testdata/inst/examples/simple_gen.json")
	m.replace_instrument(1, new_inst)

	mut new_inst2 := l.load_instrument("testdata/inst/test_inst4.json")
	//mut new_inst2 := l.load_instrument("testdata/inst/examples/simple_gen.json")
	new_inst2.base_note = p.get_note("C-5")
	//new_inst2.env_vol.disable()
	new_inst2.env_pitch.disable()
	m.set_instrument(2, new_inst2)

	pl.render_to_file("/tmp/render_out.raw")

	println("t6")

	//mut inst1 := l.load_instrument("testdata/inst/test_inst1.json")
	//mut inst1 := l.load_instrument("testdata/inst/test_inst2.json")
	//mut inst1 := l.load_instrument("testdata/inst/test_inst3.json")
	mut inst1 := l.load_instrument("testdata/inst/test_inst4.json")

	//println("inst1: $inst1")
	println("inst1.name: {inst1.name]")
	println("inst1.vol: {inst1.vol}")
	println("inst1.gens[0].base_note: {inst1.gens[0].base_note}")
	println("inst1.base_note: {inst1.base_note}")
	
	println("inst1.env_vol.enabled: {inst1.env_vol.enabled()}")
	println("inst1.env_pitch.enabled: {inst1.env_pitch.enabled()}")

	inst1.render_to_file("/tmp/z__inst1_out.raw", 44100)

	//pt1 := l.load_pattern("testdata/pattern/test1.json")
	//_ = pt1

	if false {
		/*
		mut ctx2 := p.new_context()

		mut l2 := p.new_module_loader(ctx2)
	
		m2 := l2.load("testdata/files/new_file3/module.json")
		_ = m2

		mut pl2 := p.new_player(mut ctx2)
		pl2.set_module(m2)

		println(m2.patterns[0].tracks[0].cells[1].inst)

		pl2.render_to_file("/tmp/m2.raw")
		*/
	}

	panic("2")

	if true {

		mut inst2 := m.add_new_instrument()

		gen1 := inst2.add_new_gen()
		_ = gen1

		// XXX
		//inst2.gens[0].freq = 440
		inst2.gens[0].freq = 100
		//inst2.gens[0].vol = pl.c_max_vol / 2
		//inst2.gens[0].vol = int(64.0 * 0.40)
		inst2.gens[0].set_vol_percent(40)

		///*
		gen2 := inst2.add_new_gen()
		_ = gen2
		//inst2.gens[1].freq = 48
		inst2.gens[1].freq = 96
		//inst2.gens[1].vol = int(64.0 * 0.25)
		inst2.gens[1].set_vol_percent(25)
		//*/

		inst2.render_to_file("/tmp/inst2_out.raw", 44100)

	}

	//println("done")
}
