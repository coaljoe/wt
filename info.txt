комманды:

волна = square wave
? noise wave?

1. G[k, *amp, *pitch] -> волна частоты ноты k, опционально амплитуды amp, опционально файнпитчем pitch
2. D[time] -> задержка (пустой блок) времени time
3. ? CV[vol] -> установить уровень звука канала в vol
	нужен для каналов с множеством генераторов, или автоматически усреднять амплитуду в
	зависимости от их количества?
4. ? GL -> глайд к следующей ноте (частоте следующей ноты) [режим глайда для канала/секции?]
5. ? FP[cent] -> fine pitch note
6. ? portamento?
7. ? break note (stop generators)
8. ? jump to 0

? параметры порты для блока (как в блендер нодах)

1. порты/слоты: freq, amp
2. параметры/ноды: LFO, value

параметры инструмента:

1. note length (0 - infinite)
2. ? loop
3. volume
4. hardware note delay?
5. NNA
6. ADSR/Envelope
7. ? low pass filter

фунцкии:

- можно запускать сколько угодно каналов комманд одновременно для генерации звука в одном инструменте
- простой adsr для каналов? или envelope?
- NNA для инструментов
- встроенный в комманды опциональный envelope для всего канала как в llms?
- возможность копировать настройки / наследовать другие каналы?
	пример: ch second: (copy #1)D[10ms]G[k-12]
- модули основанные на формулах и тегах (слотах) для LFO и параметров,
	как Formula Controller https://www.image-line.com/support/flstudio_online_manual/html/plugins/Fruity%20Formula%20Controller.htm
	инфо:
	https://www.taletn.com/reaper/mono_synth/
	https://www.kvraudio.com/product/wave-designer-by-dnr-collaborative
	FuncShaper http://www.rs-met.com/freebies.html
	https://www.wolframalpha.com/input/?i=play+%2816%2F15%29cos%280.5t%29-%281%2F15%29cos%282t%29
- синтезаторы основанные на формулах
- рендеринг примерных типов волн для инструментов (wave preview)

эффекты:

- ? low pass filter https://ccrma.stanford.edu/~jos/fp/Definition_Simplest_Low_Pass.html
	L - per channel
	G - ? global
- 

вопроcы:

- 8bit or 16bit mixing and data?
- должен ли канал рестартовать ноты/генераторы при проигрывании следующего блока?

идеи:

- built-in bfsound plugins / channels processing?
	commands:
		++              - next block
		--              - prev block
		sleep xx        - sleep xx time
		w id v idx      - write to block `id` value `v` at index `idx`
		w v idx         - write to current block value `v` at index `idx`
		r var v idx     - read block's value at index `idx` to `var`
		set name=value  - set variable name `name` to value `value`
		print var       - print variable `var`
		j               - jump to position
		exit            - exit

- blocks parameters/ports LFOs / OSCs?

- bfaudio based generators? similar to Arrow or paintfuck?
- WASM? javascript (duktape)?

- простой режим (micro) где только простые каналы и расширенный (extendend) для модулей.
  тип режима сохраняется в модуль.

- ? vol ramp famitracker.com/images/ftScrSequence2.png

	
