// Debug routines for working with images
module image

import sdl
import sdl.image as img
import os

[heap]
pub struct Context {
pub mut:
	window &sdl.Window
	screen &sdl.Surface
	renderer &sdl.Renderer
}

pub struct Image {
mut:
	// XXX do we need tex? remove?
	tex &sdl.Texture
	surf &sdl.Surface
pub:
	w int
	h int
module:
	// XXX per image?
	ctx &Context // XXX Link
}

fn init_ctx() &Context {
	println("image: init_ctx")

	// Init sdl
	println("init sdl...")

	w := 320
	h := 240
	title := "test"

	// XXX Sdl
	mut window := &sdl.Window(0)
	mut screen := &sdl.Surface(0)
	mut renderer := &sdl.Renderer(0)

	sdl.init(sdl.init_video)
	C.atexit(sdl.quit)
	//ttf.init()
	//C.atexit(ttf.quit)
	//C.TTF_Init()
	//C.atexit(C.TTF_Quit)
	bpp := 32
	_ = bpp
	//vsdl2.create_window_and_renderer(w, h, 0, &window, &renderer)
	//flags1 := u32(C.SDL_WINDOW_HIDDEN)
	//flags1 := u32(0)
	//flags1 := C.SDL_WINDOW_RESIZABLE
	//flags1 := u32(0)
	flags1 := u32(sdl.WindowFlags.resizable)
	
	//vsdl2.create_window_and_renderer(w, h, flags1, &window, &renderer)

	/*
	// XXX sdl c enums not working
	//ret := int(C.SDL_SetHint(C.SDL_HINT_FRAMEBUFFER_ACCELERATION, "X"))
	ret := C.SDL_SetHint(C.SDL_HINT_FRAMEBUFFER_ACCELERATION, "0")
	_ = ret
	C.SDL_SetHint(C.SDL_HINT_RENDER_DRIVER, "software")
	//if ret != C.SDL_TRUE {
	//if ret != 0 {
	//	s := cstring_to_vstring(C.SDL_GetError())
	//	println("s: $s")
	//	panic("set hint failed")
	//}
	*/

	/*
	ret2 := C.SDL_GetHint(C.SDL_HINT_FRAMEBUFFER_ACCELERATION)
	println("ret2: $ret2")
	if !isnil(ret2) {
		s := cstring_to_vstring(ret2)
		println("ret2 s: $s")
	}
	//panic("2")
	*/

	///*
	window = sdl.create_window(title.str, 0, 0, w, h, flags1)
	screen = sdl.get_window_surface(window)
	//screen := get_screen()
	renderer = sdl.create_software_renderer(screen)
	//*/

	if isnil(renderer) {
		panic("bad renderer ?")
	}
	
	//a.w = w
	//a.h = h
	//screen = vsdl2.create_rgb_surface(0, w, h, bpp, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)
	//a.texture = C.SDL_CreateTexture(sdl.renderer, C.SDL_PIXELFORMAT_ARGB8888, C.SDL_TEXTUREACCESS_STREAMING, w, h)

	//sdl.set_window_title(window, title.str)

	flags := int(img.InitFlags.png)
	imgres := img.init(flags)
	if (imgres & flags) != flags {
		panic("error initializing image library.")
	}

	ctx_inst := &Context{
		window: window,
		screen: screen,
		renderer: renderer,
	}

	println("done init sdl")

	return ctx_inst
}

/*
pub fn init() &Context {
	println("init")

	return init_ctx()
}
*/

/*
fn init() {
	println("init")

	init_ctx()
}
*/

pub fn get_ctx() &Context {
	println("image: get_ctx")

	return init_ctx()
}


fn resize_window(mut ctx Context, w int, h int) {
	println("image: resize_window w: $w, h: $h")

	///*

	// XXX?
	target := C.SDL_GetRenderTarget(ctx.renderer)
	C.SDL_SetRenderTarget(ctx.renderer, voidptr(0))
	C.SDL_SetWindowSize(ctx.window, w, h)
	C.SDL_SetRenderTarget(ctx.renderer, target)

	//println(ctx.screen)

	ctx.screen = C.SDL_GetWindowSurface(ctx.window)

	//println(&ctx.screen)

	//panic("2")

	//C.SDL_DestroyRenderer(ctx.renderer)

	// XXX
	//ctx.renderer = C.SDL_CreateSoftwareRenderer(ctx.screen)
	
	// XXX update ctx.screen surface
	//ctx.screen = C.SDL_GetWindowSurface(ctx.window)
	//ctx.ctx2.screen = C.SDL_GetWindowSurface(ctx.window)

	//new_screen := C.SDL_GetWindowSurface(ctx.window)
	//ctx.set_screen(new_screen)

	//*/
}


fn tex_to_surf(ctx &Context, tex &sdl.Texture) &sdl.Surface {
	println("image: tex_to_surf")

	format := C.SDL_PIXELFORMAT_ARGB8888
	//format := C.SDL_PIXELFORMAT_RGBA8888
	
	target := C.SDL_GetRenderTarget(ctx.renderer)
	C.SDL_SetRenderTarget(ctx.renderer, tex)
	w, h := get_sdl_tex_size(tex)
	//surf := C.SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0)
	surf := sdl.create_rgb_surface(0, w, h, 32, 0, 0, 0, 0)
	C.SDL_RenderReadPixels(ctx.renderer, voidptr(0), format, surf.pixels, surf.pitch)
	//IMG_SavePNG(surface, file_name);
	//SDL_FreeSurface(surface);
	C.SDL_SetRenderTarget(ctx.renderer, target)

	println("done tex_to_surf")

	return surf
}

fn save_tex(ctx &Context, path string, tex &sdl.Texture) {
	println("image: save_tex path: $path, tex: (isnil) ${isnil(tex)}")

	surf := tex_to_surf(ctx, tex)

	//C.SDL_SaveBMP(surf, path.str)
	img.save_png(surf, path.str)

	println("freeing surface...")
	// XXX?
	//vsdl2.free_surface(surf)

	println("done save_tex")
}

fn save_surf(path string, surf &sdl.Surface) {	
	println("image: save_surf path: $path, surf: (isnil) ${isnil(surf)}")

	img.save_png(surf, path.str)

	println("done save_surf")
}

pub fn update_loop(ctx &Context) {
	println("image: update_loop")

	mut quit := false
	for {
		ev := sdl.Event{}
		// XXX?
		unsafe {
		for 0 < sdl.poll_event(&ev) {
			match int(ev.@type) {
				C.SDL_QUIT {
					quit = true
				}
				else {}
			}
		}
		}

		if quit {
			break
		}

		surf := C.SDL_GetWindowSurface(ctx.window)
		_ = surf
		

		if C.SDL_UpdateWindowSurface(ctx.window) != 0 {
			unsafe {
				s := cstring_to_vstring(C.SDL_GetError())
				println("s: $s")
			}
			panic("can't flip")
		}
	}
}

// Create empty image
pub fn create_image(mut ctx Context, w int, h int) &Image {
	println("image: create_image w: $w, h: $h")

	resize_window(mut ctx, w, h)

	//surf := vsdl2.create_rgb_surface(0, w, h, 32, 0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000)
	surf := sdl.create_rgb_surface(0, w, h, 32, 0, 0, 0, 0)
	tex := sdl.create_texture_from_surface(ctx.renderer, surf)
	
	mut i := &Image{
		ctx: ctx,
		w: w,
		h: h,
		tex: tex,
		//surf: &vsdl2.Surface(0),
		surf: surf,
	}

	i.fill(sdl.Color{255, 255, 255, 255})

	return i
}

// Load image from file
pub fn load_image(mut ctx Context, path string) &Image {
	println("image: load_image path: $path")

	//println("renderer: (isnil) ${isnil(ctx.renderer)}")

	if !(os.exists(path) && os.is_file(path)) {
		panic("error: path doesn't exist")
	}

	surf := img.load(path.str)
	mut tex := &sdl.Texture(0)

	if !isnil(surf) {
		tex = sdl.create_texture_from_surface(ctx.renderer, surf)
	}

	w, h := get_sdl_tex_size(tex)

	resize_window(mut ctx, w, h)

	i := &Image{
		ctx: ctx,
		w: w,
		h: h,
		tex: tex,
		surf: surf,
	}
	
	return i
}

/*
fn (mut i Image) update_tex() {
	println("Image update_tex")

	sz := cstring_to_vstring(C.SDL_GetError())
	println("derp sz: $sz")

	old_tex := i.tex
	new_tex := vsdl2.create_texture_from_surface(i.ctx.renderer, i.surf)

	if isnil(new_tex) {
		s := cstring_to_vstring(C.SDL_GetError())
		println("new_tex is null s: $s")
		panic("update_tex error")
	}
	
	i.tex = new_tex

	// XXX
	println("freeing old_tex...")
	vsdl2.destroy_texture(old_tex)	
}
*/

pub fn (mut i Image) put_pixel(x int, y int, c sdl.Color) {
	//println("Image put_pixel x: $x, y: $y, c: $c")

	// XXX use UpdateTexture?
	
	rect := sdl.Rect{x, y, 1, 1}
	//sdl.fill_rect(i.surf, &rect, u32(c))
	sdl.fill_rect(i.surf, &rect,
		sdl.map_rgba(i.ctx.screen.format, c.r, c.g, c.b, c.a))

	// XXX update tex
	//i.update_tex()

	//save_surf("/tmp/z2_surf.png", i.surf)
	//save_tex(i.ctx, "/tmp/z2.png", i.tex)
}

pub fn (i &Image) get_pixel(x int, y int) sdl.Color {
	panic("not imlemented")
}

// TODO: add fill_rect
pub fn (mut i Image) fill(c sdl.Color) {
	println("image: Image fill c: $c")

	rect := sdl.Rect{0, 0, i.w, i.h}

	//sdl.fill_rect(i.surf, &rect, u32(c))
	sdl.fill_rect(i.surf, &rect,
		sdl.map_rgba(i.ctx.screen.format, c.r, c.g, c.b, c.a))

	// XXX update tex
	//i.update_tex()
}

// Go version
pub fn (mut i Image) draw_line(x1_ int, y1_ int, x2_ int, y2_ int, c sdl.Color) {
	println("image: Image draw_line x1: $x1_, y1: $y1_, x2: $x2_, y2: $y2_, c: $c")

	mut x0 := x1_
	mut y0 := y1_
	mut x1 := x2_
	mut y1 := y2_

	mut dx := x1 - x0
	if dx < 0 {
		dx = -dx
	}
	mut dy := y1 - y0
	if dy < 0 {
		dy = -dy
	}
	mut sx, mut sy := 0, 0
	if x0 < x1 {
		sx = 1
	} else {
		sx = -1
	}
	if y0 < y1 {
		sy = 1
	} else {
		sy = -1
	}
	mut err := dx - dy

	for {
		i.put_pixel(x0, y0, c)
		if x0 == x1 && y0 == y1 {
			break
		}
		e2 := 2 * err
		if e2 > -dy {
			err -= dy
			x0 += sx
		}
		if e2 < dx {
			err += dx
			y0 += sy
		}
	}
}

pub fn (i &Image) save(path string) {
	println("image: Image save path: $path")

	img.save_png(i.surf, path.str)

	println("done save")
}

pub fn (i &Image) free() {
	println("image: Image free")

	//i.
	panic("not implemented")
}



fn load_image_sdl_tex(ctx &Context, path string) &sdl.Texture {
	println("image: load_image_sdl_tex path: $path")

	//println("renderer: (isnil) ${isnil(ctx.renderer)}")

	if !(os.exists(path) && os.is_file(path)) {
		panic("error: path doesn't exist")
	}

	sdl_img := img.load(path.str)
	mut tex := &sdl.Texture(0)

	if !isnil(sdl_img) {
		tex = sdl.create_texture_from_surface(ctx.renderer, sdl_img)
	} else {
		s := unsafe { cstring_to_vstring(sdl.get_error()) }
		panic("error s: $s")
	}

	///*
	// XXX???
	if isnil(tex) {
		panic("tex is nil")
	}
	//*/

	return tex
}

fn get_sdl_tex_size(tex &sdl.Texture) (int, int) {
	println("image: get_sdl_tex_size tex: (isnil) ${isnil(tex)}")

	///*
	// XXX???	
	if isnil(tex) {
		panic("tex is nil")
		//return
	}
	//*/

	texw := 0
	texh := 0
	C.SDL_QueryTexture(tex, 0, 0, &texw, &texh)

	return texw, texh
}
