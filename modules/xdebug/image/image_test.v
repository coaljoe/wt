module image

import sdl


fn test_image() {
	//test()

	//XXX
	//mut ctx := init()
	mut ctx := get_ctx()

	//get_renderer(ctx)

	
	tex := load_image_sdl_tex(ctx, "test/heightmap.png")

	//println(tex)

	println(get_sdl_tex_size(tex))
	

	mut i := load_image(mut ctx, "test/heightmap.png")
	//mut i := create_image(ctx, 64, 64)
	println("i.w: $i.w, i.h: $i.h")

	

	//i.fill(vsdl2.Color{0, 255, 0, 255})
	i.put_pixel(i.w/2, i.h/2, sdl.Color{255, 0, 0, 255})
	//i.put_pixel(10, 10, vsdl2.Color{0, 255, 0, 255})

	//panic("z2")

	//println("i: $i")
	//panic("z")

	i.draw_line(0, 0, i.w, i.h, sdl.Color{0, 0, 255, 255})

	//panic("z3")

	i.draw_line(i.w, 0, 0, i.h, sdl.Color{0, 255, 0, 255})

	i.save("/tmp/out.png")

	//panic("z4")

	//update_loop(ctx)
}
