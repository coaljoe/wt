module main

import player
//import importers.importxm
import importxm
import importmod

pub struct Editor {
pub mut:
	m &player.Module
	// ??? should this be in context ?
	pl &player.Player
	ctx_player &player.Context
	ctx &Context
}

pub fn new_editor(mut ctx Context, mut ctx_player player.Context) &Editor {
	e := &Editor{
		m: &player.Module(unsafe { nil }),
		pl: &player.Player(unsafe { nil }),
		ctx_player: ctx_player,
		ctx: ctx,
	}
	ctx.editor = e

	return e
}

// XXX ???
fn (mut e Editor) create_player() {
	mut pl := player.new_player(mut e.ctx_player)
	e.pl = pl
}

pub fn (mut e Editor) load(path string) {
	println("Editor load: path: $path")

	mut l := player.new_module_loader(e.ctx_player)
	
	m := l.load(path)
	_ = m

	e.create_player()
	
	e.load_module(m)
}

pub fn (mut e Editor) load_module(m &player.Module) {
	e.m = m

	e.pl.set_module(m)

	// XXX fixme?
	mut pv := new_patternview(e.ctx, m.patterns[0])
	//mut pv := new_patternview(e.ctx, m.patterns[1])
	pv.@spawn()
	
	e.ctx.gui.patternview = pv
}

pub fn (mut e Editor) import_mod(path string) {
	println("Editor import_mod path: $path")

	//mut l := importxm.new_xm_importer(e.ctx_player)

	//mut l := ImporterI{}
	// XXX fixme ?
	mut l := &ImporterI(importxm.XmImporter{0})

	// XXX fixme ?
	//if true {
	if path.to_lower().ends_with(".xm") {
		println("import xm...")
		l = importxm.new_xm_importer(e.ctx_player)
	} else if path.to_lower().ends_with(".mod") {
		println("import mod...")
		l = importmod.new_mod_importer(e.ctx_player)
	} else {
		panic("unknown path ext")
	}

	m := l.load(path)
	_ = m

	e.create_player()
	e.load_module(m)
}

pub fn (mut e Editor) import_module(m &player.Module) {
	
}

// XXX save module to path ?
pub fn (e &Editor) save(path string) {
	println("Editor save path: $path")

	mut s := player.new_module_saver(e.ctx_player)
	s.save(e.m, path)
}

// XXX TODO: move to tools ?
//pub fn (e &Editor) tool_replace_instruments(inst_num int) {
pub fn (mut e Editor) tool_replace_instruments(inst_num int) {
	println("Editor tool_replace_instruments inst_num: $inst_num")

	for z, pt in e.m.patterns {
		println("z: $z")
		for i, tr in pt.tracks {
			println("-> tr i: $i")
			for j, c in tr.cells {
			// XXX not working, bug ?
			//for j, mut c in tr.cells {
				println("-> c j: $j")

				mut xc := tr.cells[j]
				if xc.has_instrument() {
					xc.inst = inst_num
				}
			}
		}
	}

	println("done tool_replace_instruments")
}

pub fn (e &Editor) play() {

}

pub fn (e &Editor) render() {

	//e.gui.patternview.render()
}

pub fn (mut e Editor) update(dt f32) {
	println("Editor update")

	e.ctx.gui.update(dt)
}
