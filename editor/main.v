module main

import os
import time
import strconv

import player
import vxguic as xg
import debug { pp }
import player_backend as pb

fn main() {
	println("main()")

	mut ctx := new_context()

	mut gui := new_gui(mut ctx)
	gui.init(800, 600)


	// XXX init player context
	mut p_ctx := player.new_context()

	mut e := new_editor(mut ctx, mut p_ctx)

	/*
	mut m := player.new_module(mut ctx, 4)

	mut pt := m.add_new_pattern()

	mut t := pt.tracks[0] 
	//t.cells[0].note = player.get_note_ref("C-4")
	//t.cells[2].note = player.get_note_ref("C-5")
	//t.cells[4].note = player.get_note_ref("C-4")
	t.cells[0].note = player.get_note_opt("C-4")
	t.cells[2].note = player.get_note_opt("C-5")
	t.cells[4].note = player.get_note_opt("C-4")

	// XXX fixme
	//note_ref := player.get_note("C-4")

	//t.cells[0].note = &note_ref
	//t.cells[2].note = &note_ref
	//t.cells[4].note = &note_ref

	//note_ref := player.get_note_ref("C-4")
	//note_ref := player.get_note("C-4")
	//t.cells[0].note = &note_ref
	//println("node_ref:")
	//println(note_ref)
	//println(t.cells[0])
	//println(pt.tracks[0])

	//panic("4")
	
	//e.load("../testdata/files/new_file3")
	//e.load("../testdata/files/new_file3_c1")
	//e.load_module(m)
	*/


	if os.args.len > 1 {
		//// XXX import option
		if !("-import" in os.args) {
			e.load(os.args[1])
		} else {
			path := os.args_after("-import")[1]
			e.import_mod(path)

			//mut s := p.new_module_saver(ctx)
			//s.save(m, "/tmp/out")

			e.save("/tmp/out_editor")

			//pp(2)

		}

		//// XXX replace instrument option
		if "-replace-instruments" in os.args {
		//if true {
			//e.tool_replace_instruments(1)

			new_num := strconv.atoi(
				os.args_after("-replace-instruments")[1]
			) or {
				panic(err)
			}
			e.tool_replace_instruments(new_num)
			//e.tool_replace_instruments(1)

			e.save("/tmp/out_editor2")
			//pp(2)
		}

		//// XXX load intstrument
		if "-load-instrument" in os.args {
		//if true {

			inst_num := strconv.atoi(
				os.args_after("-load-instrument")[1]
			) or {
				panic(err)
			}
			path := os.args_after("-load-instrument")[2]

			mut dl := player.new_module_loader(p_ctx)
			//inst := dl.load_instrument("/tmp/out_inst.json")
			inst := dl.load_instrument(path)

			//e.m.instruments[0] = inst
			e.m.instruments[inst_num-1] = inst

			//pp(2)
		}
		
		//// XXX save load intstrument
		if "-save-instrument" in os.args {
		//if true {

			inst_num := strconv.atoi(
				os.args_after("-save-instrument")[1]
			) or {
				panic(err)
			}
			path := os.args_after("-save-instrument")[2]

			inst := e.m.instruments[inst_num-1]

			ds := player.new_module_saver(p_ctx)
			//ds.save_instrument(inst, "/tmp/out_inst.json")
			ds.save_instrument(inst, path)

			pp(2)
		}
	} else {
		e.load("../testdata/files/new_file3")
		// XXX bug ?
		//e.load("../testdata/files/new_file3_c3")
	}

	if true {
		e.save("/tmp/out_editor3")
	}

	if false {
		inst := e.m.instruments[0]

		ds := player.new_module_saver(p_ctx)
		ds.save_instrument(inst, "/tmp/out_inst.json")

		pp(2)
	}
	
	//panic("2")

	//// Init audio backend
	mut pl := e.pl
	//pb.init_backend(mut pl)
	pb.init_backend2(
		pb.BackendConfig{
			num_frames: 1024,
			//num_frames: 2048,
			//num_frames: 8192,
		})

	if true {
		pl.play()
		//pl.cb_done = true
		pl.cb_done = false
	}

	//// XXX standard player_cli update loop
	/*
	for !pl.cb_done {
		// do some stuff here
		// XXX should it be called at 50hz or not? not tested
		time.sleep(16 * time.millisecond)
		//time.sleep(5000 * time.millisecond)
	}
	*/

	///*
	//for int(C.xg_app_step()) != 0 {

		// XXX
		//C.xg_app_step()
		
		println("step")

		/*
		e.render()
		e.render()
		e.render()
		e.render()
		e.render()
		*/

		//e.update(0.01)
		//e.render()
		
		//e.render()
		//e.update(0.01)

		//if false {
		// XXX 50hz update loop ?
		//for !pl.cb_done {
		for pl.playing {
			println("xstep")
			
			//num_frames := pb.get_buffer_frames2()
			num_frames := pb.expect2()

			println("num_frames: $num_frames")
				
			if num_frames > 0 {
			
				out, ret := pl.render_buf(num_frames)
				_ = ret

				pb.push_audio2(out, num_frames)
				
				// XXX should it be called at 50hz or not? not tested
				//time.sleep(16 * time.millisecond)
				//time.sleep(100 * time.millisecond)
			}
			// XXX should it be called at 50hz or not? not tested
			//time.sleep(16 * time.millisecond)

			// XXX

			if int(C.xg_app_step()) == 0 {
				panic(6)
			}
		
			// do some stuff here
			// XXX should it be called at 50hz or not? not tested
			//time.sleep(16 * time.millisecond)
			//time.sleep(40 * time.millisecond)
			//time.sleep(5000 * time.millisecond)

			e.update(0.01)
			e.render()

			//if int(C.xg_app_step()) == 0 {
			//	panic(6)
			//}
			
			//// XXX here should be a delay for main loop ?
				
			// XXX should it be called at 50hz or not? not tested
			//time.sleep(16 * time.millisecond) // 60 hz
			time.sleep(20 * time.millisecond) // 50 hz
		}
		//}
	//}
	//*/


	// Finally
	pb.deinit_backend()

	println("exiting...")

	println("done")
}
