module main

pub struct Style {
pub mut:
	//// Colors
	
	cl_note C.xg_Color
	cl_instrument C.xg_Color
	cl_vol C.xg_Color
	cl_effect C.xg_Color
	// XXX fixme?
	cl_bg C.xg_Color

	//// Layout info
	
	li_cell_width int
	li_cell_height int
	//li_cell_padding_left int
	//li_track_separator_width int

	li_pattern_row_numbers_width int

	// Styles

	//st_note C.xg_Style
	st_note voidptr
	st_instrument voidptr
	st_vol voidptr
	st_effect voidptr

	// hl ?
	st_hl_playing_row voidptr
	st_hl_playing_row_nohl voidptr

	// XXX fixme?
	st_bg voidptr
}

fn new_style() &Style {
	mut s := &Style{}
	//s.cl_note = C.xg_Color{0, 255, 0, 255}
	s.cl_note = C.xg_Color{50, 30, 120, 255}
	s.cl_instrument = C.xg_Color{54, 64, 74, 255}
	//s.cl_instrument = C.xg_Color{62, 72, 80, 255}
	s.cl_vol = C.xg_Color{54, 64, 74, 255}
	s.cl_effect = C.xg_Color{54, 64, 74, 255}
	s.cl_bg = C.xg_Color{0, 255, 0, 255}
	//s.cl_bg = C.xg_Color{100, 100, 100, 255}	
	//s.cl_bg = C.xg_Color{50, 80, 90, 255}

	s.st_note = C.xg_new_style()
	s.st_instrument = C.xg_new_style()
	s.st_vol = C.xg_new_style()
	s.st_effect = C.xg_new_style()
	s.st_bg = C.xg_new_style()

	// On hl ?
	s.st_hl_playing_row = C.xg_new_panel_style()
	// Default state / off hl ?
	s.st_hl_playing_row_nohl = C.xg_new_panel_style()

	s.build()

	return s
}

fn (mut s Style) build() {
	println("Style build")

	// XXX
	//s.st_note.text_color = s.cl_note

	{
		mut st := s.st_note
		C.xg_style_set_text_color(st, s.cl_note)
	}
	
	{
		mut st := s.st_instrument
		C.xg_style_set_text_color(st, s.cl_instrument)
	}

	{
		mut st := s.st_bg
		C.xg_style_set_text_color(st, s.cl_bg)
	}

	{
		mut st := s.st_hl_playing_row

		p_st := C.xg_style_pget_panel_style(st)
		
		//C.xg_style_set_text_color(st, s.cl_bg)
		C.xg_panel_style_set_draw_type_rect(p_st)

		//C.xg_panel_style_set_draw_type_rect(st)
		
		//C.xg_panel_style_set_draw_type_rectz(st)
		
		rs := C.xg_panel_style_pget_rect_style(p_st)
		cl := C.xg_Color{0, 255, 0, 255}
		C.xg_rect_style_set_color(rs, cl)
	}	

	println("done Style build")
}
