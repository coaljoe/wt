module main

import player

pub interface ImporterI {
mut:
	load(path string) &player.Module
}
