module main

import player
import vxguic as xg

[heap]
pub struct TrackView {
pub mut:
	patternview &PatternView // link
	m &player.Track
	panel C.voidptr // xg.Panel fixme?
	//text &xg.Label
	text C.voidptr // xg.Label XXX fixme
	//panel &xg.Panel
	offset_x int
	offset_x_step int
	draw_separator bool
	//separator &xg.Panel
	separator_width int
	cellviews []&CellView
	ctx &Context
}

fn new_trackview(ctx &Context, pv &PatternView, m &player.Track) &TrackView {
	println("new_trackview")

	v := &TrackView{
		patternview: pv,
		m: m,
		//offsetXStep:    60,
		draw_separator:  true,
		//separator_width: tut.gui.style.liTrackSeparatorWidth,
		separator_width: 2,
		ctx: ctx,
	}
	
	return v
}

fn (mut v TrackView) @spawn() {
	println("TrackView spawn")

	println(v.m)

	//my_sheet := C.xg_sheetsys_create_sheet(c"test")
	//C.xg_sheetsys_add_sheet(my_sheet)
	

	//cell_width := 110
	//cell_width := 110
	//cell_width := 120
	cell_width := 130
	//cell_height := 40
	cell_height := 42

	track_width := cell_width

	println("ZZZ: $v.m.channel_idx")

	v.offset_x = track_width * v.m.channel_idx
	//v.offset_x = 0

	println("ZZZ2: $v.offset_x")

	//panic(2)

	{
		rect := C.xg_Rect{v.offset_x, 0, track_width, 500}
		v.panel = C.xg_new_panel(rect)

		C.xg_widget_add_child(v.patternview.panel, v.panel)
	}

	//rect := C.xg_Rect{0, 20, 100, 100}
	//debug_text := C.xg_new_button(rect)

	//root := C.xg_sheet_root(my_sheet)
	//C.xg_widget_add_child(root, debug_text)

	

	for i, c in v.m.cells {
		println("i: $i, c: $c")

		mut cv := new_cellview(v, c, i)
		v.cellviews << cv
		cv.@spawn()

		/*
		
		rect := C.xg_Rect{v.offset_x, cell_height * i, cell_width, cell_height}
		debug_text := C.xg_new_button(rect)		

		//
		//root := C.xg_sheet_root(my_sheet)
		//C.xg_widget_add_child(root, debug_text)	
		//

		C.xg_widget_add_child(v.patternview.panel, debug_text)

		mut title := "$i    "

		if !isnil(c.note) {
			title +=  c.note.val.name
			//panic("3")
		} else {
			title += "   "
		}

		label := C.xg_button_pget_label(debug_text)
		C.xg_label_set_text(label, &char(title.str))

		*/


		//if i >= 10 {
		if i >= 16 {
			break
		}
	}


	//panic("2")

}

/*
func (v *TrackView) spawn() {
	// Build
	idx := v.m.ChannelIdx
	v.offsetX = v.offsetXStep * idx

	//cellHeight := 20
	cellHeight := tut.gui.style.liCellHeight
	cellWidth := tut.gui.style.liCellWidth
	offsetX := tut.gui.style.liPatternRowNumbersWidth
	//padding := 2 //4
	padding := v.separatorWidth //4
	pw := cellWidth + padding
	ph := cellHeight * v.m.Length()
	px := offsetX + (pw * v.m.ChannelIdx)
	v.panel = xg.NewPanel(xg.Rect{px, 0, pw, ph})
	v.panel.Label.SetText("")
	//v.panel.Style.DrawAsRect = true
	//v.panel.Style.DrawRectColor = xg.Color{49, 54, 59, 255}
	v.panel.Style = tut.gui.style.stTrackPanel
	v.patternView.panel.AddChild(v.panel)

	//v.text = xg.NewLabel(xg.Pos{0, 0}, "ch1 | ch2 | ch3 | ch4")
	title := fmt.Sprintf("| ch%d |", idx)
	v.text = xg.NewLabel(xg.Pos{v.offsetX, 0}, title)
	//v.panel.AddChild(v.text)
	//tut.gui.sheet.Root().AddChild(v.arrow)

	//v.text.Stylexgi.Context.Style.Fonts["my_font"]

	for i, c := range v.m.Cells {
		cv := newCellView(v, &c, i)
		v.cellViews = append(v.cellViews, cv)
		cv.spawn()
	}

	// Separator
	rect := xg.Rect{(px + cellWidth) - padding, 0, v.separatorWidth, ph}
	//pp(rect)
	v.separator = xg.NewPanel(rect)
	v.separator.Label.SetText("")
	v.separator.Style.DrawAsRect = true
	v.separator.Style.DrawRectColor = tut.gui.style.clTrackSeparator
	v.patternView.panel.AddChild(v.separator)
}

func (v *TrackView) update(dt float64) {
}

*/
