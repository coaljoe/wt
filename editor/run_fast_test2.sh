#!/bin/bash

#export FREETYPE_PROPERTIES=truetype:interpreter-version=35
#export FREETYPE_PROPERTIES=truetype:interpreter-version=38
#export FREETYPE_PROPERTIES=no-stem-darkening=0
#export FREETYPE_PROPERTIES=type1:darkening-parameters=500,300,1000,200,1500,100,2000,0
#export FREETYPE_PROPERTIES=no-stem-darkening=0:type1:darkening-parameters=500,300,1000,200,1500,100,2000,0
export FREETYPE_PROPERTIES="no-stem-darkening=0 darkening-parameters=500,300,1000,200,1500,100,2000,0"
#export FREETYPE_PROPERTIES="truetype:interpreter-version=35 cff:no-stem-darkening=1 autofitter:warping=1"

## Run

echo "run..."
#./build.sh && ./editor |& tee log
#./build.sh && ./editor ../testdata/files/new_file3_c3 |& tee log
#./build.sh && ./editor -import ../testdata/mod/hydra-the_church.mod |& tee log
./build.sh && ./editor -import ../testdata/mod/hydra-the_church_ed_test1.mod |& tee log
