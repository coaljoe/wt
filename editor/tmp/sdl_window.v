module main

import os
import sdl
import math
import player as p
import common as c

fn main() {

	// delay uses milliseconds so 1000 ms / 30 frames (30fps) roughly = 33.3333 ms/frame
	time_per_frame := 1000.0 / 30.0
	//stdout_f := os.open("/dev/stdout") or { panic("error: stdout") }
	out_f_path := "/tmp/out_f.raw"
	mut out_f := os.create(out_f_path) or { panic("error") }

	mut g := p.new_gen()
	//mut freq := 440.0
	mut freq := 440.5
	//fstep := 10.0
	fstep := 0.5

	C.SDL_Init(C.SDL_INIT_VIDEO)
	window := C.SDL_CreateWindow('Hello SDL2', 300, 300, 500, 300, 0)
	renderer := C.SDL_CreateRenderer(window, -1, C.SDL_RENDERER_ACCELERATED | C.SDL_RENDERER_PRESENTVSYNC)

	mut frame := 0
	mut should_close := false

	mut this_time := sdl.get_perf_counter()
	mut last_time := u64(0)
	mut dt := 0.0
	for {

		last_time = this_time
		this_time = sdl.get_perf_counter()
		dt = f64(this_time - last_time) / f64(sdl.get_perf_frequency())
	
		ev := SDL_Event{}
		for 0 < sdl.poll_event(&ev) {
			match int(ev.@type) {
				C.SDL_QUIT { should_close = true }
				C.SDL_KEYDOWN {
					key := int(ev.key.keysym.sym)
					if key == C.SDLK_ESCAPE || key == C.SDLK_q {
					        should_close = true
					        break
					}
					eprintln("key: $key")

					if key == C.SDLK_UP {
						freq += fstep
						eprintln("new freq: $freq")
					}
					if key == C.SDLK_DOWN {
					    freq -= fstep
					    eprintln("new freq: $freq")
					}

					title_s := "freq: $freq"
					C.SDL_SetWindowTitle(window, title_s.str)
					
				}
				else {
					
				}
			}
		}
		if should_close {
			break
		}

		eprintln("frame: $frame")
		eprintln("dt: $dt")

		if frame != 0 {

			//length := 440*2
			//length := 1024
			sr := 44100
			num_samples := f32(sr) * dt
			eprintln("num_samples: $num_samples")
			length := int(f32(sr) * dt)
			//length /= 2
		
			data := g.square_wave_cont(freq, 1.0, length)
			//eprintln(data)
			c.dump_raw("/dev/stdout", data)
			//c.write_raw_to_file(mut stdout_f, data)
			//c.write_raw_to_file(mut stdout, data)
			//c.dump_raw_append(out_f_path, data)
			c.dump_raw_append_file(mut out_f, data)
		}
	
		C.SDL_SetRenderDrawColor(renderer, 255, 55, 55, 255)
		C.SDL_RenderClear(renderer)
		C.SDL_RenderPresent(renderer)
		
		// should limit system to (1 / time_per_frame) fps
		sdl.delay(u32(math.floor(time_per_frame - dt)))  

		frame++
	}
	
	C.SDL_DestroyRenderer(renderer)
	C.SDL_DestroyWindow(window)
	C.SDL_Quit()
}