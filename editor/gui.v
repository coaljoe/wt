module main

[heap]
pub struct Gui {
pub mut:
	//xgi *...
	patternview &PatternView
	style &Style
	enabled bool
	ctx &Context
}

fn new_gui(mut ctx &Context) &Gui {
	g := &Gui{
		patternview: &PatternView(unsafe { nil }),
		style: &Style(unsafe { nil }),
		enabled: true,
		ctx: ctx,
	}
	ctx.gui = g
	
	return g
}

fn (mut g Gui) init(width int, height int) {
	println("Gui init")

	C.xg_create_xgui_and_sdl_app(width, height, false)
	
	///*
	//f1 := C.xg_load_font(c"res/gui/base/fonts/TerminusTTF.ttf", 12)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/TerminusTTF.ttf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/nimbusmono-regular.otf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/nimbusmono-bold.otf", 15)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/SourceCodePro-Medium.otf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/SourceCodePro-Bold.otf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/texgyrecursor-regular.otf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/texgyrecursor-bold.otf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/texgyrecursor-regular.otf", 15)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/nimbusmono-regular.otf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/MonteCarloFixed12-Bold.ttf", 17)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/berryBold.otb", 60)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/uushimedium.otb", 60)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/lemon.ttf", 18)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Boxfont Round.ttf", 13)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/clR8x8.otb", 8)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Px437_Cordata_PPC-21.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Px437_Cordata_PPC-400.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Mx437_Cordata_PPC-21.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Narrow.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Narrow.ttf", 12)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Narrow.ttf", 13)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Narrow.ttf", 18)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 12)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 13)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 14)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 15)
	f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Regular.ttf", 16)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Square.ttf", 10)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Square.ttf", 12)
	//f1 := C.xg_load_font(c"res/gui/base/fonts/Deferral-Square.ttf", 11)
	_ = f1
	C.xg_set_font(c"text_font", f1)
	//*/

	// XXX
	//C.xg_font_set_aa(f1, false)

	// Style
	g.style = new_style()

	//g.patternview = &PatternView(0)
}

pub fn (g &Gui) render() {
	println("Gui render")

	g.patternview.render()
}

pub fn (mut g Gui) update(dt f32) {
	println("Gui update")

	// XXX ?
	g.patternview.update(dt)

	// ?
	g.render()
}
