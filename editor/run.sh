#!/bin/bash

## XXX rebuilding xguic binary libs (like .a)

echo "rebuild xguic binary lib files..."
pushd .
cd ../../xgui_c/xguic
./build.sh || { echo "xguic build error. exiting..."; exit 1; }
#test $? -eq 0 || { echo "xguic build error. exiting..."; exit 1; }
popd

## XXX update/rebuild xguic

echo "rebuild vxguic..."
pushd .
cd ./lib/vxguic
./build.sh || { echo "vxguic build error. exting..."; exit 1; }
popd

## Run

echo "run..."
./build.sh && ./editor |& tee log
