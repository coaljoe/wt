module main

import strconv
import player

struct CellView {
pub mut:
	trackview &TrackView // link
	m &player.Cell
	text C.voidptr // xg.Label
	note_label C.voidptr // xg.Label
	instrument_label C.voidptr // xg.Label
	vol_label C.voidptr// xg.Label
	effect_label C.voidptr// xg.Label
	hl_cell bool
	hl_panel C.voidptr// xg.Panel
	offset_ystep int
	cell_idx int
}

pub fn new_cellview(tv &TrackView, m &player.Cell, cell_idx int) &CellView {
	println("new_cellview")
	
	cv := &CellView{
		trackview: tv,
		m: m,
		//offset_ystep: tut.gui.style.liCellHeight,
		offset_ystep: 20, // fixme?
		cell_idx: cell_idx,
	}

	return cv
}

pub fn (mut v CellView) build() {
	println("CellView build")

	/*
	stFg := tut.gui.style.stNote
	stBg := tut.gui.style.stTrackText
	*/

	// XXX use ctx?
	gui := v.trackview.ctx.gui
	st_bg := gui.style.st_bg

	///*
	{
	// Style
	w := C.xg_label_pget_widget(v.note_label)
	st := C.xg_widget_pget_style(w)
	
	// Update data
	if v.m.has_note() {
		z := v.m.note.val.name
		C.xg_label_set_text(v.note_label, &char(z.str))
	
		C.xg_style_set_style(st, gui.style.st_note)
		
		//v.noteLabel.SetText(v.m.note.name)
		//v.noteLabel.Style = stFg
	} else {
		C.xg_label_set_text(v.note_label, c"---")
		C.xg_style_set_style(st, st_bg)
		
		//v.noteLabel.SetText("---")
		//v.noteLabel.Style = stBg
	}
	}
	//*/

	///*
	{
	// Style
	w := C.xg_label_pget_widget(v.instrument_label)
	st := C.xg_widget_pget_style(w)	
	
	if v.m.inst != -1 {
		s := strconv.v_sprintf("%02d", v.m.inst)
		C.xg_label_set_text(v.instrument_label, &char(s.str))

		C.xg_style_set_style(st, gui.style.st_instrument)	
		
		//v.instrumentLabel.Style = tut.gui.style.stInstrument
	} else {
		C.xg_label_set_text(v.instrument_label, c"--")

		C.xg_style_set_style(st, st_bg)
		
		//v.instrumentLabel.Style = stBg
	}
	}
	//*/

	///*
	{
	// Style
	w := C.xg_label_pget_widget(v.vol_label)
	st := C.xg_widget_pget_style(w)	
	
	if v.m.vol != -1 {
		s := strconv.v_sprintf("%02d", v.m.vol)
		C.xg_label_set_text(v.vol_label, &char(s.str))
		C.xg_style_set_style(st, gui.style.st_vol)
		//v.volLabel.SetText(s)
		//v.volLabel.Style = tut.gui.style.stVol
	} else {
		C.xg_label_set_text(v.vol_label, c"--")
		C.xg_style_set_style(st, st_bg)
		// Reset style
		//v.volLabel.Style = tut.gui.style.stNoteLabel
		//v.volLabel.Style = stBg
	}
	}
	//*/

	///*
	{
	// Style
	w := C.xg_label_pget_widget(v.effect_label)
	st := C.xg_widget_pget_style(w)	
	
	if v.m.effect != -1 {
		/*
		eff := v.m.effect
		s := eff.toString()
		v.effectLabel.SetText(s)
		v.effectLabel.Style = tut.gui.style.stEffect
		*/
		C.xg_style_set_style(st, gui.style.st_effect)
	} else {
		C.xg_label_set_text(v.effect_label, c"---")
		C.xg_style_set_style(st, st_bg)
		//v.effectLabel.Style = stBg
		//v.effectLabel.Style = tut.gui.style.stEffectCol
	}
	}
	//*/

	/*
	if v.m.instrumentNum != nil {
		num := *v.m.instrumentNum
		s := fmt.Sprintf("%02d", num)
		v.instrumentLabel.SetText(s)
		v.instrumentLabel.Style = tut.gui.style.stInstrument
	} else {
		v.instrumentLabel.SetText("--")
		v.instrumentLabel.Style = stBg
	}

	if v.m.vol != nil {
		vol := *v.m.vol
		s := fmt.Sprintf("%02x", vol)
		v.volLabel.SetText(s)
		v.volLabel.Style = tut.gui.style.stVol
	} else {
		v.volLabel.SetText("--")
		// Reset style
		//v.volLabel.Style = tut.gui.style.stNoteLabel
		v.volLabel.Style = stBg
	}

	if v.m.effect != nil {
		eff := v.m.effect
		s := eff.toString()
		v.effectLabel.SetText(s)
		v.effectLabel.Style = tut.gui.style.stEffect
	} else {
		v.effectLabel.SetText("---")
		//v.effectLabel.Style = stBg
		v.effectLabel.Style = tut.gui.style.stEffectCol
	}
	*/
}

pub fn (mut v CellView) @spawn() {

	title := "XXX-YY-ZZZ"

	//frame_padding_y := 1 //4
	//frame_padding_y := 8 //4
	frame_padding_y := 4 //4
	_ = frame_padding_y
	px := 2
	py := frame_padding_y + (v.offset_ystep * v.cell_idx)

	pos := C.xg_make_pos(px, py)
	v.text = C.xg_new_label(pos, &char(title.str))

	//C.xg_widget_add_child(v.trackview.panel, v.text)

	

	rect := C.xg_widget_rect(v.trackview.panel)
	cell_w := rect.w
	cell_h := v.offset_ystep
	panel := C.xg_new_panel(C.xg_make_rect(0, py, cell_w, cell_h))
	//panel.Style.DrawAsRect = true

	// XXX test
	{
		// Style
		w := C.xg_label_pget_widget(panel)
		st := C.xg_widget_pget_style(w)
		cl := C.xg_Color{255, 0, 0, 255}
		C.xg_style_set_text_color(st, cl)
	}
	
	// XXX test
	{
		// Style
		st := C.xg_panel_widget_style(panel)

		println("st:")
		println(st)
		
		C.xg_panel_style_set_draw_type_rect(st)
		//C.xg_panel_style_set_draw_type_rect(st)

		//C.xg_panel_style_set_draw_type_rectz(st)

		rs := C.xg_panel_style_pget_rect_style(st)
		cl := C.xg_Color{0, 0, 255, 255}
		C.xg_rect_style_set_color(rs, cl)

		//panic(4)
	}

	
	C.xg_widget_set_active(panel, false)
	v.hl_panel = panel
	//if v.cellIdx%16 == 0 {
	//	v.hlDefaultStyle = tut.gui.style.stBeatStepRowHlPanel
	//} else {
	//	v.hlDefaultStyle = tut.gui.style.stTrackPanel
	//}
	//v.hlPanel.Style = v.hlDefaultStyle
	C.xg_widget_add_child(v.trackview.panel, panel)



	//v.labelStyle = tut.gui.style.stNote
	//st := v.labelStyle

	//y_corr := 1
	y_corr := 2
	//y_corr := 4
	frame_padding_x := 0 // 3
	padding := 0       //6
	//padding_left := tut.gui.style.liCellPaddingLeft
	//padding_left := 2 // XXX fixme
	padding_left := 4 // XXX fixme
	v.note_label = C.xg_new_label(C.xg_make_pos(frame_padding_x + padding_left, py + y_corr), c"nnn")
	//v.note_label.Style = st
	C.xg_widget_set_active(v.note_label, false)
	C.xg_widget_add_child(v.trackview.panel, v.note_label)

	// XXX test
	{
		// Style
		w := C.xg_label_pget_widget(v.note_label)
		st := C.xg_widget_pget_style(w)
		cl := C.xg_Color{255, 0, 0, 255}
		C.xg_style_set_text_color(st, cl)
	}

	/*
	//charSize := 20
	font := v.noteLabel.Style.TextFont
	style := v.noteLabel.Style.FontStyle
	char_size, _ := tut.gui.xgi.Context.TextRenderer.SizeText(" ", font, style)
	sepSize := 6 //4
	sepSize2 := 6
	//pp(charSize)
	*/
	//sep_size := 6 //4
	sep_size := 12 //4
	//sep_size2 := 6
	//sep_size2 := 10
	sep_size2 := 12
	//char_size := 20
	//char_size := 12
	char_size := 8

	mut shx := (char_size * 3) + sep_size
	v.instrument_label = C.xg_new_label(C.xg_make_pos(frame_padding_x + shx + padding + padding_left, py + y_corr), c"ii")
	//v.instrumentLabel.Style = st
	C.xg_widget_set_active(v.instrument_label, false)
	C.xg_widget_add_child(v.trackview.panel, v.instrument_label)
	
	
	shx += (char_size * 2) + sep_size2
	v.vol_label = C.xg_new_label(C.xg_make_pos(frame_padding_x + shx + padding + padding_left, py + y_corr), c"vv")
	//v.volLabel.Style = tut.gui.style.stVol
	C.xg_widget_set_active(v.vol_label, false)
	C.xg_widget_add_child(v.trackview.panel, v.vol_label)

	
	shx += (char_size * 2) + sep_size2
	v.effect_label = C.xg_new_label(C.xg_make_pos(frame_padding_x + shx + padding + padding_left, py + y_corr), c"eee")
	//v.effectLabel.Style = st
	C.xg_widget_set_active(v.effect_label, false)
	C.xg_widget_add_child(v.trackview.panel, v.effect_label)
	

	// XXX
	//v.build()
}
