#!/bin/bash

#export FREETYPE_PROPERTIES=truetype:interpreter-version=35
#export FREETYPE_PROPERTIES=truetype:interpreter-version=38
#export FREETYPE_PROPERTIES=no-stem-darkening=0
#export FREETYPE_PROPERTIES=type1:darkening-parameters=500,300,1000,200,1500,100,2000,0
#export FREETYPE_PROPERTIES=no-stem-darkening=0:type1:darkening-parameters=500,300,1000,200,1500,100,2000,0
export FREETYPE_PROPERTIES="no-stem-darkening=0 darkening-parameters=500,300,1000,200,1500,100,2000,0"
#export FREETYPE_PROPERTIES="truetype:interpreter-version=35 cff:no-stem-darkening=1 autofitter:warping=1"

export VERBOSE=1
export DEBUG=1

export GOTRACEBACK=crash

## Run

echo "run..."
#./build.sh && ./editor |& tee log
#./build.sh && ./editor ../testdata/files/new_file3_c3 |& tee log
#./build.sh && ./editor -import ../testdata/mod/hydra-the_church.mod |& tee log
#./build.sh && ./editor -import ../testdata/mod/hydra-the_church_ed_test1.mod |& tee log
./build.sh && ./editor -import ../testdata/mod/hydra-the_church_ed_test1.mod -replace-instruments 1 -load-instrument 1 ../testdata/inst/out_inst2.json |& tee log

#./build.sh && strace ./editor -import ../testdata/mod/hydra-the_church_ed_test1.mod -replace-instruments 1 -load-instrument 1 ../testdata/inst/out_inst2.json |& tee log

#./build.sh && ./editor -import ../testdata/mod/hydra-the_church_ed_test1.mod -replace-instruments 1 -load-instrument 1 ../testdata/inst/out_inst2.json 1>/dev/null
