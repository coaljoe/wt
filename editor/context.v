module main

[heap]
pub struct Context {
pub mut:
	gui &Gui
	editor &Editor
}

pub fn new_context() &Context {
	mut x := &Context{
		gui: &Gui(unsafe { nil }),
		editor: &Editor(unsafe { nil }),
	}

	return x
}
