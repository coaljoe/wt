module importmod

// Info:
// https://github.com/libxmp/libxmp/blob/master/docs/formats/FORMAT.Protracker
// https://www.exotica.org.uk/wiki/Protracker

import os
import strconv
import encoding.binary

import player
//import editor

pub struct ModImporter {
pub mut:
	//ctx_editor &editor.Context
	ctx_player &player.Context
}

pub fn new_mod_importer(ctx_player &player.Context) &ModImporter {
	im := &ModImporter{
		ctx_player: ctx_player,
	}
	return im
}

pub fn (mut im ModImporter) load(path string) &player.Module {
	// XXX create module
	//num_tracks := 2
	num_tracks := 4
	mut m := player.new_module(mut im.ctx_player, num_tracks)

	mut pt := m.add_new_pattern()

	/*
	mut t := pt.tracks[0]
	//t.cells[0].note = player.get_note_ref("C-4")
	//t.cells[2].note = player.get_note_ref("C-5")
	//t.cells[4].note = player.get_note_ref("C-4")
	t.cells[0].note = player.get_note_opt("C-4")
	t.cells[2].note = player.get_note_opt("C-5")
	t.cells[4].note = player.get_note_opt("C-4")
	*/

	// XXX
	mut f := os.open(path) or {
		panic(err)
	}

	im.parse(mut f)

	return m
}

pub fn (mut im ModImporter) parse(mut f os.File) {
	println("ModImporter parse")

	// XXX
	mut m := im.ctx_player.m
	
	m_songname := read_next_bytes(f, 20)

	println("m_songname: $m_songname")

	f.seek(950, .start) or { panic(err) }
	//f.seek(951, .start) or { panic(err) }

	m_songlength := int(read_next_bytes(f, 1)[0])

	println("m_length: $m_songlength")

	f.seek(952, .start) or { panic(err) }

	m_orders := read_next_bytes(f, 128)

	println("m_orders: $m_orders")

	// XXX load orders
	for i in 0..m_songlength {
		println("-> i: $i")

		v := player.PlayOrder(i)
		m.orders << v
	}

	// XXX create dummy/empty instruments as a placeholder,
	// XXX TODO: add option do disable this ?
	// XXX TODO: add clear_instruments and call it first ?
	for i in 0..31 {
		inst := m.add_new_instrument()
		_ = inst
	}

	{
		//panic(2)
	}

	// XXX read patterns

	f.seek(1084, .start) or { panic(err) }

	// 1024 bytes:
	// 4 bytes * 4 notes/channels x 64 (rows)
	// row-major ?
	pt_data := read_next_bytes(f, 1024)
	
	println("pt_data: $pt_data")

	n1_data := pt_data[0..4]

	read_note(n1_data)

	n2_data := pt_data[4..8]
	read_note(n2_data)

	n3_data := pt_data[8..12]
	read_note(n3_data)

	n4_data := pt_data[12..16]
	read_note(n4_data)

	// XXX
	// Try to load pattern data
	// Fixme ?

	println("")
	println("test read pattern")
	println("")

	// XXX cleanup
	m.clear_patterns()

	// XXX rewind
	f.seek(1084, .start) or { panic(err) }

	num_ch := 4

	for z in 0..m_songlength {
	println("-> z: $z")

	// XXX create new pattern
	mut pt := m.add_new_pattern()
	
	for i in 0..64 {
		println("i: $i")

		// XXX read row
		row_size := 4 * num_ch
		idx := i * row_size
		r_data := pt_data[idx .. idx+row_size]

		assert r_data.len == row_size

		for j in 0..num_ch {
			println("j: $j")
			
			// Note X data nx_data
			n_data := r_data[j*4 .. j*4+4]

			// XXX
			nx, px, _ := read_note(n_data)

			// XXX Try write data to module, fixme ?
			{
				mut have_note := false
				mut note := player.Note{}
				if px != 0 {
					note = note_period_to_note(px)
					have_note = true
				}

				// XXX write note
				

				//mut pt := m.patterns[0]
				//mut pt := m.patterns[z]

				mut t := pt.tracks[j]

				if have_note {
					//t.cells[i].note = player.get_note_opt("C-4")
					// XXX
					note_opt := player.get_note_opt_from_note(note)
					t.cells[i].note = note_opt

					t.cells[i].inst = nx
				}
			}
			
		}

		if i == 1 {
			//panic(2)
			//break
		}
	}
	}
}

// XXX read note ?
// 4 note bytes -> sample number, note period, effect
//pub fn read_note(data [4]u8) {
pub fn read_note(data []u8) (int, int, int) {
	println("read_note data: $data")

	b1 := data[0]
	b2 := data[1]
	b3 := data[2]
	b4 := data[3]

	// Sample number

	// Upper bits (mask)
	x_high := b1 & 0b11110000

	println("x_high: $x_high")

	// Lower bits (mask)
	x_low := b3 & 0b11110000

	println("x_low: $x_low")

	//x3 := x & x2
	//x3 := (x<<4) ^ x2
	nx := (x_high<<4) | (x_low>>4)

	println("nx: $nx")

	// Note period

	p_high := int(b1 & 0b00001111)
	p_low := int(b2)

	println("p_high: $p_high")
	println("p_low: $p_low")

	px := (p_high<<8) | p_low

	println("px: $px")

	// Effect

	return nx, px, -1
}

// XXX

pub fn note_period_to_note(np int) player.Note {
	println("note_period_to_note np: $np")

	mut m := map[int]player.Note{}

	// C-1
	m[856] = player.get_note("C-4")
	m[808] = player.get_note("C#4")
	m[762] = player.get_note("D-4")
	m[720] = player.get_note("D#4")
	m[678] = player.get_note("E-4")
	m[640] = player.get_note("F-4")
	m[604] = player.get_note("F#4")
	m[570] = player.get_note("G-4")
	m[538] = player.get_note("G#4")
	m[508] = player.get_note("A-4")
	m[480] = player.get_note("A#4")
	m[453] = player.get_note("B-4")

	// C-2
	m[428] = player.get_note("C-5")
	m[404] = player.get_note("C#5")
	m[381] = player.get_note("D-5")
	m[360] = player.get_note("D#5")
	m[339] = player.get_note("E-5")
	m[320] = player.get_note("F-5")
	m[302] = player.get_note("F#5")
	m[285] = player.get_note("G-5")
	m[269] = player.get_note("G#5")
	m[254] = player.get_note("A-5")
	m[240] = player.get_note("A#5")
	m[226] = player.get_note("B-5")

	// C-3
	m[214] = player.get_note("C-6")
	m[202] = player.get_note("C#6")
	m[190] = player.get_note("D-6")
	m[180] = player.get_note("D#6")
	m[170] = player.get_note("E-6")
	m[160] = player.get_note("F-6")
	m[151] = player.get_note("F#6")
	m[143] = player.get_note("G-6")
	m[135] = player.get_note("G#6")
	m[127] = player.get_note("A-6")
	m[120] = player.get_note("A#6")
	m[113] = player.get_note("B-6")

	if !(np in m) {
		panic("error: no such np: $np")
	}

	return m[np]
}


// XXX util ?

/*
func readBin(file *os.File, number int, data interface{}, name string) {
	println("readBin")
	if name != "" {
		println("name:", name)
	}
	readdata := readNextBytes(file, number)
	p("readdata:", readdata)
	buffer := bytes.NewBuffer(readdata)
	if err := binary.Read(buffer, binary.LittleEndian, data); err != nil {
		panic(err)
	}
}
*/

pub fn read_bin(file os.File, number int, mut data voidptr, name string) {
	println("read_bin")
	
	if name != "" {
		println("name: $name")
	}
	readdata := read_next_bytes(file, number)
	println("readdata: $readdata")
	//buffer := bytes.NewBuffer(readdata)

	//num := int(C.fread(data, 1, number, file.cfile))

	mut out_buffer := []u8{len: readdata.len}

	// XXX
	if number == 1 {
		//data = readdata[0]
		out_buffer = readdata.clone()
	} else if number == 2 {
		v := binary.little_endian_u16(readdata)
		println("v: $v")
		out_buffer = readdata.clone()
	} else if number == 4 {
		v := binary.little_endian_u32(readdata)
		println("v: $v")
		out_buffer = readdata.clone()
	} else if number == 8 {
		v := binary.little_endian_u64(readdata)
		println("v: $v")
		out_buffer = readdata.clone()
	} else {
		// XXX
		panic("bad number: $number; fixme?")
	}

	println("out_buffer: $out_buffer")

	// XXX ???
	// data = 1
	// unsafe {
	//    //*data = 1
	//    *data = voidptr(1)
	//    data[1] = voidptr(2)
	//    //*(data[1]) = voidptr(3)
	// }
}

/*
func readNextBytes(file *os.File, number int) []byte {
	bytes := make([]byte, number)

	getOffset := func() int64 {
		offset, err := file.Seek(0, os.SEEK_CUR)
		if err != nil {
			panic(err)
		}
		// Seek back?
		file.Seek(offset, os.SEEK_SET)
		return offset
	}

	off1 := getOffset()
	fmt.Printf("read bytes from: %d (0x%x) number: %d until: %d (0x%x)\n",
		off1, off1, number, int(off1)+number, int(off1)+number)
	_, err := file.Read(bytes)
	if err != nil {
		panic(err)
	}

	return bytes
}
*/

pub fn read_next_bytes(f os.File, number int) []u8 {
	println("read_next_bytes number: ${number}")
	//println("f: $f")

	mut bytes := []u8{len: number}

	off1 := f.tell() or {
		panic(err)
		//return
	}

	// XXX fixme: bug ?
	x1 := int(off1)+number
	x2 := int(off1)+number
	strconv.v_printf("read bytes from: %d (0x%x) number: %d until: %d (0x%x)\n",
		//off1, off1, number, int(off1)+number, int(off1)+number)
		off1, off1, number, x1, x2)
		//off1, off1, number, (number+1), number)
	//_, err := file.read_into_ptr(bytes, number)
	f.read(mut bytes) or {
		panic(err)
	}


	return bytes
}
