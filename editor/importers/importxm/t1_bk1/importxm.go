package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	//"strings"
)

var (
	p = fmt.Println
)

type XmInfo struct {
	//x int
	IDtext        [17]byte
	ModuleName    [20]byte
	_             byte
	TrackerName   [20]byte
	VersionNumber uint16
}

type XmHeader struct {
	HeaderSize          uint32
	SongLength          uint16
	RestartPosition     uint16
	NumberOfChannels    uint16
	NumberOfPatterns    uint16
	NumberOfInstruments uint16
	Flags               uint16
	DefaultTempo        uint16
	DefaultBPM          uint16
	//PatternOrderTable [256]byte // TODO
}

type XmPattern struct {
	PatternHeaderLength   uint32
	PackingType           byte   // always 0
	NumberOfRows          uint16 // 1..256
	PackedPatterndataSize uint16
}

type XmNote struct {
	// (C-0 = 0 (xm docs; real value is C-0 = 1))
	Note             byte // 0-71
	Instrument       byte // 0-128
	VolumeColumnByte byte
	EffectType       byte
	EffectParameter  byte
}

type XmInstrument struct {
	InstrumentSize  uint32
	InstrumentName  [22]byte
	InstrumentType  byte // always 0
	NumberOfSamples uint16
}

type XmSample struct {
	SampleLength       uint32
	SampleLoopStart    uint32
	SampleLoopLength   uint32
	Volume             byte
	Finetune           byte // -16..15
	Type               byte // Bit
	Panning            byte // 0-255
	RelativeNoteNumber byte // signed byte
	_                  byte // reserved
	SampleName         [22]byte
}

type XmImporter struct {
	info        XmInfo
	header      XmHeader
	patterns    []XmPattern
	patternData [][][]XmNote // use *xmnote?
	instruments []XmInstrument
	samples     [][]XmSample // inst -> sample
	//sampleData [][]int16 // raw
	sampleDataConv [][][][2]float64 // inst -> sample -> data
}

func NewXmImporter() *XmImporter {
	xi := &XmImporter{
		//samples: make([][]xmsample, 0),
	}
	//xi.info.x = 1
	return xi
}

func (xi *XmImporter) Parse(f *os.File) {
	p("Parse")

	// Info

	//info := xminfo{}
	info := &xi.info
	data := readNextBytes(f, 60)

	p("data:")
	p(data)
	p(len(data))

	buffer := bytes.NewBuffer(data)
	err := binary.Read(buffer, binary.LittleEndian, info)
	if err != nil {
		p("binary.Read failed")
		panic(err)
	}

	fmt.Printf("parsed data:\n%+v\n", info)

	// Header

	p("parsing header...")
	data = readNextBytes(f, 20) //+256)

	p("data:")
	p(data)
	p(len(data))

	buffer = bytes.NewBuffer(data)
	err = binary.Read(buffer, binary.LittleEndian, &xi.header)
	if err != nil {
		p("binary.Read failed")
		panic(err)
	}

	fmt.Printf("parsed data:\n%+v\n", xi.header)

	numInst := int(xi.header.NumberOfInstruments)
	xi.instruments = make([]XmInstrument, 0)
	//pp(numInst, xi.instruments)
	xi.samples = make([][]XmSample, 0)
	xi.sampleDataConv = make([][][][2]float64, 0)

	// Skip pattern order
	nominalHeaderSize := 20
	diff := int(xi.header.HeaderSize) - nominalHeaderSize
	//pp(diff)
	p("skip pattern order...")
	_ = readNextBytes(f, diff)

	// Patterns

	numCh := int(xi.header.NumberOfChannels)
	numPt := int(xi.header.NumberOfPatterns)
	// Init PatternData
	xi.patternData = make([][][]XmNote, numPt)

	p("parsing patterns...")
	for i := 0; i < numPt; i++ {

		p("read pattern n:", i)
		data = readNextBytes(f, 9)
		p("data:", data)
		buffer = bytes.NewBuffer(data)
		pt := XmPattern{}
		err = binary.Read(buffer, binary.LittleEndian, &pt)
		if err != nil {
			p("binary.Read failed")
			panic(err)
		}
		fmt.Printf("parsed data:\n%+v\n", pt)
		xi.patterns = append(xi.patterns, pt)

		pdSize := int(pt.PackedPatterndataSize)
		_ = pdSize
		//pd := readNextBytes(f, pdSize)
		//_ = pd

		// Decode patterndata

		//pd := &xi.patternData[i]
		numRows := int(xi.patterns[i].NumberOfRows)
		pd := make([][]XmNote, numRows)
		for i := range pd {
			pd[i] = make([]XmNote, numCh)
		}

		// Set pd
		//xi.patternData[i] = pd
		//pp(pd[0][0])

		//for i := 0; i < pdSize; i++ {
		for row := 0; row < numRows; row++ {
			for ch := 0; ch < numCh; ch++ {
				p("read note...")

				xnote := XmNote{}

				//data = pd[i : 5]
				var note byte
				//if err := binary.Read(f, binary.LittleEndian, &note); err != nil {
				//	panic(err)
				//}
				readBin(f, 1, &note, "note")
				//pp(note)
				offset := 1

				if note&0x80 != 0 {
					println("packed")
					// Packed
					if note&0x01 != 0 {
						readBin(f, 1, &xnote.Note, "p_note")
						offset++
					}
					if note&0x02 != 0 {
						readBin(f, 1, &xnote.Instrument, "p_instrument")
						offset++
					}
					if note&0x04 != 0 {
						readBin(f, 1, &xnote.VolumeColumnByte, "p_volumecolumntbyte")
						offset++
					}
					if note&0x08 != 0 {
						readBin(f, 1, &xnote.EffectType, "p_effecttype")
						offset++
					}
					if note&0x10 != 0 {
						readBin(f, 1, &xnote.EffectParameter, "p_effectparameter")
						offset++
					}
				} else {
					// Unpacked
					println("unpacked")
					xnote.Note = note
					readBin(f, 1, &xnote.Instrument, "u_instrument")
					readBin(f, 1, &xnote.VolumeColumnByte, "u_volumecolumnbyte")
					readBin(f, 1, &xnote.EffectType, "u_effecttype")
					readBin(f, 1, &xnote.EffectParameter, "u_effectparameter")
					offset += 4
					// pp(3)
				}

				pd[row][ch] = xnote

				p("row:", row, "ch:", ch)
				p("offset:", offset)
				//pv(xnote)
				//pp(2)
			}
		}

		// Set
		xi.patternData[i] = pd
		//pdump(pd)
		//pp(3)
	}

	// Instruments

	println("parsing instruments...")
	p("numInst:", numInst)
	for i := 0; i < numInst; i++ {
		inst := XmInstrument{}
		readBin(f, 29, &inst, "instrument")
		//pv(inst)
		instName := string(inst.InstrumentName[:])
		p("instName:", instName)

		xi.instruments = append(xi.instruments, inst)
		//xi.instruments[i] = inst

		//pp(2)
		numSamples := int(inst.NumberOfSamples)

		if numSamples == 0 {
			println("no samples, continue")
			continue
		}

		//xi.samples[i] = make([]xmsample, numSamples)
		//xi.samples[i] = make([]xmsample, 0)
		slot := make([]XmSample, 0)
		xi.samples = append(xi.samples, slot)

		/*
			var sampleHeaderSize uint32
			readBin(f, 4, &sampleHeaderSize, "sampleHeaderSize")
			p(sampleHeaderSize)
			pp(3)
		*/

		// Skip the second half on the instrument
		//n := 214
		n := 263 - 29 // 263 = inst.InstrumentSize
		_ = readNextBytes(f, n)
	}

	p("done Parse")

}

func readBin(file *os.File, number int, data interface{}, name string) {
	println("readBin")
	if name != "" {
		println("name:", name)
	}
	readdata := readNextBytes(file, number)
	p("readdata:", readdata)
	buffer := bytes.NewBuffer(readdata)
	if err := binary.Read(buffer, binary.LittleEndian, data); err != nil {
		panic(err)
	}
}

func readBin2(r io.Reader, data interface{}) {
	if err := binary.Read(r, binary.LittleEndian, data); err != nil {
		panic(err)
	}
}

func readNextBytes(file *os.File, number int) []byte {
	bytes := make([]byte, number)

	getOffset := func() int64 {
		offset, err := file.Seek(0, os.SEEK_CUR)
		if err != nil {
			panic(err)
		}
		// Seek back?
		file.Seek(offset, os.SEEK_SET)
		return offset
	}

	off1 := getOffset()
	fmt.Printf("read bytes from: %d (0x%x) number: %d until: %d (0x%x)\n",
		off1, off1, number, int(off1)+number, int(off1)+number)
	_, err := file.Read(bytes)
	if err != nil {
		panic(err)
	}

	return bytes
}

func main() {
	println("main()")
}
