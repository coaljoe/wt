#!/bin/bash

go run importxm.go notes.go $* 2>&1 | tee log && jsonnet /tmp/out.jsonnet > /tmp/out.json
