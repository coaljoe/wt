package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	//"strings"
)

var (
	p = fmt.Println
)

type XmInfo struct {
	//x int
	IDtext        [17]byte
	ModuleName    [20]byte
	_             byte
	TrackerName   [20]byte
	VersionNumber uint16
}

type XmHeader struct {
	HeaderSize          uint32
	SongLength          uint16
	RestartPosition     uint16
	NumberOfChannels    uint16
	NumberOfPatterns    uint16
	NumberOfInstruments uint16
	Flags               uint16
	DefaultTempo        uint16
	DefaultBPM          uint16
	//PatternOrderTable [256]byte // TODO
}

type XmPattern struct {
	PatternHeaderLength   uint32
	PackingType           byte   // always 0
	NumberOfRows          uint16 // 1..256
	PackedPatterndataSize uint16
}

type XmNote struct {
	// (C-0 = 0 (xm docs; real value is C-0 = 1))
	Note             byte // 0-71
	Instrument       byte // 0-128
	VolumeColumnByte byte
	EffectType       byte
	EffectParameter  byte
}

type XmImporter struct {
	info        XmInfo
	header      XmHeader
	patterns    []XmPattern
	// XXX row-based ?
	patternData [][][]XmNote // use *xmnote?
}

func NewXmImporter() *XmImporter {
	xi := &XmImporter{
		//samples: make([][]xmsample, 0),
	}
	//xi.info.x = 1
	return xi
}

func (xi *XmImporter) Parse(f *os.File) {
	p("Parse")

	// Info

	//info := xminfo{}
	info := &xi.info
	data := readNextBytes(f, 60)

	p("data:")
	p(data)
	p(len(data))

	buffer := bytes.NewBuffer(data)
	err := binary.Read(buffer, binary.LittleEndian, info)
	if err != nil {
		p("binary.Read failed")
		panic(err)
	}

	fmt.Printf("parsed data:\n%+v\n", info)

	// Header

	p("parsing header...")
	data = readNextBytes(f, 20) //+256)

	p("data:")
	p(data)
	p(len(data))

	buffer = bytes.NewBuffer(data)
	err = binary.Read(buffer, binary.LittleEndian, &xi.header)
	if err != nil {
		p("binary.Read failed")
		panic(err)
	}

	fmt.Printf("parsed data:\n%+v\n", xi.header)

	// Skip pattern order
	nominalHeaderSize := 20
	diff := int(xi.header.HeaderSize) - nominalHeaderSize
	//pp(diff)
	p("skip pattern order...")
	_ = readNextBytes(f, diff)

	// Patterns

	numCh := int(xi.header.NumberOfChannels)
	numPt := int(xi.header.NumberOfPatterns)
	// Init PatternData
	xi.patternData = make([][][]XmNote, numPt)

	p("parsing patterns...")
	for i := 0; i < numPt; i++ {

		p("read pattern n:", i)
		data = readNextBytes(f, 9)
		p("data:", data)
		buffer = bytes.NewBuffer(data)
		pt := XmPattern{}
		err = binary.Read(buffer, binary.LittleEndian, &pt)
		if err != nil {
			p("binary.Read failed")
			panic(err)
		}
		fmt.Printf("parsed data:\n%+v\n", pt)
		xi.patterns = append(xi.patterns, pt)

		pdSize := int(pt.PackedPatterndataSize)
		_ = pdSize
		//pd := readNextBytes(f, pdSize)
		//_ = pd

		// Decode patterndata

		//pd := &xi.patternData[i]
		numRows := int(xi.patterns[i].NumberOfRows)
		//pd := make([][]XmNote, numRows)
		pd := make([][]XmNote, numCh)
		for i := range pd {
			//pd[i] = make([]XmNote, numCh)
			pd[i] = make([]XmNote, numRows)
		}

		// Set pd
		//xi.patternData[i] = pd
		//pp(pd[0][0])

		//for i := 0; i < pdSize; i++ {
		// XXX read in 'transpose' order ?
		for row := 0; row < numRows; row++ {
			for ch := 0; ch < numCh; ch++ {
		//for ch := 0; ch < numCh; ch++ {
		//	for row := 0; row < numRows; row++ {
				p("read note...")

				xnote := XmNote{}

				//data = pd[i : 5]
				var note byte
				//if err := binary.Read(f, binary.LittleEndian, &note); err != nil {
				//	panic(err)
				//}
				readBin(f, 1, &note, "note")
				//pp(note)
				offset := 1

				if note&0x80 != 0 {
					println("packed")
					// Packed
					if note&0x01 != 0 {
						readBin(f, 1, &xnote.Note, "p_note")
						offset++
					}
					if note&0x02 != 0 {
						readBin(f, 1, &xnote.Instrument, "p_instrument")
						offset++
					}
					if note&0x04 != 0 {
						readBin(f, 1, &xnote.VolumeColumnByte, "p_volumecolumntbyte")
						offset++
					}
					if note&0x08 != 0 {
						readBin(f, 1, &xnote.EffectType, "p_effecttype")
						offset++
					}
					if note&0x10 != 0 {
						readBin(f, 1, &xnote.EffectParameter, "p_effectparameter")
						offset++
					}
				} else {
					// Unpacked
					println("unpacked")
					xnote.Note = note
					readBin(f, 1, &xnote.Instrument, "u_instrument")
					readBin(f, 1, &xnote.VolumeColumnByte, "u_volumecolumnbyte")
					readBin(f, 1, &xnote.EffectType, "u_effecttype")
					readBin(f, 1, &xnote.EffectParameter, "u_effectparameter")
					offset += 4
					// pp(3)
				}

				//pd[row][ch] = xnote
				pd[ch][row] = xnote

				p("row:", row, "ch:", ch)
				p("offset:", offset)
				//pv(xnote)
				//pp(2)
			}
		}

		// Set
		xi.patternData[i] = pd
		//pdump(pd)
		//pp(3)
	}

	p("done Parse")

}

func readBin(file *os.File, number int, data interface{}, name string) {
	println("readBin")
	if name != "" {
		println("name:", name)
	}
	readdata := readNextBytes(file, number)
	p("readdata:", readdata)
	buffer := bytes.NewBuffer(readdata)
	if err := binary.Read(buffer, binary.LittleEndian, data); err != nil {
		panic(err)
	}
}

func readBin2(r io.Reader, data interface{}) {
	if err := binary.Read(r, binary.LittleEndian, data); err != nil {
		panic(err)
	}
}

func readNextBytes(file *os.File, number int) []byte {
	bytes := make([]byte, number)

	getOffset := func() int64 {
		offset, err := file.Seek(0, os.SEEK_CUR)
		if err != nil {
			panic(err)
		}
		// Seek back?
		file.Seek(offset, os.SEEK_SET)
		return offset
	}

	off1 := getOffset()
	fmt.Printf("read bytes from: %d (0x%x) number: %d until: %d (0x%x)\n",
		off1, off1, number, int(off1)+number, int(off1)+number)
	_, err := file.Read(bytes)
	if err != nil {
		panic(err)
	}

	return bytes
}

// Transpose pattern data
func transposePd(pd [][]XmNote) [][]XmNote {
	p("transposePd")

	lenX := len(pd)
	lenY := len(pd[0])

	p("lenX:", lenX)
	p("lenY:", lenY)

	// Make result matrix
	r := make([][]XmNote, len(pd[0]))
    for x, _ := range r {
        r[x] = make([]XmNote, len(pd))
    }

	// Transpose(?)
	for y, s := range pd {
        for x, e := range s {
            r[x][y] = e
        }
    }

	p("done transposePd")

	return r
	
	/*
	for i := 0; i < lenY; i++ {
		for j := 0; j < lenX; j++ {
			pd[j][i] = pd[i][j]
		}
	}

	return pd
	*/
}

func main() {
	println("main()")

	l := NewXmImporter()
	//f, err := os.Open("/tmp/black-720.xm")
	//f, err := os.Open("/tmp/black-720_1.xm")
	//f, err := os.Open("/tmp/120beat.xm")
	//f, err := os.Open("/tmp/bother_ed.xm")

	if len(os.Args) < 2 {
		println("error: bad args")
		panic(1)
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	l.Parse(f)

	//p(l.patterns)
	//p(l.patternData)

	//nd := l.patternData[0]

	// XXX ?
	numCh := int(l.header.NumberOfChannels)
	// XXX
	//numCh = 1
	//numCh = 5
	//println("numCh:", numCh)
	//println()
	//panic(3)

	moduleHdrStartFmt := `
{
	"module": {
		"title": "120beat",
		//"bpm": 120,
		"bpm": %d,
		//"num_tracks": 1
		"num_tracks": %d
	},
		
	"instruments": [
		{
			"num": 1, // XXX FIXME
			"name": "Instrument 1",
			"volume": 64,
			"loop": false,
			"num_gens": 1,
			"gens": [
				// Gens data
				{
					"type": "square",
					//"volume": 64,
					"volume": 8,
					"panning": 0,
					//"base_note": 48,
					//"tune": 127,
					//"freq": 100,
					//"freq": 1000,
					"freq": 440,
				},
			],

			// Volume env
			"envelope_volume": {
				"enabled": true,
				"loop_enabled": false,
				"loop_points": [1, 2],
				"loop_type": "forward",
				"sustain_enabled": true,
				"sustain_point": 1,
				"data": [
					[0, 64],
					[4, 20],
					[10, 5],
					//[20, 2]
					[20, 0]
				]
			},
		}
	],


	"orders": [0],
		
	"patterns": [

`
	moduleHdrEnd := "]}"

	moduleHdrStart := fmt.Sprintf(moduleHdrStartFmt, l.header.DefaultBPM, numCh)

	patternHdrStartFmt := "{\n" +
		//"\t\"nrows\": 64,\n" +
		"\t\"__meta__pattern_id\": %d,\n" +
		"\t\"num_rows\": %d,\n" +
		"\t\"tracks\":\n" +
		"\t[\n"// +
		//"\t\t{\n" //+
		//"\t\t\t\"cells\": [\n"
	patternHdrEnd := "\n" +
		"\t\t]\n" +
		"\t\t},\n"
		//"\t\t}\n" +
		//"\t]\n" +
		//"},"
	_ = patternHdrStartFmt
	_ = patternHdrEnd
	trackHdrStart := "\t\t{\n" +
		"\t\t\t\"cells\": [\n"
	//trackHdrStart := "\t\t\t[\n"
	trackHdrEnd := "\t\t\t],\n" +
		"\t\t},\n"
		//"\t\t}\n"
	_ = trackHdrStart
	_ = trackHdrEnd

	numPt := int(l.header.NumberOfPatterns)
	//numPt := 1
	//numPt := 2
	//numPt := 0
	//numRows := 64
	//ptIdx := 0
	//ptIdx := 1
	//ptIdx := 3
	//znumRows := int(l.patterns[ptIdx].NumberOfRows)

	//p("znumRows:", znumRows)
	p()
	//panic(2)

	outS := ""
	outS += moduleHdrStart
	//outS += patternHdrStart
	//outS += fmt.Sprintf(patternHdrStartFmt, znumRows)

	//procData := l.patternData.Copy()
	// Init
	//numPt := 1
	//numRows := 64
	//numCh := 2
	//numCh := 4
	procData := make([][][]XmNote, numPt)
	_ = procData
	/*
	pd := make([][]XmNote, numRows)
	for i := range pd {
		pd[i] = make([]XmNote, numCh)
	}
	*/
	var _ = `
	p("creating procData...")
	for z := 0; z < numPt; z++ {
		///*
		p("creating pd...")
		p("numCh:", numCh)
		pd := make([][]XmNote, numCh)
		for i := range pd {
			// XXX variable ?
			xnumRows := int(l.patterns[i].NumberOfRows)
			p("xnumRows:", xnumRows)
			pd[i] = make([]XmNote, xnumRows)
		}
		//*/
		p(pd[0][0])
		//p(pd[0][1])
		//p(pd[1][0])
		//p(pd[2][0])
		/*
		for i := 0; i < numRows; i++ {
			for j := 0; j < numCh; j++ {
				pd[i][j] = XmNote{}
			}
		}
		*/
		//p("pd:")
		//p(pd)

		p(pd[0][0])
		//p(pd[0][1])
		//p(pd[1][0])
	
		//panic(3)
		//procData[0] = pd
		procData[z] = pd
	}
	//panic(2)
	

	p("l.patternData:")
	// Pt
	p("len pt:", len(l.patternData))
	// Rows
	p("len pt0 Rows:", len(l.patternData[0]))
	// Cols (Ch)
	p("len pt0 Cols:", len(l.patternData[0][0]))
	
	p("len pt1 Rows:", len(l.patternData[1]))
	p("len pt1 Rows:", len(l.patternData[1][0]))

	p("procData:")
	// Pt
	p("len pt:", len(procData))
	// Rows
	p("len pt0 Rows:", len(procData[0]))
	// Cols (Ch)
	p("len pt0 Cols:", len(procData[0][0]))

	p("len pt1 Rows:", len(procData[1]))
	p("len pt1 Cols:", len(procData[0][1]))

	//panic(2)
	`

	p()
	p("transpose...")
	// XXX try to transpose note data ?
	// doesn't work/fixme ?
	for z := 0; z < numPt; z++ {

		/*
		pd := procData[z]
		newPd := transposePd(pd)
		_ = newPd
		
		panic(2)
		*/

		/*
		// XXX variable ?
		xnumRows := int(l.patterns[z].NumberOfRows)
		println("xnumRows:", xnumRows)
		for i := 0; i < xnumRows; i++ {
			for j := 0; j < numCh; j++ {
				println("z:", z, "i:", i, "j:", j)
				//n[i][j] = pd[j][i]
				//procData[z][i][j] = l.patternData[z][i][j]
				//procData[z][i][j] = l.patternData[z][j][i]
				p("z:", z)
				p("b:", l.patternData[z][i][j])
				p("a:", procData[z][j][i])
				// Transpose
				procData[z][j][i] = l.patternData[z][i][j]
				//p(procData[z][j][i])
				//p(procData[0][1][1])
				//p(procData[z][i][j])
				//p(l.patternData[z][j][i])
			}
		}
		*/
	}


	//panic(2)


	//for trackIdx, nd := range l.patternData {
	//for trackIdx, nd := range l.patternData[0] {
	//for trackIdx, nd := range l.patternData[ptIdx] {
	//numPt = 1
	for z := 0; z < numPt; z++ {

	// ptIdx
	znumRows := int(l.patterns[z].NumberOfRows)
	outS += fmt.Sprintf(patternHdrStartFmt, z, znumRows)
	
	for trackIdx, nd := range l.patternData[z] {
	//for trackIdx, nd := range procData {
	//for trackIdx, nd := range procData[0] {
		p("trackIdx", trackIdx)
		p("nd:", nd)

		outS += trackHdrStart

		r := nd
		//for _, r := range nd {
			p("r:", r)
			// Note record
			for _, n := range r {
				//p("r:", r)
				p("n:", n)

				var xn int

				skip := false
				if n.Note == 0 {
					skip = true
				} else {
					xn = int(n.Note) - 1
					// XXX pass, fixme?
					if xn > 95 {
						skip = true
					}
				}

				if skip {
					outS += "\t\t\t\t[null, null, null, null],\n"
					continue
				}

				z := GetNoteByNum(xn)
				p("z:", z)

				xi := int(n.Instrument)
				_ = xi

				// XXX remapt instrumet number to 1
				xi = 1

				// XXX out not rec
				//s := "\n"
				s := ""
				s += "\t\t\t\t["
				s += "\"" + z.Name + "\""
				s += fmt.Sprintf(", %d", xi)
				// Vol
				if true {
					vol_byte := int(n.VolumeColumnByte)
					if vol_byte < 0x10 || vol_byte > 0x50 {
						// XXX skip vol
						s += ", null"
					} else {
						// 0x10-0x50 range
						vol := vol_byte - 0x10
						//p("vol", vol)
						//panic("2")
						s += fmt.Sprintf(", %d", vol) 
					}
				} else {
					s += ", null"
				}
				//s += ", null, null],"
				s += ", null],"
				s += "\n"

				outS += s
			}
		//}

		outS += trackHdrEnd
	}
	outS += patternHdrEnd
	}

	outS += moduleHdrEnd

	p("outS:")
	p(outS)

	file, err := os.Create("/tmp/out.jsonnet")
	if err != nil {
		panic(err)
	}

	file.WriteString(outS)
	file.Close()
}
