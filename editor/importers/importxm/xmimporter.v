module importxm

import player
//import editor

pub struct XmImporter {
pub mut:
	//ctx_editor &editor.Context
	ctx_player &player.Context
}

pub fn new_xm_importer(ctx_player &player.Context) &XmImporter {
	im := &XmImporter{
		ctx_player: ctx_player,
	}
	return im
}

pub fn (mut im XmImporter) load(path string) &player.Module {
	// XXX create module
	num_tracks := 2
	mut m := player.new_module(mut im.ctx_player, num_tracks)

	return m
}
