module main

import player

[heap]
pub struct PatternView {
pub mut:
	m &player.Pattern
	panel C.voidptr // xg.Panel fixme?
	hl_row_idx int
	prev_hl_row_idx int
	trackviews []&TrackView
	ctx &Context
}

pub fn new_patternview(ctx &Context, m &player.Pattern) &PatternView {
	println("new_patternview")
	
	pv := &PatternView{
		m: m,
		ctx: ctx,
	}

	return pv
}

pub fn (mut v PatternView) @spawn() {

	my_sheet := C.xg_sheetsys_create_sheet(c"panel_test")
	C.xg_sheetsys_add_sheet(my_sheet)

	rect := C.xg_Rect{0, 0, 500, 500}
	panel := C.xg_new_panel(rect)

	root := C.xg_sheet_root(my_sheet)
	C.xg_widget_add_child(root, panel)

	v.panel = panel

	track_width := 100
	
	//n := 2
	n := 3

	for i in 0..n {
		tr := v.m.tracks[i]
		mut tv := new_trackview(v.ctx, v, tr)
		tv.@spawn()

		v.trackviews << tv
	}
}

pub fn (mut v PatternView) update(dt f32) {
	println("PatternView update")

	//panic(3)
	//pp(3)

	{
	//return
	}

	pl := v.ctx.editor.pl

	// XXX
	v.hl_row_idx = v.ctx.editor.pl.row

	println("v.hl_row_idx: $v.hl_row_idx")
	println("v.prev_hl_row_idx: $v.prev_hl_row_idx")

	/*
	cur_pt, cur_pt_pos := pl.get_pattern_by_pos_idx(pl.row)

	if cur_pt == v.m {
		if cur_pt_pos > 63 {
			// XXX FIXME not supported
			println(cur_pt_pos)
			panic(5)
			return
		}
	}
	*/

	pt_cur_row := pl.get_current_row_for_pattern(v.m)

	if pt_cur_row != -1 {
		if pt_cur_row > 63 {
			// XXX FIXME not supported
			println(pt_cur_row)
			panic(5)
			return
		}
	}

	// XXX currently not supported
	{
		if v.hl_row_idx > 14 {
			return
		}
	}

	if v.hl_row_idx != v.prev_hl_row_idx {

		for tv in v.trackviews {
			//if ctx.player.row() == i {
			mut cv := tv.cellviews[v.hl_row_idx]
			cv.hl_cell = true
			//cv.hlPanel.SetVisible(true) //ToggleVisible()
			//cv.hlPanel.Style = tut.gui.style.stPlayingRowHlPanel
			// Style
			//w := C.xg_label_pget_widget(cv.hl_panel)
			//st := C.xg_widget_pget_style(w)
			//C.xg_style_set_style(st, v.ctx.gui.style.st_hl_playing_row)

			//st := C.xg_widget_pget_style(cv.hl_panel)
			//C.xg_style_set_style(st, v.ctx.gui.style.st_hl_playing_row)

			//C.xg_label_set_text(cv.effect_label, c"---")
			C.xg_label_set_text(cv.effect_label, c"--------------------------------12")
			//C.xg_style_set_style(st, st_bg)

			//mut prev_cv := tv.cellviews[v.prev_hl_row_idx]
			//prev_cv.hl_cell = false
			//prevCv.hlRowPanel.SetVisible(false)
			//prevCv.hlPanel.Style = prevCv.hlDefaultStyle
			//C.xg_style_set_style(st, st_bg)
			//}

			//panic(4)
		}
		
		// XXX return state of prev position
		{
			for tv in v.trackviews {
				mut cv := tv.cellviews[v.prev_hl_row_idx]
				cv.hl_cell = true
				C.xg_label_set_text(cv.effect_label, c"---")
			}
		}

		/*
		// Try center view
		// XXX FIXME TODO
		if v.hlRowIdx > 10 {
			v.scrollDown(1)
		}
		*/
	}

	v.prev_hl_row_idx = v.hl_row_idx
}

pub fn (v &PatternView) render() {
	
}
