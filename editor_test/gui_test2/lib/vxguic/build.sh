#!/bin/bash

#v -showcc -shared -cg .
#v -autofree -showcc -shared -cg -g -cstrict . -cc gcc
v -autofree -showcc -shared -g -cstrict . -cc gcc -gcflags "-N"
