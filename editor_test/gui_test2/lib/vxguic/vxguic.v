module vxguic

#pkgconfig sdl2
#pkgconfig SDL2_image
#pkgconfig SDL2_ttf
//#flag -I /mnt/t/dev/test/wt/editor/gui_test2/lib/xguicz
//#flag -Ixguic
//#flag -Lxguic
#flag -I/home/k/dev/xgui_c/xguic
#flag -L/home/k/dev/xgui_c/xguic
//#flag -l xguic
// XXX
#flag -l:xguic.a
//#flag -lxguic
//#flag -lxguic.a

#include "xguic.h"
//#include "xguic/xguic.h"
#include "stdbool.h"

/*
struct C.Pos {
	x int
	y int
}
*/

/*
pub struct Pos {
mut:
	x int
	y int
}
*/


[typedef]
struct C.xg_Pos {
mut:
	x int
	y int
}

[typedef]
pub struct C.xg_Rect {
mut:
	x int
	y int
	w int
	h int
}

[typedef]
pub struct C.xg_Color {
mut:
	//r uint8_t
	//r u8
	r byte
	g byte
	b byte
	a byte
}

pub struct LabelStyle {
pub mut:
	text_color voidptr 
}

pub struct PanelStyle {
pub mut:
	draw_as_rect bool
	draw_rect_color voidptr
	draw_rect_border_width int
	draw_rect_border_color voidptr
}

//type Label = C.int
//type Label = C.Label

/*
[typedef]
pub struct C.Label {
}
*/

fn C.xg_create_xgui_and_sdl_app(width int, height int, fullscreen bool)

//fn C.xg_app_step() C.bool
//fn C.xg_app_step() C.Bool
//fn C.xg_app_step() C.uint8
fn C.xg_app_step() int
//fn C.xg_app_step()


fn C.xg_make_pos(x int, y int) C.xg_Pos
//fn C.xg_pos(x int, y int) Pos

fn C.xg_make_rect(x int, y int, w int, h int) C.xg_Rect

fn C.xg_new_label(pos C.xg_Pos, text charptr) voidptr
// XXX Deprecated
//fn C.xg_label_pget_style(la_ptr voidptr) voidptr
//fn C.xg_label_pset_style(la_ptr voidptr, label_style &LabelStyle) voidptr
fn C.xg_label_pget_widget(la_ptr voidptr) voidptr
//fn C.xg_label_set_text(la_ptr C.voidptr, text C.charptr)
fn C.xg_label_set_text(la_ptr voidptr, text charptr)

fn C.xg_new_button(rect C.xg_Rect) voidptr
fn C.xg_button_pget_label(b_ptr voidptr) voidptr
// XXX Deprecated
//fn C.xg_button_pget_style(b_ptr voidptr) voidptr

fn C.xg_new_panel(rect C.xg_Rect) voidptr
fn C.xg_panel_pget_label(p_ptr voidptr) voidptr
fn C.xg_panel_widget_style(p_ptr voidptr) voidptr
//fn C.xg_panel_pset_style(p_ptr voidptr, panel_style &PanelStyle) voidptr

//fn C.xg_sheetsys_create_sheet(name C.charptr) C.voidptr
//fn C.xg_sheetsys_create_sheet(name charptr) voidptr
fn C.xg_sheetsys_create_sheet(name &u8) voidptr
fn C.xg_sheetsys_add_sheet(sh_ptr voidptr)
fn C.xg_sheet_root(sh_ptr voidptr) voidptr

fn C.xg_widget_add_child(w_ptr voidptr, child_ptr voidptr)
//fn C.xg_widget_set_visible(w_ptr voidptr, val C.GoUint8)
//fn C.xg_widget_set_active(w_ptr voidptr, val C.GoUint8)
fn C.xg_widget_set_visible(w_ptr voidptr, val bool)
fn C.xg_widget_set_active(w_ptr voidptr, val bool)
fn C.xg_widget_rect(w_ptr voidptr) C.xg_Rect
fn C.xg_widget_pget_style(la_ptr voidptr) voidptr

// Style
fn C.xg_new_style() voidptr
fn C.xg_style_set_text_color(st_ptr voidptr, color C.xg_Color)
fn C.xg_style_get_style(st_ptr voidptr) voidptr
fn C.xg_style_set_style(st_ptr voidptr, st_other_ptr voidptr)
fn C.xg_style_pget_panel_style(st_ptr voidptr) voidptr

// PanelStyle
fn C.xg_new_panel_style() voidptr
fn C.xg_panel_style_set_draw_type_rect(p_st_ptr voidptr)
fn C.xg_panel_style_pget_rect_style(p_st_ptr voidptr) voidptr

// RectStyle
fn C.xg_rect_style_set_color(p_ptr voidptr, color C.xg_Color)

fn C.xg_load_font(font_name charptr, size int) voidptr
fn C.xg_set_font(font_name charptr, f_ptr voidptr)
fn C.xg_font_set_aa(f_ptr voidptr, val bool)

pub fn test() {
	println("xgui: test")

	C.xg_create_xgui_and_sdl_app(800, 600, false)
	C.xg_app_step()
}
