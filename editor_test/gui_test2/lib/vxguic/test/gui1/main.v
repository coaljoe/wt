module main

import vxguic as xg

fn main() {
	println("main()")

	C.xg_create_xgui_and_sdl_app(800, 600, false)

	// XXX code

	my_sheet := C.xg_sheetsys_create_sheet(c"test")
	C.xg_sheetsys_add_sheet(my_sheet)

	pos := C.xg_Pos{0, 20}
	_ = pos
	//pos := C.xg_pos(0, 20)
	
	debug_text := C.xg_new_label(pos, c"debugText")
	///*

	C.xg_widget_set_visible(debug_text, false)
	C.xg_label_set_text(debug_text, c"")


	root := C.xg_sheet_root(my_sheet)
	C.xg_widget_add_child(root, debug_text)

	
	C.xg_label_set_text(debug_text, c"Debug text")
	C.xg_widget_set_visible(debug_text, true)
	//*/

	for int(C.xg_app_step()) != 0 {
		//println("step")		
	}

	println("exiting...")

	println("done")
}
