module main

//import xguic as xg
import vxguic as xg

fn main() {
	println("main()")

	//p := xg.Pos{}
	//println("p: $p")
	//xg.xg_create_xgui_and_sdl_app(800, 600, false)
	//xg.test()

	C.xg_create_xgui_and_sdl_app(800, 600, false)
	
	for {
		ret_c := C.xg_app_step()
		ret := int(ret_c)
		println("ret: $ret")

		if ret <= 0 {
			println("exiting...")
			exit(0)
		}

		
	}

	println("done")
}
