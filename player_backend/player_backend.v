module player_backend

import math
import sokol.audio
import common as c

import player as p
//import common as c

[inline]
fn sintone(periods int, frame int, num_frames int) f32 {
	//println("periods: $periods, frame: $frame, num_frames: $num_frames")
	return math.sinf(f32(periods) * (2 * math.pi) * f32(frame) / f32(num_frames))
}

/*
fn my_audio_stream_callback(buffer &f32, num_frames int, num_channels int) {
	println("callback: num_frames: $num_frames, num_channels: $num_channels")

	//ms := sw.elapsed().milliseconds() - sw_start_ms
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
			//idx := frame * num_channels + ch
			//idx := frame * num_channels
			idx := frame
			//for ch := 0; ch < num_channels; ch++ {
			//	idx := frame * num_channels + ch
				//if ms < 250 {
				//	soundbuffer[idx] = 0.5 * sintone(20, frame, num_frames)
				//} else if ms < 300 {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//} else if ms < 1500 {
				//	soundbuffer[idx] *= sintone(22, frame, num_frames)
				//} else {
				//	soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
				//}
				
			//soundbuffer[idx] = 0.5 * sintone(25, frame, num_frames)
			soundbuffer[idx] = 0.5 * sintone(100, frame, num_frames)
			//}
		}
	}
}
*/


// XXX TODO debug:
// XXX dump buffer with zero padded data at the end to disk
// XXX trim it to a specific size (of the rendered file) then compare file checksums
// XXX TODO make this a function for reuse in the player? like xmp_play_buffer(?)
//fn my_audio_stream_callback1(buffer &f32, num_frames int, num_channels int, user_data voidptr) {
//fn my_audio_stream_callback1(buffer &f32, num_frames int, num_channels int, mut pl p.Player) {
pub fn my_audio_stream_callback1(buffer &f32, num_frames int, num_channels int, mut pl p.Player) {
	println("callback1: num_frames: $num_frames, num_channels: $num_channels")

	/*
	println("pl:")
	mut pl := &p.Player(user_data)
	*/
	println("pl.tick:")
	println(pl.tick)
	//panic("3")

	/*
	if !pl.playing {
		println("callback: not playing, exiting...")
		return
	}
	*/

	/*
	if pl.cb_done {
		println("cb callback: done, returning...")
		return
	}
	*/

	//println("pl.cb_done: $pl.cb_done")
	
	if pl.cb_done {
		println("cb callback: done, returning...")
		return
	}
	

	mut out := []f32{}
	mut ret := false
	_ = ret

	//if ret == false {
	if !pl.playing {
		if pl.render_buffer_rest.len != 0 {
			println("DERP")
			println("overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")
			println("len rest pl.render_buffer_rest: ${pl.render_buffer_rest.len}")

			n := pl.render_buffer_rest.len / num_frames
			println("n: $n")
			
			//out << pl.render_buffer_rest
			//pl.render_buffer_rest = []

			if n > 0 {
				// Feed some
				out << pl.render_buffer_rest[..num_frames]
				// ltrim
				pl.render_buffer_rest = pl.render_buffer_rest[num_frames..]
			} else {
				//out << pl.render_buffer__rest
				mut buf := pl.render_buffer_rest.clone()
				if buf.len < num_frames {
					// XXX pad buffer with zeroes... (?)
					diff := num_frames - buf.len
					println("diff: $diff")
					buf << p.silence(diff)
				}
				println("len buf: $buf.len")
				out << buf
				pl.render_buffer_rest = []
			}
			
			//panic("2")
		} else {
			println("HERP")
			//panic("2")
			out = p.silence(num_frames) // ?
			pl.cb_done = true
			//return
		}
	} else {

		//println("?0 pos pl.sample_pos: ${pl.sample_pos}")
		//println("?0 overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")

		out, ret = pl.render_buf(num_frames)

		//println("ret: $ret")
	}

	/*
	//if out.len == 0 {
	if !pl.playing {
		pl.cb_done = false
	}
	*/

	//println("x len out: ${out.len}")
	//panic("3")

	//println("pl.playing: ${pl.playing}")
	//println("pl.cb_done: ${pl.cb_done}")
	//println("?pos pl.sample: ${pl.sample_pos}")
	//println("?overshot pl.render_buffer_overshot: ${pl.render_buffer_overshot}")
	

	///*
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
		
			idx := frame
			
			soundbuffer[idx] = out[idx]
		}
	}
	//*/

	// XXX not used
	pl.cb_pos += out.len
	//println("pl.cb_pos: $pl.cb_pos")

	//if !pl.playing {
	//		println("callback: not playing, exiting...")
	//		return
	//}
}


// XXX
// TODO: add more options
// TODO: add custom callback option ?
pub fn init_backend(mut pl p.Player) bool {
	println("player_backend init_backend")

	// XXX open audio
	audio.setup(
		num_channels: 1,
		 // XXX bad value: not PoT
		//buffer_frames: 6000,
		//buffer_frames: 4096,
		buffer_frames: 8192,
		//stream_cb: my_audio_stream_callback

		stream_userdata_cb: my_audio_stream_callback1,
		user_data: pl,
	)

	return true
}

//pub fn push_audio2(frames &f32, num_frames int) int {
pub fn push_audio2(frames []f32, num_frames int) int {
	println("player_backend push_audio2 frames: [buf], num_frames: $num_frames")

	/*
	unsafe {
		mut soundbuffer := buffer
		for frame := 0; frame < num_frames; frame++ {
		
			idx := frame
			
			soundbuffer[idx] = out[idx]
		}
	}
	*/

	//ret := audio.push(frames, num_frames)
	ret := audio.push(&frames[0], num_frames)
	
	println("ret: $ret")
	
	// XXX debug
	if true {
		// XXX FIXME: should touch file with c.touch(outf) first ?
		println("dumping...")
		c.dump_raw_append("/tmp/wt_debug_push_audio_out.raw", frames)
	}

	return ret
}

pub fn get_buffer_frames2() int {
	println("player_backend get_buffer_frames2")

	ret := audio.buffer_frames()
	
	return ret
}

pub fn expect2() int {
	println("player_backend expect2")

	ret := audio.expect()
	
	return ret
}

// A non-callback push version test ?
pub fn init_backend2(conf BackendConfig) bool {
	println("player_backend init_backend2 conf: $conf")

	println("... init v's audio backend")

	// XXX open audio
	audio.setup(
		num_channels: 1,
		 // XXX bad value: not PoT
		//buffer_frames: 6000,
		//buffer_frames: 4096,
		//buffer_frames: 8192,
		buffer_frames: conf.num_frames,
	)
	
	println("... done")
	
	println("... test if audio is valid")
	if !audio.is_valid() {
		return false
	} 
	
	println("... audio is valid")
	
	println("... all done?")
	
	return true
}


pub fn deinit_backend() {
	println("player_backend deinit_backend")

	audio.shutdown()
}

[params]
pub struct BackendConfig {
	// Buffer frames (buffer size in frames)
	num_frames int = 8192
}
