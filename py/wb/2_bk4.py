#!/usr/bin/env python
#from wavebender import *
import math
from math import cos
import struct
from itertools import *


sr = 44100

def playSquare(freq, amp, time):
    channels = ((square_wave(freq, amplitude=amp),),)

    samples = compute_samples(channels, sr * time)
    #return samples
    write_wavefile(stdout, samples, sr * time, nchannels=1)


def genSquare(freq, amp, time):
    t = 0
    for s in square_wave(freq, amplitude=amp):
        t += 1
        print("t:", t)
        yield s

def sine_wave(frequency=440.0, framerate=44100, amplitude=0.5):
    if amplitude > 1.0: amplitude = 1.0
    if amplitude < 0.0: amplitude = 0.0
    return (float(amplitude) * math.sin(2.0*math.pi*float(frequency)*(float(i)/float(framerate))) for i in count(0))

def white_noise(amplitude=0.5):
    return (float(amplitude) * random.uniform(-1, 1) for _ in count(0))


def square_wave(frequency=440.0, framerate=44100, amplitude=0.5):
    for s in sine_wave(frequency, framerate, amplitude):
        if s > 0:
            yield amplitude
        elif s < 0:
            yield -amplitude
        else:
            yield 0.0

def silence():
    return (float(0) for _ in count(0))

    
# precalc noise
noise = cycle(islice(white_noise(), 44100))

gDef = """
g1: 
G[440, 0.5, 1]
D[0.2]
G[220, 0.5, 1]

g2:
G[220, 0.5, 1]

"""

___gDef = """
g1: 
G[440, 0.5, 1]
D[0.2]
G[220, 0.5, 1]

"""

commandQ = {}

def parseDef(idx, d):
    print("parseDef")
    for line in d:
        line = line.replace('[', ' ')
        line = line.replace(']', ' ')
        line = line.replace(',', '')
        line = line.strip()
        print("line:", line)
        if idx not in commandQ:
            commandQ[idx] = []
        commandQ[idx].append(line)
    pass

# Parse defs
record = False
d = []
datas = []
for line in gDef.splitlines():
    line = line.strip()
    print("l:", line)
    if line.endswith(':'):
        print("derp")
        record = True
        continue

    if line == "" or line.isspace():
        if record:
            print("d:")
            print(d)
            print("END")
            datas.append(d)
            d = []
            record = False

    if record:
        d.append(line)

print("datas:", datas)

print()
for idx, d in enumerate(datas):
    parseDef(idx, d)

print()
print(commandQ)

commandData = {}

print()
print("done")

channels = []

for idx, cmds in commandQ.items():
    print("idx:", idx, "cmds", cmds)
    #channels.append( (genSquare(440, 0.5, 1),) )
    #channels.append( (sine_wave(300.0, amplitude=0.1),) )

    commandData[idx] = []
    
    for cmd in cmds:
        cmdP = cmd.split()

        if cmdP[0] == "G":
            print("processing:", cmdP)
            freq = float(cmdP[1])
            amp = float(cmdP[2])
            time = float(cmdP[3])
            #channels.append( (genSquare(freq, amp, 1),) )

            pl = int(sr * time)
            data = list(islice(square_wave(freq, sr, amp), pl))
            commandData[idx].extend(data)

        if cmdP[0] == "D":
            print("processing", cmdP)
            time = float(cmdP[1])

            pl = int(sr * time)
            data = list(islice(silence(), pl))
            commandData[idx].extend(data)


pl = int(44100*16*0.3)
#samples = compute_samples(channels, None)
silence = list(islice(repeat(0),pl))
#print("silince:", silence)

#print("commandData:", commandData)

#exit()

#channels = (
#            #(cycle(commandData[0])), None
#            #(cycle(chain(silence, silence))), None
#            (cycle(silence)),
#           )
#
#samples = compute_samples(channels, sr * 1)

mixed_data = []

dataLen = len(commandData[0]) # fixme

for i in range(dataLen):
    v = 0
    for idx in commandData.keys():
        try:
            v += commandData[idx][i]
        except IndexError: # fixme
            #print("i:", i)
            #exit()
            pass

    mixed_data.append(v)

print("dataLen:", dataLen)
print("mixed_data len:", len(mixed_data))
#exit()

print(commandData[1])

#write_wavefile(stdout, samples, None)
f = open("/tmp/out.raw", "wb")
#write_wavefile(f, samples, nchannels=1)
#f.write(commandData[0])
#d = commandData[0]
#d = commandData[1]
d = mixed_data
#print("x:", len(d), type(d))
#exit()
for v in d:
    #print("v:", v)
    #print("test")
    #exit()
    ba = bytearray(struct.pack("f", v))
    f.write(ba) 
f.close()
