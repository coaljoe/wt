#!/usr/bin/env python
from wavebender import *
from math import cos
from itertools import *


sr = 44100

def playSquare(freq, amp, time):
    channels = ((square_wave(freq, amplitude=amp),),)

    samples = compute_samples(channels, sr * time)
    #return samples
    write_wavefile(stdout, samples, sr * time, nchannels=1)


def genSquare(freq, amp, time):
    t = 0
    for s in square_wave(freq, amplitude=amp):
        t += 1
        print("t:", t)
        yield s

_gDef = """
g1: 
G[440, 0.5, 1]
D[0.2]
G[220, 0.5, 1]

g2:
G[220, 0.5, 1]

"""


gDef = """
g1: 
G[440, 0.5, 1]
D[0.2]

"""

commandQ = {}

def parseDef(idx, d):
    print("parseDef")
    for line in d:
        line = line.replace('[', ' ')
        line = line.replace(']', ' ')
        line = line.replace(',', '')
        line = line.strip()
        print("line:", line)
        if idx not in commandQ:
            commandQ[idx] = []
        commandQ[idx].append(line)
    pass

# Parse defs
record = False
d = []
datas = []
for line in gDef.splitlines():
    line = line.strip()
    print("l:", line)
    if line.endswith(':'):
        print("derp")
        record = True
        continue

    if line == "" or line.isspace():
        if record:
            print("d:")
            print(d)
            print("END")
            datas.append(d)
            d = []
            record = False

    if record:
        d.append(line)

print("datas:", datas)

print()
for idx, d in enumerate(datas):
    parseDef(idx, d)

print()
print(commandQ)

commandData = {}

print()
print("done")

channels = []

for idx, cmds in commandQ.items():
    print("idx:", "cmds", idx, cmds)
    #channels.append( (genSquare(440, 0.5, 1),) )
    #channels.append( (sine_wave(300.0, amplitude=0.1),) )

    commandData[idx] = []
    
    for cmd in cmds:
        cmdP = cmd.split()

        if cmdP[0] == "G":
            freq = float(cmdP[1])
            amp = float(cmdP[2])
            time = float(cmdP[3])
            #channels.append( (genSquare(freq, amp, 1),) )

            pl = 100
            data = islice( genSquare(freq, amp, 1), pl)
            commandData[idx].append(data)
            


pl = int(44100*16*0.3)
#samples = compute_samples(channels, None)
silence = islice(repeat(0),pl)
print("silince:", silence)

print("commandData:", commandData)

channels = (
            #(cycle(commandData[0])), None
            #(cycle(chain(silence, silence))), None
            (cycle(silence)),
           )

samples = compute_samples(channels, sr * 1)

#write_wavefile(stdout, samples, None)
f = open("/tmp/out.wav", "wb")
write_wavefile(f, samples, nchannels=1)
f.close()
