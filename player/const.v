module player

// XXX fixme
pub const (
	c_sr = 44100
	//c_num_rows = 64
	// Default new pattern length ? (rows)
	c_default_pattern_length = 64
	// Ticks per row, or `speed`
	// is this value fixed/constant or tunable?
	c_ticks_per_row = 6
	c_rows_per_beat = 4

	c_max_vol = 64 // num

	// XXX?
	c_max_env_x = 256 // num
	// XXX same as c_max_vol
	c_max_env_y = 64 // num

	// Envelope step
	//c_env_tick_size = 16 // samples
	c_env_default_data_range = c_max_vol

	// Default mixing volume / amp for channels
	c_ch_mix_vol = 0.8

	// XXX test
	c_inst_not_set = -1
	c_vol_not_set = -1
	c_inst_null = -1
	c_vol_null = -1

	// XXX
	// instrument note min freq ?
	// in hz ?
	//c_inst_min_freq = 1
	//c_inst_min_play_freq = 1
	c_inst_min_play_freq = 1.0
)
