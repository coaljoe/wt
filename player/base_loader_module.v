module player

//import debug { pp }

// XXX Module data reader-writter
pub struct BaseModuleLoader {
//pub mut:
//	ctx &Context
}

fn new_base_module_loader() &BaseModuleLoader {
	println("new_base_module_loader()")

	bl := &BaseModuleLoader{
		//ctx: ctx
	}

	return bl
}

// XXX utility function
pub fn (bl &BaseModuleLoader) symbol_to_note(s string) Note {
	if s == "===" {
		n := Note{
			note_off: true
		}
		return n
	} else if s == "^^^" {
		n := Note{
			note_cut: true
		}
		return n
	}

	n := get_note(s)
	return n
}

// XXX utility function
pub fn (bl &BaseModuleLoader) note_to_symbol(n Note) string {
	if n.note_off {
		return "==="
	} else if n.note_cut {
		return "^^^"
	}

	return n.name
}
