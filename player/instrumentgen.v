module player

//import math

pub enum InstType {
	square
	//silence
	noise_random
	noise_square
	//mute // ? note_off, block
	formula // XXX
	// XXX for testing
	test_sine
}

[heap]
pub struct InstrumentGen {
pub mut:
	inst &Instrument // link
	//gen &Gen
	typ InstType
	//vol f32
	// Base note (middle C by default) (C-4)
	// XXX this is for transpose of .freq
	// XXX per instrument?
	base_note Note
	// Frequency of the middle C note (C-4)
	// in float hz (.00?)
	// XXX values in samples are depend on mixing_frequency (not driver frequency?)
	// XXX higher freq means fine pitch up, lower fine pitch down (shift)
	// XXX the default value is the value of middle C freq (C-4): 261.62 hz
	freq f32
	// Mix volume
	//vol f32
	vol int

	// Envelopes per gen
	env_vol &Envelope
	env_pitch &Envelope
}

pub fn new_instrument_gen(i &Instrument) &InstrumentGen {
	mut ig := &InstrumentGen{
		inst: i,
		//gen: new_gen(),
		base_note: get_note("C-4"),
		//vol: 1.0,
		vol: c_max_vol,

		// XXX?
		env_vol: new_envelope(i.ctx),
		env_pitch: new_envelope(i.ctx),
	}

	// XXX init
	ig.freq = ig.base_note.freq

	return ig
}

// Frequence difference between fixed middle C freq
// and tuned instrument middle C freq (shift)
pub fn (ig &InstrumentGen) freq_shift() f32 {
	println("InstrumentGen freq_shift")

	// 261.6256 hz
	fixed_middle_c_freq := get_note("C-4").freq
	// Frequency shift from fixed middle C
	freq_shift := ig.freq - fixed_middle_c_freq

	println("fixed_middle_c_freq: ${fixed_middle_c_freq}")
	println("ig.freq: ${ig.freq}")
	println("freq_shift: ${freq_shift}")

	return freq_shift
}

// Get frequency diff for IG's base note
// +/- range
pub fn (ig &InstrumentGen) base_note_freq_diff() f32 {
	println("InstrumentGen base_note_freq_diff")

	fixed_middle_c_freq := get_note("C-4").freq
	base_note_freq := ig.base_note.freq
	//base_note_freq := get_note("B-3").freq
	//base_note_freq := get_note("D-4").freq
	
	ret := base_note_freq - fixed_middle_c_freq

	println("fixed_middle_c_freq: ${fixed_middle_c_freq}")
	println("base_note_freq: ${base_note_freq}")
	println("ret: {ret}")
	
	return ret
}

// XXX volume in 0..1 range
// XXX rename?
pub fn (ig &InstrumentGen) mix_vol() f32 {
	return f32(ig.vol) / f32(c_max_vol)
}

// 0..1 range
pub fn (mut ig InstrumentGen) set_mix_vol(val f32) {
	ig.vol = int(val * c_max_vol)
}

pub fn (ig &InstrumentGen) vol_percent() int {
	return int(ig.mix_vol() * 100.0)
}

pub fn (mut ig InstrumentGen) set_vol_percent(val int) {
	ig.set_mix_vol(f32(val) / 100.0)
}

// XXX move to instrumentgenplayer?
// XXX utility function, returns envelope type as a result record (not a full clone)
fn (ig &InstrumentGen) join_envelopes(e1 &Envelope, e2 &Envelope, pitch_mode bool) &Envelope {
	println("InstrumentGen join_envelopes e1: ${e1}, e2: ${e2}, pitch_mode: ${pitch_mode}")

	// XXX tmp envelope for return
	mut r := new_envelope(ig.inst.ctx)

	mut d1 := e1.data.clone()
	mut d2 := e2.data.clone()

	
	if pitch_mode {
		// Checks
		if e1.data_range % 2 != 0 || e2.data_range % 2 != 0 {
			panic("bad data_range: e1.data_range: ${e1.data_range}; e2.data_range: ${e2.data_range}")
		}
	}


	if pitch_mode {
		// XXX unscale data (apply magnitude)
	
		envs := [e1, e2]
		datas := [d1, d2]
		for i in 0..2 {
			e := envs[i]
			mut d := datas[i]
			
			// Normalize data
			if e.data_range != c_env_default_data_range {
				println("unscale env data from: ${c_env_default_data_range} to: ${e.data_range}")
				//pp(2)

				for mut v in d {
					println("v: ${v}")
					//nv_y := map_range(0, e.data_range, 0, c_env_default_data_range, v.y)
					nv_y := map_range(0, c_env_default_data_range, 0, e.data_range, v.y)
					v.y = nv_y
					
					//println("v: {v}")
					//pp(2)
				}
				//println(d[0])
				//pp(2)
			}
		}
	}
	

	/*
	if pitch_mode {

		envs := [e1, e2]
		datas := [d1, d2]
		for i in 0..2 {
			e := envs[i]
			d := datas[i]
			// Normalize data

			// XXX add function for return expanded / unrolled data in pitch mode?
			// ? with canal centeral value? center_val?
			if e.data_range != c_env_default_data_range {
				println("normalize env data from: $e.data_range to: $c_env_default_data_range")
				//pp(2)
				//d1.map(map_range(0, e.data_range, 0, c_env_default_data_range, it))
				//d.map(map_range(0, e.data_range, 0, c_env_default_data_range, it))
				for mut v in d {
					println("v: $v")
					nv_y := map_range(0, e.data_range, 0, c_env_default_data_range, v.y)
					v.y = nv_y
					
					//println("v: $v")
					//pp(2)
				}
				//println(d[0])
				//pp(2)
			}
		}
	}
	*/

	if pitch_mode {
		max_data_range := max(e1.data_range, e2.data_range)
		//pp(max_data_range)

		// XXX not exact magnitude, but rather used as return value
		// fixme?
		r.data_range = max_data_range
	}

	/*
	max_data_range := max(e1.data_range, e2.data_range)
	pp(max_data_range)

	r.data_range = max_data_range
	*/
	

	//data1 := e1.get_expanded_data()
	//data2 := e2.get_expanded_data()

	res_data := mix_envelope_data(d1, d2)

	r.data = res_data

	println("res_env: ${r}")
	
	return r
}

// Create new instrument gen player
pub fn (ig &InstrumentGen) new_player() &InstrumentGenPlayer {
	println("InstrumentGen new_player")

	igp := new_instrument_gen_player(ig)

	return igp
}
