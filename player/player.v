module player

import common as c

pub struct Player {
pub mut:
	m &Module
	// Current tick position (tick_idx) (global)
	tick int
	// Current row position (row_idx) (global)
	row int
	playing bool
	// XXX row buffer test
	// ? render_buffer / player position in samples / frames (?) XXX not tested
	// Current sample position
	sample_pos int
	// XXX ? row buffer ?
	// ? buffer overshot
	render_buffer_overshot int
	// ? buffer overshot rest (data)
	// rendered rest
	render_buffer_rest []f32
	// XXX cb test
	cb_done bool
	cb_pos int
	cb_total_pos int
	channels []&Channel
	ctx &Context
}

pub fn new_player(mut ctx Context) &Player {
	p := &Player{
		m: ctx.m,
		ctx: ctx,
	}

	// Update ctx
	ctx.player = p
	return p
}

pub fn (mut p Player) set_module(m &Module) {
	p.m = m

	p.set_num_tracks(p.m.num_tracks)
}

// XXX
pub fn (mut p Player) set_num_tracks(n int) {
	println("Player set_num_tracks n: ${n}")

	// Init channels
	for i in 0..n {
		ch := new_channel(i)
		p.channels << ch
	}

	println("len p.channels: ${p.channels.len}")
}

// XXX experimental / test
// Get current playing row of given pattern, otherwise returns -1, if cur pattern is different
pub fn (p &Player) get_current_row_for_pattern(pt &Pattern) int {
	println("Player get_current_row_for_pattern")

	cur_row_idx := p.row
	
	cur_pt, pos := p.get_pattern_by_pos_idx(cur_row_idx)
	_ = pt

	if cur_pt == pt {
		return pos
	} else {
		return -1
	}
}


// Returns pattren and row num / position in pattern by global pos_idx (row idx / row num)
[inline]
pub fn (p &Player) get_pattern_by_pos_idx(pos_idx int) (&Pattern, int) {
	println("Player get_pattern_by_pos_idx: ${pos_idx}")
	//mut row_num := 0

	// Lookup pos_idx
	mut j := 0
	for i, po in p.m.orders {
		println("-> i: ${i}, po: ${po}")
	
		//pt := p.m.patterns[int(po) - 1]
		pt := p.m.patterns[int(po)]
		j_min := j
		j_max := (j + pt.length) - 1

		println("j_min: ${j_min}")
		println("j_max: ${j_max}")

		if j_min <= pos_idx && pos_idx <= j_max {
			//row_num := j_min + (j_max - pos_idx)
			row_num := pos_idx - j_min
			println("row_num: ${row_num}")
			
			return pt, row_num
		}
		
		j = j_max + 1
	}

	panic("pattern not found")
}

pub fn (mut p Player) play_row(pt &Pattern, row_num int) []f32 {
	println("Player play_row pt: [pt], row_num: ${row_num}")

	mut out := []f32{}

	// XXX TODO play order
	//t := p.m.patterns[0].tracks[0]
	//c := t.cells[p.row]

	//n := pt.num_tracks()
	pt_n := pt.num_tracks()
	m_n := p.ctx.m.num_tracks

	// XXX clamp max n value to module's version
	n := if pt_n > m_n {
		m_n
	} else {
		pt_n
	}

	/*
	if pt_n > m_n {
		panic("pt_n > m_n ({pt_n} > {m_n})")
	}
	*/
	
	//mut outs := [][]f32{}
	mut outs := [][]f32{len: n}
	outs = outs.map([]f32{})

	println("n: ${n}")

	//println(outs)
	//panic("3")

	for i in 0..n {
		println("i: ${i}")

		t := pt.tracks[i]

		//println("t0")
		xc := t.cells[row_num] // XXX

		//println("t1")
		//println("c: {c})
		//panic("3")

		// XXX TODO multichannel
		//out << p.play_cell(c, 0)

		//outs << []
		outs[i] << p.play_cell(xc, i)

		//println("outs[i]:")
		//println(outs[i])
		//panic("2")
	}

	//println("t3")

	l := p.m.row_size()

	// Amp coeff
	//x := f32(1)/f32(l)
	// XXX 100% amp mixing (??)
	//x := f32(1.0)
	//x := f32(c_ch_mix_vol)
	// XXX -6db mixing (fixme?)
	//x := f32(0.5)
	// XXX -3db mixing (fixme?)
	// XXX 50% amp mixing (?)
	// https://www.rapidtables.com/electric/decibel.html
	// https://www.silisoftware.com/tools/db.php
	x := f32(0.708)
	
	for i in 0..l {
		//println("i: {i}")
		
		mut v := f32(0.0)
		for o in outs {
			v += o[i] * x
		}
		out << v
	}

	//println(out)
	println(out.len)
	//panic("2")

	return out
}

// Play row with global row idx
pub fn (mut p Player) play_row_idx(row_idx int) []f32 {
	println("Player play_row_idx row_idx: ${row_idx}")

	println("p.row: ${p.row}")

	// XXX TODO play order
	//pt := p.m.patterns[0]
	//row_num := row_idx // XXX

	pt, row_num := p.get_pattern_by_pos_idx(row_idx)
	//row_num := pos_idx

	return p.play_row(pt, row_num)
}


pub fn (mut p Player) play_cell(xc &Cell, ch_num int) []f32 {
	//println("Player play_cell c: {c}, ch_num int")
	//println("Player play_cell ch_num: ${ch_num}")
	vprint("Player play_cell ch_num:", ch_num)

	mut out := []f32{}

	println("len p.channels ${p.channels.len}")
	mut ch := p.channels[ch_num]

	mut skip := false
	note_length := p.m.tick_size() * c_ticks_per_row

	// Checks
	if xc.inst != -1 && xc.inst < 1  {
		panic("bad value for inst: {xc.inst}")
	}

	if xc.inst == -1 && isnil(ch.inst) {
		skip = true
	}

	// Skip note
	if skip {
		return silence(note_length)
	}
	
	if xc.has_note() {

		ch.reset_state()

		// Extract note
		note := xc.note.val

		if note.note_off || note.note_cut{
			//panic("3")
			//ch.reset_state()
			// Skip
			return silence(note_length)
		}
	
		println("note at: ${p.row}")
		println(" name: ${note.name}")
		println(" freq: ${note.freq}")
		println(" num: ${note.num}")

		println("inst: ${xc.inst}")

		mut inst := if xc.inst == -1 {
			ch.inst
		} else {
			p.m.get_instrument_num(xc.inst)
		}


		// XXX vol?
		if xc.vol != -1 {
			inst.vol = xc.vol
		}
		

		//base_note := notes["C-4"]
		//base_note := get_note("C-4")
		base_note := inst.base_note
		cur_note := note

		println("base_note: ${base_note}")
		println("base_note.freq: ${base_note.freq}")

		// Base note shift from middle c ?
		// +-n notes ?
		// freq=261.63 (C-4)
		middle_c_note := get_note("C-4")
		middle_c_freq := get_note("C-4").freq
		//base_note_diff_middle_c := base_note.num - middle_c_note.num
		base_note_diff_middle_c := 0
		//base_note_diff_middle_c := 1
		//base_note_diff_middle_c := 12
		//base_note_diff_middle_c := -12
		// ?
		// Frequency of the resulting note ?
		base_note_diff_middle_c_freq := note_diff_to_freq(base_note.freq, base_note_diff_middle_c)


		println("base_note.num: ${base_note.num}")
		println("middle_c.num: ${middle_c_note.num}")
		println("middle_c.freq: ${middle_c_note.freq}")
		println("middle_c_freq: ${middle_c_freq}")
		println("base_note_diff_middle_c: ${base_note_diff_middle_c}")
		println("base_note_diff_middle_c_freq: ${base_note_diff_middle_c_freq}")

		// XXX 'real note' (as in what will acutually be played)
		real_note_num := note.num + base_note_diff_middle_c
		real_note := get_note_by_num(real_note_num)
		//real_note := get_note("C-4")

		println("")
		println("real_note_num: ${real_note_num}")
		println("real_note: ${real_note}")
		println("real_note.num: ${real_note.num}")
		println("real_note.freq: ${real_note.freq}")

		//freq_diff := cur_note.freq - base_note.freq

		//rate := cur_note.freq / base_note.freq
		rate := real_note.freq / middle_c_freq
		//rate = 1.0
		println("rate: ${rate}")

		// XXX FIXME: cleanup ?
		panic("2")
			
		//note_data := p.m.instruments[0].render_wave(rate, note_length)



		//if !ch.playing() {
		if true {
			ch.inst = inst
			ch.inst_player = inst.new_player()
			// XXX remember rate
			ch.rate = rate
			//panic("3")
		} else {
			//panic("2")
		}

		note_data := ch.inst_player.render_buf_cont(rate, note_length)
		//note_data := p.m.instruments[0].render_wave(rate, note_length)
			
		out << note_data
	} else {
		mut data := []f32{}
		if ch.playing() {
			data = ch.inst_player.render_buf_cont(ch.rate, note_length)
		} else {
			data = silence(note_length)
		}
		
		out << data
	}

	//c.dump_raw(path, out)
	//c.dump_raw(p.output_path, out)
	return out
}

//pub fn (p mut

pub fn (mut p Player) render_to_file(path string) {
	println("Player render_to_file path: ${path}")

	mut out := []f32{}

	// XXX this function fires .play?
	p.play()

	//bpm := 120
	//rows_per_beat := 4
	//tick_time_sec := 60.0 / f32(bpm) / f32(rows_per_beat) / c_ticks_per_row

	println("row_time_sec: ${p.m.row_time_sec()}")
	
	tick_time_sec := p.m.tick_time_sec()
	println("tick_time_sec: ${tick_time_sec}")
	tick_size := int(f32(c_sr) * tick_time_sec)
	println("tick_size: ${tick_size}")
	

	//total_ticks := c_num_rows * c_ticks_per_row
	//total_rows := c_num_rows
	total_rows := p.m.length_rows()
	_ = total_rows

	//note_data := p.m.instruments[0].render_wave(1.0, -1)

	/*
	mut row := 0
	for _ in 0..total_rows {
	//for tick in 0..total_ticks {
		println("row: {row}")

		out << p.render_row()
	}
	//panic("2")
	*/

	for p.playing {
		println("step:")
		
		out << p.render_row()
	}

	c.dump_raw(path, out)
}

// cont
fn (mut p Player) render_tick_cont() []f32 {
	println("Player render_tick_cont")
	
	panic("not implemented")
}

// XXX render row
// XXX update tick
// XXX update row?
// XXX ? rename: render_next_row, render_row_cont
// cont
fn (mut p Player) render_row() []f32 {
	println("Player render_row")

	//println("len patterns: {p.m.patterns.len}")
	//panic(p.m.patterns.len)

	mut out := []f32{}

	//total_rows := c_num_rows
	total_rows := p.m.length_rows()

	mut tick := p.tick

	println("tick: ${tick}")
	mut fire_note := false
			
	if tick % c_ticks_per_row == 0  {
		// XXX fixme: always firing row?
		// XXX upd: should be removed ?
		if tick != 0 {
			p.row++
		}
		fire_note = true
	
		println("--> row: ${p.row}")
	}
		
	if fire_note {
		out << p.play_row_idx(p.row)
	}
	
	// skip ticks
	// XXX fixme?
	//tick += c_ticks_per_row - 1
	tick += c_ticks_per_row

	// Finally
	// XXX fixme?
	if p.row == total_rows - 1 {
		p.playing = false
	}

	// Update p.tick
	p.tick = tick
	
	return out
}


// Render fixed-size buffer
// XXX TODO: add render_buf_silince function for buffers padded with silence?
// cont
pub fn (mut p Player) render_buf(buf_size int) ([]f32, bool) {
	println("Player render_buf buf_size: ${buf_size}")

	mut out := []f32{}

	//tick_size := p.m.tick_size()
	row_size := p.m.row_size()

	// XXX dont need this currently ?
	// XXX have row buffer
	// XXX not tested
	if false {
		// XXX fixme in future?
		if buf_size < row_size {
			println("min_size: ${row_size}")
			panic("buf_size is too small: ${buf_size}")
		}
	}

	// XXX extra checks
	if p.m.orders.len == 0 {
		panic("no orders")
	}
	
	num_rows := buf_size / row_size
	num_rows_rest := buf_size - (row_size * num_rows)

	println("row_size: ${row_size}")
	println("num_rows: ${num_rows}")
	println("num_rows_rest: ${num_rows_rest}")
	//panic("2")

	// Append data from the rest
	out << p.render_buffer_rest
	p.render_buffer_rest = []
	p.render_buffer_overshot = 0

	println("ZZZ len out: ${out.len}")
	//panic("2")


	mut render_size := 0
	for _ in 0..num_rows+1 { // XXX n ticks + 1
		//tick_idx := p.tick + i
		render_row_out := p.render_row()
		println("YYY len render row out: ${render_row_out.len}")

		out << render_row_out

		println("YYY len out: ${out.len}")
		
		//panic("2")

		//render_size += tick_size
		render_size += row_size

		// XXX?
		if !p.playing {
			println("XXX play break")
			break
		}
	}
	println("XXX len out: ${out.len}")
	//panic("2")

	println("render_size: ${render_size}")

	overshot := render_size - buf_size

	// XXX
	p.sample_pos += render_size

	println("sample_pos: ${p.sample_pos}")
	println("tick: ${p.tick}")

	p.render_buffer_overshot = overshot

	println("render_buffer_overshot: ${p.render_buffer_overshot}")

	if overshot > 0 {
		println("len out: ${out.len}")
		// XXX ? clicks?
		//p.render_buffer_ret_rest = out[buf_size .. render_size]
		p.render_buffer_rest = out[buf_size..]
	}

	// XXX Trim data
	out = out[..buf_size]

	// Playing state
	ret := p.playing

	return out, ret
}

pub fn (mut p Player) play() {
	p.playing = true
}

pub fn (mut p Player) stop() {
	p.playing = false
}
