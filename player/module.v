module player

//import strconv

[heap]
pub struct Module {
mut:
	bpm int
pub mut:
	// XXX info?
	info &ModuleInfo
	instruments []&Instrument
	patterns []&Pattern
	orders []PlayOrder
	num_tracks int
	ctx &Context
}

pub fn new_module(mut ctx Context, num_tracks int) &Module {
	mut m := &Module{
		bpm: 120,
		info: new_module_info(),
		num_tracks: num_tracks,
		ctx: ctx,
	}

	// Update ctx
	ctx.m = m
	
	return m
}

pub fn (m &Module) has_instrument(i &Instrument) bool {
	return i in m.instruments
}

pub fn (mut m Module) add_new_instrument() &Instrument {
	i := new_instrument(m.ctx)
	m.instruments << i

	return i
}

pub fn (mut m Module) remove_instrument_num(num int) {
	println("Module remove_instrument_num num: ${num}")
	
	println("len instruments: ${m.instruments.len}")
	m.instruments.delete(num - 1)
	println("len instruments: ${m.instruments.len}")
}

pub fn (mut m Module) set_instrument(num int, inst &Instrument) {
	if m.has_instrument(inst) {
		panic("already have this instrument")
	}
	
	m.instruments.insert(num - 1, inst)
}

pub fn (mut m Module) replace_instrument(num int, new_inst &Instrument) {
	m.remove_instrument_num(num)
	//m.instruments[inst_num - 1] = new_inst
	//m.instruments.insert(inst_num - 1, new_inst)
	m.set_instrument(num, new_inst)
}

pub fn (m &Module) has_pattern(pt &Pattern) bool {
	return pt in m.patterns
}

pub fn (mut m Module) add_new_pattern() &Pattern {
	mut pt := new_pattern(m)
	pt.init() // XXX fixme?
	//m.patterns << p

	m.add_pattern(pt)
	
	return pt
}

pub fn (mut m Module) add_pattern(pt &Pattern) {
	if m.has_pattern(pt) {
		panic("already have this pattern")
	}
	m.patterns << pt
}

pub fn (mut m Module) remove_pattern(pt &Pattern) {
	println("Module remove_pattern")

	if !m.has_pattern(pt) {
		panic("don't have this pattern")
	}
	for i, x in m.patterns {
		println("-> ${i}")
		println("x: ${x}")
		//println(strconv.v_sprintf("xp: %p", voidptr(x)))
		zp := unsafe { __addr(x) }
		//println("xp: {zp}")
		//println(int(zp))
		//zzp := *zp
		//println(zp)
		// ???
		println(*zp)
		//println(zzp)
		
		if x == pt {
			m.remove_pattern_num(i)
			return
		}
	}
	panic("pattern not found")
}

pub fn (mut m Module) remove_pattern_num(num int) {
	println("Module remove_pattern_num num: ${num}")
	
	m.patterns.delete(num)

	println("len patterns: ${m.patterns.len}")

	for z, x in m.patterns {
		println("-> ${z}")
		println("x: ${x}")
		//println(strconv.v_sprintf("xp: %p", voidptr(x)))
		zp := unsafe { __addr(x) }
		//zp := __addr(x)
		//println("xp: {zp:d}")
		//println(zp)
		println(*zp)
	}
	//panic("5")
}

pub fn (mut m Module) clear_patterns() {
	println("Module clear_patterns")
	
	for z, _ in m.patterns {
		println("-> ${z}")

		m.remove_pattern_num(z)		
	}

	if m.patterns.len != 0 {
		panic("error")
	}
	
	//panic("5")
}


// Get instrument by num
pub fn (m &Module) get_instrument_num(num int) &Instrument {
	if num < 1 {
		panic("bad instrument num: {num}")
	}

	// XXX check
	if m.instruments.len < num {
		panic("no such instrument num: {num}, len instruments: {m.instruments.len}")
	}
	
	return m.instruments[num - 1]
}

pub fn (m &Module) bpm() int {
	return m.bpm
}

pub fn (mut m Module) set_bpm(v int) {
	m.bpm = v
}

// Tick time in seconds
pub fn (m &Module) tick_time_sec() f32 {
	tick_time_sec := 60.0 / f32(m.bpm) / f32(c_rows_per_beat) / c_ticks_per_row
	//println("tick_time_sec: {tick_time_sec}")

	return tick_time_sec
}

// Row time in seconds
pub fn (m &Module) row_time_sec() f32 {
	row_time_sec := 60.0 / f32(m.bpm) / f32(c_rows_per_beat)

	return row_time_sec
}

// Beat time in seconds (4 rows?)
pub fn (m &Module) beat_time_sec() f32 {
	beat_time_sec := 60.0 / f32(m.bpm)

	return beat_time_sec
}

// Tick size in samples
pub fn (m &Module) tick_size() int {
	tick_size := int(f32(c_sr) * m.tick_time_sec())
	//println("tick_size: {tick_size}")

	return tick_size
}

// Row size in samples
pub fn (m &Module) row_size() int {
	return m.tick_size() * c_ticks_per_row
}

// Module length in rows
pub fn (m &Module) length_rows() int {
	mut length := 0
	for po in m.orders {
		pt := m.patterns[int(po)]
		length += pt.length
	}
	
	return length
}

// Module length in seconds
pub fn (m &Module) length_sec() f32 {
	return m.length_rows() * m.row_time_sec()
}
