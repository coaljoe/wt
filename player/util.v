module player

// XXX fixme: v bug #5181
pub fn string_to_inst_type_enum(s string) InstType {

	mut ok := true
	
	val := match s {
	
		"square" { InstType.square }
		"noise_random" { InstType.noise_random }
		"noise_square" { InstType.noise_square }
		"formula" { InstType.formula }
		
		else {
			//panic("bad enum")
			ok = false
			InstType.square
		}
	}

	if !ok {
		println("bad enum: s: ${s}")
		// XXX
		panic("bad enum")
	}
	
	return val
}

// Length in seconds to length in samples
pub fn seconds_to_samples(secs f32) int {
	return int(c_sr * secs)
}

// Length in samples to length in seconds
pub fn samples_to_seconds(nsamples int) f32 {
	return f32(nsamples) / f32(c_sr)
}
