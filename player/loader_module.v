module player

import os
import x.json2
import common as c
import debug { pp }

// XXX Module data reader
pub struct ModuleLoader {
	BaseModuleLoader
pub mut:
	ctx &Context
}

pub fn new_module_loader(ctx &Context) &ModuleLoader {
	println("new_module_loader()")

	l := &ModuleLoader{
		BaseModuleLoader: new_base_module_loader(),
		ctx: ctx
	}

	return l
}

pub fn (mut l ModuleLoader) load(path string) &Module {
	println("ModuleLoader load: path: ${path}")

	mut xpath := path

	if !os.exists(xpath) {
		panic("error: no such path")
	}

	if os.is_dir(xpath) {
		println("path is dir... trying to load module.json")
		xpath += "/module.json"
	}

	println("1")
	str_data := c.load_json_file_string(xpath)

	println("2")
	raw_data := json2.raw_decode(str_data) or {
		panic(err)
	}
	println("3")

	data := raw_data.as_map()
	_ = data
	println("4")

	/*
	mut m := &Module{
		ctx: l.ctx,
	}
	*/

	if !("module" in data) {
		panic("no module definition found")
	}

	// Load module data
	d := unsafe { data["module"].as_map() }
	_ = d

	// XXX create module
	num_tracks := unsafe { d["num_tracks"].int() }
	mut m := new_module(mut l.ctx, num_tracks)

	// XXX patch module data
	title := unsafe { d["title"].str() }
	println("title: ${title}")
	m.info.title = title
	bpm := unsafe { d["bpm"].int() }
	println("bpm: ${bpm}")
	m.bpm = bpm // XXX should use set_bpm?

	m.num_tracks = unsafe { d["num_tracks"].int() }
	println("m num_tracks: ${m.num_tracks}")

	// XXX load instruments
	println("load instruments...")
	
	if !("instruments" in data) {
		panic("no instruments definition found")
	}

	inst_arr := unsafe { data["instruments"].arr() }
	println("len inst_arr: ${inst_arr.len}")
	//panic("3")

	for i, inst_d in inst_arr {
		println("load instrument i: ${i} ...")
		
		inst_data := inst_d.as_map()
		inst := l.decode_instrument_json(inst_data)

		// XXX use different method?
		m.instruments << inst
	}

	println("done loading instruments")
	//pp(2)

	// XXX load patterns
	{
		println("load patterns...")
	
		if !("patterns" in data) {
			panic("no patterns definition found")
		}

		pt_arr := unsafe { data["patterns"].arr() }
		println("len pt_arr: ${pt_arr.len}")
		//panic("3")

		for i, pt_d in pt_arr {
			println("load pattern i: ${i} ...")
		
			pt_data := pt_d.as_map()
			pt := l.decode_pattern_json(pt_data)

			// XXX use different method?
			m.patterns << pt
		}

		println("done loading patterns")

		//panic("2")
	}

	// XXX load orders
	{
		println("load orders...")

		ord_arr := unsafe { data["orders"].arr() }
		println("len ord_arr: ${ord_arr.len}")

		for i, ord_d in ord_arr {
			println("-> i: ${i}")
			
			ord_data := ord_d.int()
		
			m.orders << ord_data
		}


		println("done loading orders")
	}

	return m
}

// XXX help function to decode saved envelope data
pub fn (l &ModuleLoader) decode_envelope_json(f json2.Any) &Envelope {
	println("ModuleLoader decode_envelope_json f: ${f}")

	// XXX TODO: test keys?

	data := f.as_map()

	// XXX Checks
	if "data" in data {
		d := unsafe { data["data"].arr() }

		if d.len < 1 {
			//panic("envelope data cannot be empty")
		}

		if d.len < 2 {
			//panic("envelope data is too short")
		}
	}

	mut env_data := []EnvPoint{}

	if "data" in data {
		d := unsafe { data["data"].arr() }
		for p_obj in d {
			p := p_obj.arr()
			println(p)
			x := p[0].int()
			y := p[1].int()
			v := EnvPoint{x, y}
			println(v)
			//pp(2)

			env_data << v
		}
	}

	//pp(2)

	// XXX
	tmp := new_envelope(l.ctx)
	_ = tmp

	/*
	e := &Envelope{
		//tick_size: c_env_tick_size,
		tick_size: tmp.tick_size,
		//enabled: data["enabled"].bool(),
		//enabled: c.js_mustbool(data["enabled"]),
		//enabled: c.js_mustbool_k(data, "enabled"),
		enabled: c.js_bool_k_or(data, "enabled", tmp.enabled),
		data: env_data,
		//data_range: data["data_range"].int(),
		//data_range: c.js_mustint(data["data_range"]),
		//data_range: c.js_mustint_k(data, "data_range"),
		data_range: c.js_int_k_or(data, "data_range", tmp.data_range),
	}
	*/

	tmp_ := *tmp

	e := &Envelope{
		...tmp_,
		enabled: c.js_bool_k_or(data, "enabled", tmp.enabled),
		data: env_data,
		data_range: c.js_int_k_or(data, "data_range", tmp.data_range),

		// XXX?
		ctx: tmp_.ctx,
	}

	println(e)
	//pp(2)
	
	return e
}

// XXX load pattern from json data
pub fn (l &ModuleLoader) decode_pattern_json(f json2.Any) &Pattern {
	println("ModuleLoader decode_pattern_json")
	//println("ModuleLoader decode_pattern_json f: {f}")

	data := f.as_map()

	//nrows := data["nrows"].int()

	//mut pt := &Pattern{}

	tr_d := unsafe { data["tracks"].arr() }
	
	
	//pp(2)

	// XXX
	tmp := new_pattern(l.ctx.m)
	_ = tmp

	tmp_ := *tmp
			
	mut pt := &Pattern{
		...tmp_,
		//length: c.js_int_k_or(data, "ntracks", tmp.length),
		length: c.js_int_k_or(data, "num_rows", tmp.length),

		// XXX?
		m: tmp_.m,
		//ctx: tmp_.ctx,
	}

	for i, tr_obj in tr_d {
		pt.add_track()

		cells_d := tr_obj.arr()
		_ = cells_d
		
		//cells := ["cells"]
		println("cells_d")
		//println(cells_d)

		println("cell_d:")
		for cell_obj in cells_d {
			//cell_d := cell_obj.arr()
			cell_d := cell_obj
			for j, cell in cell_d.arr() {
				println("i: ${i}, j: ${j}")
				println("cell: ${cell}")

				// Set data
				t := pt.tracks[i]
				mut xc := t.cells[j]

				cell_arr := cell.arr()
				if !(cell_arr[0] is json2.Null) {
					note_name := cell_arr[0].str()
					println(note_name)

					// XXX handle nulls
					mut inst_num := -1
					if !(cell_arr[1] is json2.Null) {
						inst_num = cell_arr[1].int()
					}
					println(inst_num)

					if inst_num == 0 {
						panic("bad inst_num: {inst_num}")
					}

					mut vol := -1
					if cell_arr[2] !is json2.Null {
						vol = cell_arr[2].int()
					}
					println(vol)

					// XXX
					//note := get_note_opt(note_name)

					note_ := l.symbol_to_note(note_name)
					note := get_note_opt_from_note(note_)
					
					println("note: ${note}")

					xc.note = note
					xc.inst = inst_num
					//c.vol
					
					//panic("3")
				}
			}
		}

		//pp(2)
		//panic("2")
	}

	//panic("2")


	// XXX ?

	//pt.init()

	//println(pt)
	//pp(2)
	
	return pt
}

// XXX load instrument from json data
pub fn (l &ModuleLoader) decode_instrument_json(f json2.Any) &Instrument {
	println("ModuleLoader decode_instrument_json")

	data := f.as_map()

	mut inst := &Instrument{
		ctx: l.ctx,

		// XXX blank
		//env_vol: &Envelope(0),
		//env_pitch: &Envelope(0),
		// default: XXX fixme?
		env_vol: new_envelope(l.ctx),
		env_pitch: new_envelope(l.ctx),

		// XXX FIXME?
		// default:
		base_note: get_note("C-4"),
		//base_note: get_note("C-3"),

		name: unsafe { data["name"].str() },
		vol: unsafe { data["volume"].int() },
		loop: unsafe { data["loop"].bool() },
	}

	// XXX read base note?
	{
		
		if "base_note" in data {
			base_note_t := unsafe { data["base_note"].str() }
			base_note := get_note(base_note_t)
			println("base_note_t: ${base_note_t}")
			//pp(2)
			inst.base_note = base_note
		}
	}

	//pp(3)

	println("keys: ${data.keys()}")

	if "envelope_volume" in data {
		inst.env_vol = l.decode_envelope_json(unsafe { data["envelope_volume"] })
		//pp(2)

		// XXX post fix
		mut e := inst.env_vol
		//if e.enabled {
		if e.empty() {
			//pp(2)

			// XXX insert default values for volume
			// ? volume envelope can't be empty(?)
			e.data << EnvPoint{0, 64}
			e.data << EnvPoint{10, 64}

			//println(e.data)
			//pp(2)
		}	
		//}
	}

	if "envelope_pitch" in data {
		inst.env_pitch = l.decode_envelope_json(unsafe { data["envelope_pitch"] })
		//pp(2)

		// XXX post fix
		mut e := inst.env_pitch

		if e.empty() {
			//pp(2)
		
			// XXX insert default values for panning
			// ? panning envelope can't be empty(?)
			e.data << EnvPoint{0, 32} // XXX center
			e.data << EnvPoint{10, 32}
		
			//println(e.data)
			//pp(2)
		}
	}

	//pp(4)

	// XXX load gens

	num_gens := unsafe { data["num_gens"].int() }
	gens_data := unsafe { data["gens"].arr() }

	for i in 0..num_gens {
		println("-> ${i} gen")

		gen_data := gens_data[i].as_map()

		// json to gen
		mut gen := inst.add_new_gen()

		type_t := unsafe { gen_data["type"].str() }
		println("type_t: ${type_t}")
		gen.typ = string_to_inst_type_enum(type_t)

		gen.vol = unsafe { gen_data["volume"].int() }
		//gen.panning = gen_data["panning"].int()
		//gen.tune = gen_data["tune"].int()
		gen.freq = unsafe { gen_data["freq"].f32() }

		if false {
		base_note_t := unsafe { gen_data["base_note"].int() }
		gen.base_note = get_note_by_num(base_note_t)
		dump(base_note_t)
		}

		
		dump(gen.base_note)
		println("gen.base_note: ${gen.base_note}")
		//panic(2)

		//dump(gen)
	}

	return inst
}

pub fn (mut l ModuleLoader) load_instrument(path string) &Instrument {
	println("ModuleLoader load_instrument: path: ${path}")

	str_data := c.load_json_file_string(path)

	raw_data := json2.raw_decode(str_data) or {
		panic(err)
	}

	data := raw_data.as_map()

	inst := l.decode_instrument_json(data)

	return inst
}

pub fn (mut l ModuleLoader) load_pattern(path string) &Pattern {
	println("ModuleLoader load_pattern: path: ${path}")

	str_data := c.load_json_file_string(path)

	raw_data := json2.raw_decode(str_data) or {
		panic(err)
	}

	data := raw_data.as_map()

	pt := l.decode_pattern_json(data)

	return pt
}
