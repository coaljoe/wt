module player

import strconv
import common as c

fn test_gen() {

	mut g1 := new_gen()
	

	/*
	len_samples := int(c_sr * 1.0)

	data := g1.wave_cont(.sine, 1000, 1.0, len_samples)

	c.dump_raw("/tmp/g1.raw", data)
	*/

	//g1.render_to_file_sec("/tmp/g1.raw", .sine, 1000, 1.0, 1.0)


	///*
	//g1.phase_t = 0.5
	//g1.render_to_file("/tmp/g1_x.raw", .sine, 1000, 1.0, 45)
	//println(g1.sine_t)

	println("g1 sine_t: {g1.sine_t}")

	////
	println("s0:")
	//g1.render_to_file("/tmp/g1a.raw", .sine, 1000, 1.0, 22)
	//g1.render_to_file("/tmp/g1a.raw", .sine, 1000, 1.0, 896)
	g1.render_to_file("/tmp/g1a.raw", .square, 1000, 1.0, 896)
	println("g1 sine_t: {g1.sine_t}")

	println("s1:")
	//g1.sine_t = 0.5
	g1.render_to_file("/tmp/g1b.raw", .sine, 1000, 1.0, 23)
	println("g1 sine_t: {g1.sine_t}")
	//*/
}

/*
fn test_gen_sweep() {

	mut g2 := new_gen()

	len_samples := int(c_sr * 1.0)

	data := g2.wave_sweep_cont(.sine, 1000, 2000, 1.0, len_samples)

	println("data: ${data[0..40]}")

	//pp(2)

	c.dump_raw("/tmp/g2.raw", data)

	// XXX square
	data2 := g2.wave_sweep_cont(.square, 100, 200, 1.0, len_samples)

	println("data2: ${data2[0..40]}")

	//pp(3)

	c.dump_raw("/tmp/g3.raw", data2)
}
*/

//*
fn test_gen_cont() {
	mut g1 := new_gen()

	println("s1")
	println("t: {g1.sine_t}")
	//g1.sine_t = 0.0226757
	g1.render_to_file("/tmp/g1z1.raw", .sine, 1000, 1.0, 2)
	println("t: {g1.sine_t}")

	cont_sine_t := g1.sine_t

	println("cont_sine_t: {cont_sine_t}")
	//panic("2")

	diff := cont_sine_t - 0.0453515
	println("diff: {diff}")
	strconv.v_printf("diff: %.15f\n", diff)
	//{ panic("2") }

	println("s2")
	g1.reset()
	g1.sine_t = 0.0453515
	//g1.sine_t = 0.0226757
	//g1.sine_t = cont_sine_t
	println("t: {g1.sine_t}")
	g1.render_to_file("/tmp/g1z2.raw", .sine, 1000, 1.0, 2)
	println("t: {g1.sine_t}")


	println("sx")
	g1.reset()
	println("t: {g1.sine_t}")
	//g1.reset()
	g1.render_to_file("/tmp/zz2.raw", .sine, 1000, 1.0, 4)
	println("t: {g1.sine_t}")

	println("sy")
	g1.reset()
	println("t: {g1.sine_t")
	//g1.reset()
	g1.render_to_file("/tmp/zz3.raw", .sine, 1000, 1.0, 441)
	println("t: {g1.sine_t}")
	g1.render_to_file("/tmp/zz3a.raw", .sine, 1000, 1.0, 441)
	println("t: {g1.sine_t}")
	g1.render_to_file("/tmp/zz3b.raw", .sine, 1000, 1.0, 123)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/zz3c.raw", .sine, 1000, 1.0, 123)
	println("t: {g1.sine_t:.8f}")

	//panic("2")
}
//*/

//*/
fn test_gen_cont2() {
	mut g1 := new_gen()

	println("s1")
	println("t: {g1.sine_t}")
	//g1.sine_t = 0.0226757
	g1.render_to_file("/tmp/2_g1z1.raw", .square, 1000, 1.0, 2)
	println("t: {g1.sine_t}")

	cont_sine_t := g1.sine_t

	println("cont_sine_t: {cont_sine_t}}")
	//panic("2")

	diff := cont_sine_t - 0.0453515
	println("diff: {diff}")
	strconv.v_printf("diff: %.15f\n", diff)
	//{ panic("2") }

	println("s2")
	g1.reset()
	g1.sine_t = 0.0453515
	//g1.sine_t = 0.0226757
	//g1.sine_t = cont_sine_t
	println("t: {g1.sine_t}")
	g1.render_to_file("/tmp/2_g1z2.raw", .square, 1000, 1.0, 2)
	println("t: {g1.sine_t}")


	println("sx")
	g1.reset()
	println("t: {g1.sine_t}")
	//g1.reset()
	g1.render_to_file("/tmp/2_zz2.raw", .square, 1000, 1.0, 4)
	println("t: {g1.sine_t}")

	println("sy")
	g1.reset()
	println("t: {g1.sine_t}")
	//g1.reset()
	g1.render_to_file("/tmp/2_zz3.raw", .square, 1000, 1.0, 441)
	println("t: {g1.sine_t}")
	g1.render_to_file("/tmp/2_zz3a.raw", .square, 1000, 1.0, 441)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/2_zz3b.raw", .square, 1000, 1.0, 123)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/2_zz3c.raw", .square, 1000, 1.0, 123)
	println("t: {g1.sine_t:.8f}")

	//panic("2")
}
//*/

//*/
fn test_gen_cont3() {
	mut g1 := new_gen()

	//f0 := f32(-145.2740)
	f0 := f32(145.2740)

	println("sy")
	g1.reset()
	println("t: {g1.sine_t")
	//g1.reset()
	g1.render_to_file("/tmp/3_zz3.raw", .square, f0, 1.0, 441)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3_zz3a.raw", .square, f0, 1.0, 441)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3_zz3b.raw", .square, f0, 1.0, 123)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3_zz3c.raw", .square, f0, 1.0, 123)
	println("t: {g1.sine_t:.8f}")

	g1.reset()
	println("t: {g1.sine_t}")
	//g1.reset()
	g1.render_to_file("/tmp/3a_zz3.raw", .sine, f0, 1.0, 441)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3a_zz3a.raw", .sine, f0, 1.0, 441)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3a_zz3b.raw", .sine, f0, 1.0, 123)
	println("t: {g1.sine_t:.8f}")
	g1.render_to_file("/tmp/3a_zz3c.raw", .sine, f0, 1.0, 123)
	println("t: {g1.sine_t:.8f}")

	panic("2")
}
//*/
