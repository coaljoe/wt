module player

[heap]
pub struct Context {
//mut:
// XXX fixme ?
pub mut:
//__global:
	m &Module
	player &Player
	//t int
}

//__global ctx &Context
//const (
//	ctx = &Context{}
//)

//var (
//	ctx := &Context{}
//)
//const (
//	mut ctx = &Context{}
//)

//pub fn init_context() {
//	ctx = &Context{}
//}

pub fn new_context() &Context {
	mut x := &Context{
		m: &Module(unsafe { nil }),
		player: &Player(unsafe { nil }),
	}

	return x
}
