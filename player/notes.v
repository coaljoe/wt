module player

import math
//import strconv
import lib.zopt

pub const (
	//notes = map[string]&Note
	// Note array: note_num -> note
	g_notes = make_notes()
	// Note name lookup table: note_name -> note
	g_note_name_lookup = make_note_name_lookup()
)

pub struct Note {
pub mut:
	name string
	freq f32
	// Note number in [0,95] range
	num int

	// XXX note sum type emulation
	// FIXME
	note_off bool
	note_cut bool
}

/*
fn (n &Note) str() string {
	//println("derp: $n.freq")
	zfreq := f64(n.freq) // XXX bug
	_ = zfreq
	//return strconv.v_sprintf("Note<n: %s, f: %f, num: %d>", n.name, zfreq, n.num)
	return strconv.v_sprintf("Note<%s>", n.name)
}
*/

fn (n Note) str() string {
	//return strconv.v_sprintf("Note<n: %s, f: %f, num: %d>", n.name, n.freq, n.num)
	if n.note_off {
		return "Note<NoteOff>"
	} else if n.note_cut {
		return "Note<NoteCut>"
	}
	//return strconv.v_sprintf("Note<%s>", n.name)
	return "Note<${n.name}>"
}

// Convert note number to piano key number
// https://en.wikipedia.org/wiki/Piano_key_frequencies
fn (n Note) num_piano() int {
	panic("not implemented")

	/*
	// Lower range
	if n.num < 0 {
		// XXX fixme?
		// XXX use negative for sub A0 range?
		// https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
		panic("not implemented: only for positive note numbers: num: $n.num")
	}

	return n.num + 1
	*/
}

// Convert note number to midi note number
fn (n Note) num_midi() int {
	return n.num + 12
}


pub fn make_notes() map[int]Note {
	println("make_notes")
	
	mut m := map[int]Note

	note_names :=
		["C-_", "C#_", "D-_", "D#_", "E-_", "F-_", "F#_",
		 "G-_", "G#_", "A-_", "A#_", "B-_"]
	mut octave_num := 0
	mut note_num := 0
	for n in 0..95+1 {
		//println("n: {n}, note_num: {note_num}, octave_num: {octave_num}")
		freq := note_to_freq(n)
		name_temp := note_names[note_num]
		name := name_temp.replace("_", octave_num.str())
		if note_num >= 11 {
			octave_num++
		}
		note_num = (note_num + 1) % 12
		//#noteNum += 1

		//println("freq: {freq}")

		note := Note{
			name: name,
			freq: freq,
			num: n,
		}
		//println("note:")
		//println(" name: {note.name}")
		//println(" freq: {note.freq}")
		//println(" num: {note.num}")
		
		//m[name] = note
		m[n] = note

		//pp(2)
	}

	//println("m:")
	//println(m)
	//panic("2")
	
	return m
}

pub fn make_note_name_lookup() map[string]Note {
	println("make_note_name_lookup")
	
	mut ret := map[string]Note

	for _, n in g_notes {
		ret[n.name] = n
	}
	
	return ret
}


pub fn init_notes() {
	/*
	n := &Note{
		name: "C-4",
	}
	notes["test"] = n
	*/
}

/*
// XXX workaround for &Note fields
pub fn get_note_ref(name string) &Note {
	note := get_note(name)
	_ = note

	return &note
}
*/

pub fn get_note_opt(name string) zopt.ZOption<Note> {
	note := get_note(name)

	opt := get_note_opt_from_note(note)

	return opt
}

pub fn get_note_opt_from_note(note Note) zopt.ZOption<Note> {
	// XXX
	mut opt := zopt.ZOption<Note>{val: note}
	opt.val_set = true

	return opt
}

// Get note by name
pub fn get_note(name string) Note {

	/*
	keys := notes.keys()
	println("keys: $keys")

	for k in keys {
		n := notes[k]

		println("n.num: $n.num")
		println("n.name: $n.name")
	}
	*/

	//panic("3")

	/*
	// TODO: optimize?
	for _, n in notes {
		//println("n: $n")
		println("n.num: $n.num")
		println("n.name: $n.name")
		//if n.name == name {
		//	return n
		//}
	}
	*/

	/*

	// TODO: optimize?
	for k, _ in notes {
		n := notes[k]
		//println("n: $n")
		//println("n.num: $n.num")
		//println("n.name: $n.name")
		if n.name == name {
			return n
		}
	}

	*/

	return g_note_name_lookup[name] or {
		panic("note not found: name: {name}")
	}
}

// Get note by note num
pub fn get_note_by_num(num int) Note {

	if !(num in g_notes) {
		panic("note not found: num: {num}")
	}

	note := g_notes[num]

	//println("num: $num -> note: {note}")
	
	return note
}

pub fn note_to_freq(n int) f32 {
	//f = ((2**(1/12)) * (n-49)) * 440.0 # slow
	//f = ((2**(1/12)) * (n-49)) * 440.0 # slow
	a440n := 57 // A440 key number (C-0 = 0)
	a440_delta := n - a440n
	dn := f32(a440_delta)
	f := 440.0 * math.powf(2.0, (dn/12.0))
	//println("noteToFreq: n: {n}, f: {f}, a440_delta: {a440_delta}")
	
	return f
}

pub fn freq_to_note(f f32) int {
	panic("not implemented")
}

// Difference between notes to freq of the resulting note
// base_note_freq: frequency of the base note
// n: delta in notes from base note
// 'n' can be any value (-x..0..+x)
// Returns the frequency of the resulting note
// http://techlib.com/reference/musical_note_frequencies.htm
pub fn note_diff_to_freq(base_note_freq f32, n int) f32 {
	// q = 2 ** (N/12.)
	// N steps from base note (freq)
	q := math.powf(2.0, f32(n)/12.0)
	r := base_note_freq * q

	return r
}
