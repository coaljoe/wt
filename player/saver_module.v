module player

import os
import x.json2
//import common as c
import debug { pp }

// XXX Module data writter
pub struct ModuleSaver {
	BaseModuleLoader
pub mut:
	ctx &Context
}

pub fn new_module_saver(ctx &Context) &ModuleSaver {
	println("new_module_saver()")

	s := &ModuleSaver{
		BaseModuleLoader: new_base_module_loader(),
		ctx: ctx,
	}

	return s
}

pub fn (mut s ModuleSaver) save(m &Module, path string) {
	println("ModuleSaver save: path: ${path}")

	mut xpath := path

	if !os.exists(xpath) {
		os.mkdir(xpath) or {
			panic(err)
		}
	}
	
	xpath += "/module.json"

	println("xpath: ${xpath}")

	mut f := os.create(xpath) or {
		panic(err)
	}

	// XXX root ?
	mut obj := map[string]json2.Any{}

	mut module_block := map[string]json2.Any{}
	{
		mut x := map[string]json2.Any{}
		//x = &hdr.move()
		x["title"] = m.info.title.clone()
		x["num_tracks"] = m.num_tracks
		x["bpm"] = m.bpm

		println(x.str())

		module_block = x.clone()

		println(module_block.str())

		// XXX
		obj["module"] = module_block
		
		//panic(2)
	}

	// XXX
	//s.encode_instrument_json(m.instruments[0], 1)

	// XXX
	//mut inst_arr := []json2.Any{}
	mut instruments_block := []json2.Any{}
	{
		for i, inst in m.instruments {
			// XXX is id/num starts with 0 or 1 ?
			inst_obj := s.encode_instrument_json(inst, i+1)
			//inst_arr << inst_obj
			instruments_block << inst_obj
		}

		println(instruments_block.str())
		//println(json2.Any(instruments_block).prettify_json_str())


		// XXX
		obj["instruments"] = instruments_block.clone()
	}

	// XXX
	mut orders_block := []json2.Any{}
	{
		for _, po in m.orders {
			v := int(po)
			orders_block << v
		}

		// XXX
		obj["orders"] = orders_block.clone()
	}

	// XXX
	mut patterns_block := []json2.Any{}
	{
		for i, pt in m.patterns {
			pt_obj := s.encode_pattern_json(pt, i)
			patterns_block << pt_obj
		}

		// XXX
		obj["patterns"] = patterns_block.clone()
	}

	println("")
	//println(json2.Any(obj).prettify_json_str())

	out_s := json2.Any(obj).prettify_json_str()
	//out_s := ""

	f.write_string(out_s) or {
		panic(err)
	}

	f.close()

	//panic(2)
}

// XXX help function to encode saved envelope data
pub fn (s &ModuleSaver) encode_envelope_json(e &Envelope) json2.Any {
	println("ModuleSaver encode_envelope_json e: (isnil) ${isnil(e)}")

	mut obj := map[string]json2.Any
	obj["enabled"] = e.enabled
	// Seems like 2d array of Any, but not quite ?
	obj["data"] = []json2.Any{}
	//obj["data"] = [][]json2.Any{len: 2, init: []json2.Any{len: 3}}
	//obj["data"] = [][]json2.Any{len: 1, init: []json2.Any{len: 1, init: 0}}
	//mut q := [][]json2.Any{len: 1, init: []json2.Any{len: 1, init: 0}}
	//mut q := [][]json2.Any{}

	/*
	//mut me := map[string]json2.Any{}
	mut me := map[string]json2.Any
	me['name'] = 'Bob'
	me['age'] = 18
	//me['x'] = q
	me['x'] = json2.Any(q)
	*/

	//println(q)
	//println(q.str())
	//panic(5)

	// XXX link / copy
	tmp := obj["data"] or {
		panic(error)
	}
	mut data_arr := tmp.arr()

	for d in e.data {
		mut v_obj := []json2.Any{}
		v_obj << d.x
		v_obj << d.y

		//obj["data"] << v_obj
		//mut x := obj["data"]
		//x << v_obj

		/*
		//obj["data"] << json2.Any(v_obj)
		mut z := obj["data"].arr()
		//mut z := obj["data"]

		z << v_obj

		println(z)
		*/

		data_arr << [v_obj]

		//panic(4)
	}

	println("data_arr:")
	println(data_arr)

	println("")

	// XXX replace/update data_arr
	obj["data"] = data_arr.clone()

	println(obj.str())
	println("")

	tmp2 := obj["data"] or {
		panic(error)
	}
	println(tmp2.str())

	//panic(5)

	return obj
}

// XXX help function to encode saved instrument gen data
pub fn (s &ModuleSaver) encode_gen_json(ig &InstrumentGen) json2.Any {
	println("ModuleSaver encode_gen_json ig: (isnil) ${isnil(ig)}")

	mut obj := map[string]json2.Any

	// XXX currently only this type
	obj["type"] = "square"
	obj["volume"] = ig.vol
	// XXX fixme
	obj["panning"] = 0
	obj["freq"] = ig.freq

	obj["envelope_volume"] = s.encode_envelope_json(ig.env_vol)
	obj["envelope_pitch"] = s.encode_envelope_json(ig.env_pitch)

	println(obj.str())

	//panic(2)

	return obj
}

// XXX save instrument to json data
pub fn (s &ModuleSaver) encode_instrument_json(inst &Instrument, inst_num_hint int) json2.Any {
	println("ModuleSaver endcode_instrument_json inst: (isnil) ${isnil(inst)}")

	// XXX TODO: handle default values/defaults ?

	mut obj := map[string]json2.Any
	//obj["env_vol"] = inst.env_vol
	//obj["env_pitch"] = inst.env_pitch

	// XXX this is only a hint, not required
	if inst_num_hint != -1 {
		obj["__num"] = inst_num_hint
	}
	obj["name"] = inst.name.clone()
	obj["volume"] = inst.vol
	obj["loop"] = inst.loop
	
	// XXX read base note?
	{
		base_note_str := inst.base_note.name

		obj["base_note"] = base_note_str.clone()
	}

	// XXX gens

	obj["num_gens"] = inst.gens.len

	//println(obj.str())

	// XXX
	mut gens_arr := []json2.Any{}

	for ig in inst.gens {
		println("->")
		ig_obj := s.encode_gen_json(ig)

		println("")
		//println(ig_obj.str())
		println("")

		gens_arr << ig_obj
	}

	//obj["gens"] = gens_arr
	obj["gens"] = gens_arr.clone()

	// XXX finally
	obj["envelope_volume"] = s.encode_envelope_json(inst.env_vol)
	obj["envelope_pitch"] = s.encode_envelope_json(inst.env_pitch)

	//println(json2.Any(obj).prettify_json_str())

	//panic(3)

	return obj
}

// XXX help function to encode pattern data
pub fn (s &ModuleSaver) encode_pattern_json(pt &Pattern, pt_num_hint int) json2.Any {
	println("ModuleSaver encode_pattren_json pt: (isnil) ${isnil(pt)}")

	mut obj := map[string]json2.Any
	
	// XXX this is only a hint, not required
	if pt_num_hint != -1 {
		obj["__num"] = pt_num_hint
	}
	obj["num_rows"] = pt.length

	// XXX
	mut tracks_arr := []json2.Any{}

	for i, tr in pt.tracks {
		println("-> tr: ${i}")

		mut tr_obj := map[string]json2.Any

		// XXX
		mut cells_arr := []json2.Any{}

		for j, c in tr.cells {
			println("-> c: ${j}")

			mut c_obj := []json2.Any{}

			// Note
			if c.has_note() {
				s_note := s.note_to_symbol(c.note.val)
				c_obj << s_note.clone()
			} else {
				c_obj << json2.null
			}

			// Instrument
			if c.inst != -1 {
				c_obj << c.inst
			} else {
				c_obj << json2.null
			}

			///*

			// Volume
			if c.vol != -1 {
				c_obj << c.vol
			} else {
				c_obj << json2.null
			}

			// Effect
			// XXX TODO
			if c.effect != -1 {
				c_obj << c.effect
			} else {
				c_obj << json2.null
			}

			//*/

			// XXX
			cells_arr << [c_obj]
		}

		tr_obj["cells"] = cells_arr.clone()

		// XXX
		tracks_arr << tr_obj
	}

	// XXX
	obj["tracks"] = tracks_arr.clone()
	
	//println(obj.str())
	println("")
	//println(json2.Any(obj).prettify_json_str())

	//panic(2)

	return obj
}

/*
// XXX
pub fn (s &ModuleSaver) save_instrument_json(inst &Instrument) json2.Any {
	println("ModuleSaver save_instrument_json inst: $inst")

	mut obj := map[string]json2.Any
	obj["enabled"] = e.enabled
}
*/

// XXX save instrument to file ?
pub fn (s &ModuleSaver) save_instrument(inst &Instrument, path string) {
	println("ModuleSaver save_instrument inst: (isnil) ${isnil(inst)}, path: ${path}")
	// XXX bug ?
	//println("ModuleSaver save_instrument path: {path}, inst: {inst}")
	

	mut f := os.create(path) or {
		panic(err)
	}

	// XXX root ?
	//mut obj := map[string]json2.Any{}

	mut obj := s.encode_instrument_json(inst, -1)

	println("")
	//println(json2.Any(obj).prettify_json_str())
	//println(obj.prettify_json_str())

	//out_s := json2.Any(obj).prettify_json_str()
	out_s := obj.prettify_json_str()

	f.write_string(out_s) or {
		panic(err)
	}

	f.close()

	panic(5)
}
