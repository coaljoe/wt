module player

// TODO add pattern resize/track resize functions
// TODO ? add pattern shrink/extend functions?

[heap]
pub struct Pattern {
pub mut:
	m &Module // Link
	length int
	//num_tracks int
	tracks []&Track
}

pub fn new_pattern(m &Module) &Pattern {
	mut p := &Pattern{
		m: m,
		length: c_default_pattern_length,
	}

	//p.init()
	
	return p
}

//pub fn new_pattern_ex(...)

pub fn (mut pt Pattern) init() {
	for i in 0..pt.m.num_tracks {
		_ = i
		t := new_track(pt, i)
		pt.tracks << t
	}
}

fn (pt &Pattern) num_tracks() int {
	return pt.tracks.len
}

fn (mut pt Pattern) add_track() {
	// ?
	ch_idx := pt.tracks.len
	
	t := new_track(pt, ch_idx)
	pt.tracks << t
}

// Resize pattern to a new length
fn (mut pt Pattern) resize(length int) {

}

fn (pt &Pattern) str() string {
	return "Pattern<{pt.length}>"
}
