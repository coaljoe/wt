module player

import math
import rand
import common as c

pub enum OscType {
	sine
	square
	noise_square
	noise_random
}

// XXX TODO:
// use integer 'int' instead of sine_t
// for phase/period management?
pub struct Gen {
pub mut:
	//inc int
	// Phase time in period [0,1]
	//phase_t f32
	// Sine period time (T) [0,1]
	sine_t f32
}

pub fn new_gen() &Gen {
	mut g := &Gen{
		sine_t: 0.0,
	}
	g.reset()
	
	return g
}

/*
pub fn (g &Gen) sine_gen(phase_t f32, freq f32, amp f32) f32 {
	//sine := amp * math.sin(2.0 * np.pi * (freq * t + phase_t))

	t := 1.0/f32(c_sr)
	sine := math.sin(2.0 * math.pi * freq * t + phase_t)

	return amp * sine
}
*/

/*
pub fn (g &Gen) sine_gen_cont(freq, amp f32, nsamples int) {
	sine := g.sine_gen(g.phase_t, freq, amp)
	//g.phase_t = math.modf(f * f32(nsamples)/f32(c_sr) + con)[0]
	return sine
}
*/

/*
pub fn (g &Gen) square_wave(freq f32, amp_ f32, nsamples int) []f32 {
	//return make_wave(.square, 0, freq, amp_, nsamples)
	data, _ := make_wave(.sine, 0, freq, amp_, nsamples)
	return data
}
*/

pub fn (mut g Gen) square_wave_cont(freq f32, amp_ f32, nsamples int) []f32 {
	//pp(2)
	//for i := 0; i < nsamples; i++
	//data := make_wave(.square, g.inc, freq, amp_, nsamples)
	//data := make_wave(.sine, g.inc, freq, amp_, nsamples)
	//data, phase_t := make_wave(.sine, g.phase_t, freq, amp_, nsamples)
	data, sine_t := make_wave(.square, g.sine_t, freq, amp_, nsamples)
	//g.inc += nsamples
	g.sine_t = sine_t
	//g.inc++
	return data
}

pub fn (mut g Gen) wave_cont(osc_type OscType, freq f32, amp_ f32, nsamples int) []f32 {
	data, sine_t := make_wave(osc_type, g.sine_t, freq, amp_, nsamples)
	//g.inc += nsamples
	g.sine_t = sine_t
	//g.inc++
	return data
}

pub fn (mut g Gen) wave_sweep_cont(osc_type OscType, freq0 f32, freq1 f32, amp_ f32, nsamples int) []f32 {
	data, sine_t := make_wave_sweep(osc_type, g.sine_t, freq0, freq1, amp_, nsamples)
	g.sine_t = sine_t

	return data
}

// XXX cont version
pub fn (mut g Gen) render_to_file(path string, osc_type OscType, freq f32, amp_ f32, nsamples int) {
	data := g.wave_cont(osc_type, freq, amp_, nsamples)

	c.dump_raw(path, data)
}

// Seconds version instead of samples
pub fn (mut g Gen) render_to_file_sec(path string, osc_type OscType, freq f32, amp_ f32, secs f32) {
	nsamples := seconds_to_samples(secs)

	g.render_to_file(path, osc_type, freq, amp_, nsamples)
}

pub fn (mut g Gen) reset() {
	//g.inc = 0
	g.sine_t = f32(0.0)
}

/*
//[inline]
pub fn sine_wave_gen(inc f32, freq f32, amp_ f32) f32 {
	//mut amp := amp_
	//if amp > 1.0 { amp = 1.0 }
	//if amp < 0.0 { amp = 0.0 }
	//v := amp_ * math.sin(2.0 * math.pi * freq * (f32(inc) / c_sr))
	//v := amp_ * math.sin(freq * (2.0 * math.pi) * f32(inc / c_sr))
	//v := amp_ * math.sin(freq * (2.0 * math.pi) * (f32(inc) / f32(c_sr)))

	//sine := math.sin(2.0 * math.pi * freq * (f32(inc) / c_sr))
	//sine := math.sin(2.0 * math.pi * freq * (f32(phase_t) / c_sr))
	sine := math.sin(2.0 * math.pi * freq * inc)

	return amp_ * sine
}

//[inline]
pub fn square_wave_gen(phase_t f32, freq f32, amp_ f32) f32 {
	vs := sine_wave_gen(phase_t, freq, amp_)
	if vs > 0.0 {
		return 1.0 * amp_
	} else if vs < 0.0 {
		return -1.0	* amp_
	} else {
		return 0.0
	}
}
*/

fn do_make_wave(osc_type OscType, sine_t f32, f0 f32, f1 f32, amp_ f32, nsamples int, sweep bool) ([]f32, f32) {
	println("do_make_wave ocs_type: ${osc_type}, sine_t: ${sine_t}, f0: ${f0}, f1: ${f1}, nsamples: ${nsamples}, sweep: ${sweep}")
	//println("do_make_wave")

	if nsamples == 0 {
		return []f32{}, 0.0
	}
	
	mut amp := amp_
	if amp > 1.0 { amp = 1.0 }
	if amp < 0.0 { amp = 0.0 }
	//println("amp: ${amp}")
	//mut ret := []f32{}
	mut ret := []f32{cap: nsamples}

	// XXX
	mut freq := f0

	if freq <= 0 {
		panic("freq can't be negative, freq: ${freq}")
	}

	// XXX sweep
	n_steps := nsamples
	interval := f32(n_steps) / c_sr

	// t delta
	t_d := f32(freq)/f32(c_sr)
	// phase/period time [0,1]
	mut t := sine_t

	//println("... t_d: $t_d")
	//println("... t: $t")
	
	for i := 0; i < nsamples; i++ {
		mut v := f32(0.0)
		//t := phase_t + f32(i)
		//t := f32(i) / f32(c_sr)

		//println("-> begin t: $t")

		// XXX sine value [-1.0,1.0] (?)
		mut sine_v := f32(0.0)
		mut square_v := f32(0.0)
		match osc_type {
			// XXX generate sine value for these gens
			//.sine, .square {
			.sine {
				if !sweep {
					//sine_v = math.sinf(2.0 * math.pi * t)
					sine_v = sine_wave_t(t)
					//v = sine_wave_gen(t_d, freq, amp)
					//v = square_wave_gen(inc + i, freq, amp)
					//eprintln("2")
				} else {
					delta := f32(i) / f32(n_steps)
					xt := interval * delta // XXX conflicts with "t"
					mut vz := 2.0 * math.pi * xt * (f0 + (f1 - f0) * delta / 2.0)

 					// optional
					for (vz > 2.0 * math.pi) {
						vz -= 2.0 * math.pi
					}

					//v = math.sin(vz)
					//v = vz
					sine_v = math.sinf(vz) // XXX
					
					//pp(3)
				}
			}
			.square {
				if !sweep {
					//square_v = xfast_square_(t * 2.0 * math.pi)
					//square_v = xfast_square_(t * math.pi/4)
					square_v = xfast_square_(t)
					//square_v = xfast_square_(t)
					//v = sine_wave_gen(t_d, freq, amp)
					//v = square_wave_gen(inc + i, freq, amp)
					//eprintln("2")
				} else {
					panic("sweep not supported")
				}
			}
			else {
				panic("error ?")
			}
		}
		
		match osc_type {
			.sine {
				v = sine_v
			}
			.square {
				//v = if t >= 0.5 { 1.0 } else { -1.0 } // XXX bug?: generates cosines?
				//v = if sine_v >= 0.0 { f32(1.0) } else { f32(-1.0) }
				
				//v = if square_v >= 0.0 { f32(1.0) } else { f32(-1.0) }
				v = square_v
				
				//v = square_wave_gen(t_d, freq, amp)
				//eprintln("3")
			}
			.noise_square {
				mut r1 := f32(rand.intn(255) or {}) / 255.0
				mut r2 := f32(rand.intn(255) or {}) / 255.0

				//r1 /= 20.0
				r1 /= 40.0
				//r1 /= 80.0
				r2 /= 2

				n_levels := 2.0
				r2 = f32(rand.intn(int(n_levels)) or {} / n_levels)
				
				//println("r1: $r1")
				v = if t >= r1 { r2 } else { -r2 }
				//v = if t >= r1 { 1.0 } else { -1.0 }
			}
			else {
				panic("error")
			}
		}
		t += t_d
		
		ret << v

		// 0..1 range
		if t >= 1.0 {
	  		t -= 1.0
		}

		//println("-> end t: $t")
	}

	//println("... t_final: $t")
	
	//println(ret)
	//println("derp")
	//pp(6)
	return ret, t
}

[inline]
pub fn make_wave(osc_type OscType, phase_t f32, freq f32, amp_ f32, nsamples int) ([]f32, f32) {
	//eprintln("make_wave: osc_type: $osc_type, phase_t nphase_t, freq $freq, amp_ $amp_,"
	//	+ " nsamples: $nsamples")
	return do_make_wave(osc_type, phase_t, freq, freq, amp_, nsamples, false)
}

[inline]
pub fn make_wave_sweep(osc_type OscType, phase_t f32, freq0 f32, freq1 f32, amp_ f32, nsamples int) ([]f32, f32) {
	return do_make_wave(osc_type, phase_t, freq0, freq1, amp_, nsamples, true)
}

[inline]
pub fn sine_wave(freq f32, amp_ f32, nsamples int) []f32 {
	data, _ := make_wave(.sine, 0, freq, amp_, nsamples)
	return data
}

[inline]
pub fn square_wave(freq f32, amp_ f32, nsamples int) []f32 {
	data, _ := make_wave(.square, 0, freq, amp_, nsamples)	
	return data
}

[inline]
pub fn silence(nsamples int) []f32 {
	//return [f32(0.0)].repeat(nsamples)
	return []f32{len: nsamples}
}

// source: ma_waveform_square_f32
//[inline]
//pub fn xfast_square(x f64) f32 {
// XXX accepts 't' (x-axis value) ?
[inline]
pub fn xfast_square_(x f32) f32 {
	// XXX range extra check
	if x < 0 || x > 1.0 {
		panic("bad range, x: {x}")
	}

	// XXX map 0..1 float range (normalize ?)
	//f := x - i64(x)
	//f := x - i32(x) // XXX ?
	f := x - int(x)
	//f := math.fmod(x, 1)
	//f := math.mod(x, 1)

	//println("f: ${f:.6f}")
	//dump(f)

	if f < 0.5 {
		return 1.0
	} else {
		return -1.0
	}
}

[inline]
pub fn xfast_square(x f32) f32 {
	return math.sinf(x)
}

// XXX t = period, phase (0, 1)
// XXX x-axis, sine_v
// XXX same as math.sinf(2.0 * math.pi * t)
[inline]
pub fn sine_wave_t(t f32) f32 {
	// XXX range extra check
	if t < 0 || t > 1.0 {
		panic("bad range, t: {t}")
	}
	
	// return sine_v
	return math.sinf(2.0 * math.pi * t)
}

