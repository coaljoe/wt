module player

[heap]
pub struct Track {
pub mut:
	// 0..N (int) Channel index to which track belong. fixme?
	channel_idx int
	cells []&Cell
}

// XXX set idx automatically?
pub fn new_track(pt &Pattern, channel_idx int) &Track {
	mut t := &Track{
		channel_idx: channel_idx,
		cells: [],
	}

	for i in 0..pt.length {
		_ = i
		c := new_cell()
		t.cells << c
	}
	
	return t
}
