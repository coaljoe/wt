module player

//import henrixounez.vpng
import sdl
import xdebug.image


fn test_envelope() {

	// XXX create test context
	mut ctx := new_context()

	// XXX create test module
	mut m := new_module(mut ctx, 4)
	//m.bpm = 100
	println("module.bpm: {m.bpm}")
	println("module.tick_size: {m.tick_size()}")
	//panic("2")

	//png_file := vpng.read("image.png") or { return }
	//vpng.write(png_file, "output.png")

	//mut im_ctx := image.init()
	mut im_ctx := image.get_ctx()

	im_w := 500
	im_h := 200

	//mut im := image.create_image(mut ctx, 64, 64)
	mut im := image.create_image(mut im_ctx, im_w, im_h)
	//im.fill(vsdl2.Color{255, 0, 0, 255})
	//im.fill(vsdl2.Color{127, 127, 127, 255})
	im.fill(sdl.Color{110, 110, 110, 255})
	

	// XXX
	mut e1 := new_envelope(ctx)

	//data := []EnvPoint{}
	data := [
		//p.EnvPoint{0, 0}, // p0 : 0
		//p.EnvPoint{1, 5}, // p1 : 0 -> 1
		//p.EnvPoint{2, 10}, // p2 : 1 -> 2
	
		//EnvPoint{0, 0}, // p0 : 0
		//EnvPoint{1, 5}, // p1 : 0 -> 1
		//EnvPoint{2, 10}, // p2 : 1 -> 2
		//p.EnvPoint{2, 5}, // p2 : 1 -> 2
		//p.EnvPoint{5, 5}, // p2 : 1 -> 2

		
		EnvPoint{0, 0}, // p0 : 0
		EnvPoint{10, 5}, // p1 : 0 -> 1
		EnvPoint{20, 10} // p2 : 1 -> 2

		/*
		EnvPoint{0, 5}, // p0 : 0
		EnvPoint{10, 5}, // p1 : 0 -> 1
		//EnvPoint{20, 5} // p2 : 1 -> 2
		EnvPoint{40, 5} // p2 : 1 -> 2
		*/
	]

	e1.set_data(data)

	println("e1.data: {e1.data}")

	mut x_scale := f32(1.0)
	mut y_scale := f32(1.0)

	//x_scale = 2
	x_scale = 10
	//x_scale = 20
	//x_scale = 100
	y_scale = 2

	println("draw...")

	draw_dot := fn(mut im image.Image, x int, y int, c sdl.Color, size int) {
		for i in 0..size {
			for j in 0..size {
				im.put_pixel(x+i, y+j, c)
			}
		}
	}
	
	for i in 0..e1.data.len - 1 {
		println("-> i: {i}")
	
		p1 := e1.data[i]
		p2 := e1.data[i+1]

		//p1 := int(x_scale * f32(e1.data[i]))
		//p2 := int(y_scale * f32(e1.data[i+1]))
	
		println("-> p1: {p1}")
		println("-> p2: {p2}")

		c := sdl.Color{0, 255, 0, 255}

		/*
		scale_point := fn(xp EnvPoint) (int, int) {
			return int(x_scale * f32(xp.x)),
				   int(y_scale * f32(xp.y))
		}
		*/

		

		sx1 := int(x_scale * f32(p1.x))
		sy1 := int(y_scale * f32(p1.y))
		sx2 := int(x_scale * f32(p2.x))
		sy2 := int(y_scale * f32(p2.y))

		println("{sx1}, {sy1}, {sx2}, {sy2}")
			
		//im.draw_line(p1.x, p1.y, p2.x, p2.y, c)
		im.draw_line(sx1, sy1, sx2, sy2, c)

		// XXX draw debug tick segments
		// ? like every 16 ticks = 1px ?

		//n := e1.tick_size
		n := int(p1.distance_to(p2))
		for j in 0..n {
			t := f32(j) / f32(n)
			zx, zy := p1.lerp_coords(p2, t)
			println("zx: {zx}, zy: {zy}")

			// XXX FIXME
			zsx := int(x_scale * zx)
			zsy := int(y_scale * zy)

			c3 := sdl.Color{255, 0, 255, 255}

			draw_dot(mut im, zsx, zsy, c3, 2)
		}


		// XXX draw points
		c2 := sdl.Color{0, 0, 127, 255}
		mut spx, mut spy := 0, 0
		spx, spy = p1.xy_scale(x_scale, y_scale)
		
		draw_dot(mut im, spx, spy, c2, 3)

		spx, spy = p2.xy_scale(x_scale, y_scale)
		
		draw_dot(mut im, spx, spy, c2, 3)
	}

	// XXX test point at

	c := sdl.Color{220, 0, 0, 255}
	//draw_dot(mut im, 10, 10, c)

	mut t := 0
	t = 1
	//t = 8
	//t = 15
	//t = 16
	//t = 31
	//t = 32

	t = 16 * 10
	//t = 16 * 9

	t = 16 * 20
	//t = 16 * 19
	t = 319
	//t = 320
	//t = 321
	//t = 1
	//t = 16 * 21
	t = 16 * 10
	t = 16 * 1
	t = 15
	t = 14
	t = 16
	t = 8

	//y := e1.get_value_at_time(t)
	x, y := e1.get_xy_value_at_time(t)
	println("y: {y}")
	println("x: {x}")
	//xv := int(t/e1.tick_size)
	//yv := int(y)
	xv := int(x)
	yv := int(y)
	xp := EnvPoint{xv, yv}
	println(xp)
	//panic("z")
	// XXX integer scale
	//spx, spy := xp.xy_scale(x_scale, y_scale)
	// XXX float scale (sub one tick)
	//spx := int(x_scale * x)
	//spy := int(y_scale * y)
	//spx, spy := e1.xy_scale(x, y, x_scale, y_scale)
	spx, spy := e1.xy_scale_int(x, y, x_scale, y_scale)
	
	draw_dot(mut im, spx, spy, c, 3)

	im.save("/tmp/out.png")
}
