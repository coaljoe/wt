module player

pub struct Channel {
pub mut:
	// 0..N (int)
	idx int
	// 1..N (decimal) // XXX rename to gui_num?
	num int
	mute bool

	//// Channel memory / state
	// Current instrument sample rate / pitch
	rate f32
	// Current instrument volume (?)
	vol int
	// Current/last instrument
	//last_inst &Instrument
	inst &Instrument
	
	//buffer *beep.Buffer
	//streamer beep.Streamer
	//streamer beep.StreamSeeker
	// Current instrument player
	inst_player &InstrumentPlayer
}

pub fn new_channel(idx int) &Channel {
	c := &Channel{
		idx: idx,
		num: idx+1, // XXX?
		rate: 1.0,
		inst: &Instrument(unsafe { nil })
		inst_player: &InstrumentPlayer(unsafe { nil })
	}

	return c
}

pub fn (c &Channel) playing() bool {
	return !isnil(c.inst_player)
}

// Reset memory / state
// XXX fixme?
pub fn (mut c Channel) reset_state() {
	//println("Channel reset_state")

	c.inst = &Instrument(unsafe { nil })
	// XXX?
	c.inst_player = &InstrumentPlayer(unsafe { nil })

	c.rate = 1.0
}
