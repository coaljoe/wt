module player

import debug { pp }
import common as c

pub struct InstrumentPlayer {
pub mut:
	i &Instrument

	/*
	cur_note   *Note
	cur_effect *Effect
	cur_sample *Sample
	cur_vol    int
	*/

	// Current note
	note &Note
	// Current vol
	vol int


	// XXX per play
	//gen_inc int
	gen_sine_t f32


	gen_players []&InstrumentGenPlayer

	// XXX test
	//gen_player &InstrumentGenPlayer


	// XXX (R) remember state
	// Env pos samples
	env_pos int

	// Playing position (in samples)
	//sample_pos int
	play_pos int
	//maxSamplePos int
	playing   bool
}

pub fn new_instrument_player(i &Instrument) &InstrumentPlayer {
	mut ip := &InstrumentPlayer{
		i: i,
		note: &Note(unsafe { nil }),
		//gen_player: &InstrumentGenPlayer(0),
	}

	//ip.gen_player = new_instrument_gen_player()
	//ip.gen_player = i.gens[0].new_player()

	ip.reset()

	return ip
}

pub fn (mut ip InstrumentPlayer) build() {
	println("InstrumentPlayer build")

	// XXX reset gen players
	ip.gen_players = []

	// XXX add instrument gents
	for g in ip.i.gens {
		pl:= g.new_player()
		ip.gen_players << pl
	}
}

// StreamerI like
pub fn (mut ip InstrumentPlayer) reset() {
	println("InstrumentPlayer reset")
	
	//ip.gen_inc = 0
	ip.gen_sine_t = 0

	// XXX
	//ip.gen.reset()

	ip.build()
}

// Get InstrumentGenPlayer for InstrumentGen
pub fn (ip &InstrumentPlayer) get_player_for_gen(ig &InstrumentGen) &InstrumentGenPlayer {
	println("InstrumentPlayer get_player_for_gen")

	///*
	// Find igp
	mut igp := &InstrumentGenPlayer(unsafe { nil })
	for x in ip.gen_players {
		// XXX compare pointers
		if x.ig == ig {
			//panic("3")
			igp = x
			break
		}
	}
	//*/
	
	if isnil(igp) {
		panic("igp not found")
	}

	return igp
}

// Get InstrumentGenPlayer for InstrumentGen
pub fn (ip &InstrumentPlayer) get_player_for_gen_num(num int) &InstrumentGenPlayer {
	println("InstrumentPlayer get_player_for_gen_num")

	ig := ip.i.gens[num]
	igp := ip.get_player_for_gen(ig)

	return igp
}



// Schedule a note for playing
pub fn (ip &InstrumentPlayer) play_note(note &Note) {
	println("InstrumentPlayer play_note: ${note}")

	
}

// StreamerI like
// Render continious buffer from start: pos with len: len (chunk)
pub fn (ip &InstrumentPlayer) render_buf_cont(pitch f32, len_samples int) []f32 {
	//println("InstrumentPlayer render_buf_cont")

	//return ip.render_buf_cont_ex(pitch, len_samples, false)
	return ip.render_buf_cont_ex(pitch, len_samples, true)
}

// StreamerI like
// Render continious buffer from start: pos with len: len (chunk)
pub fn (ip &InstrumentPlayer) render_buf_cont_ex(pitch f32, len_samples int,
		disable_envs bool) []f32 {
	//println("InstrumentPlayer render_buf_cont_ex pitch: {pitch}," +
	//	"len_samples: {lensamples}, disable_envs: {disable_envs}")

	// Standard render buf code

	length := len_samples
	
	if length <= 0 {
		println("bad length: ${length}")
		pp(2)
	}

	println("collect gen waves...")
	//mut ch_data := [][]f32
	mut g_data := map[string][]f32
	//for j, g in ip.gens {
	for j, _ in ip.i.gens {
		println("render gen: j: ${j}")
	
		// XXX not tested
		// XXX rename to ig
		mut g := ip.i.gens[j]
		_ = g

		//ch_data << ch.render_wave()
		//data := ch.render_wave()
		//data := g.render_wave(pitch, length)
		//data := g.render_wave(pitch, length, g.env_vol, g.env_pitch)
		//data := g.render_wave(pitch, length, false)


		//mut igp := new_instrument_gen_player(g)
		//data := igp.render_wave(pitch, length, false)

		mut igp := ip.get_player_for_gen(g)

		//mut igp := ip.gen_player
		//data := igp.render_wave(pitch, length, false)
		//data := igp.render_buf_cont(pitch, length, false)
		data := igp.render_buf_cont_ex(pitch, length, false,
			disable_envs)

		//println("igp cont_gen_inc: {igp.cont_gen_inc}")
		println("igp cont_gen_sine_t: ${igp.cont_gen_sine_t}")


		println("j: ${j} len data: ${data.len}")
		//println(data)
		//println("panic test")
		//panic("")
		//pp(2)
	
		// XXX apply envelope for data
	
			
		g_data[j.str()] = data
	
		if true {
			println("dumping...")
			// Dump extended channels
			c.dump_raw("/tmp/wt__debug_inst_g_${j}.raw", data)
		}
	
		//println("derp")
		//pp(3)
	}
	
	//println("size ch_data: {ch_data.size}")
	//pp(2)
	
	//println("mix length: {max_length} ({f32(max_length) / c_sr})")
	//println("mix...")
	// Mix
	//data_len := ch_data["0"].len // XXX fixme?
	data_len := length
	//ch_default_mix_vol := 0.2
	//ch_default_mix_vol := 0.8
	ch_default_mix_vol := 1.0
	mut data := []f32{}
	for j := 0; j < data_len; j++ {
		//ch_vol = ip.channels[i].vol
			
		mut v := 0.0
		//k := j.str()
		// Process channel data
		for k := 0; k < g_data.len; k++ {
			kk := k.str()
			//println("j: {j} kk: {kk} ch_data[kk].len = {ch_data[kk].len}")
	
			if g_data[kk].len <= j {
				continue
			}
				
			d := g_data[kk][j]
			//v += d //* ch_vol
			v += (d * ch_default_mix_vol)
		}
		//println("v: {v}")
		if j > 40 {
		//pp(4)
		}
		data << f32(v)
	}
	
	//println(data[0..100])
	//println(data)
	//pp(2)
		
	return data
}

/*
// Render continues buffer from start: pos with len: len (chunk)
pub fn (ip &InstrumentPlayer) render_buf_cont(pitch f32, pos int, len int) []f32 {
	env_pos := ....

	gen_pos := ....

	// standard render buf code	
}
*/

pub fn (ip &InstrumentPlayer) render_tick_buf() {

}
