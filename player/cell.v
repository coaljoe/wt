module player

import lib.zopt

[heap]
pub struct Cell {
pub mut:
	// XXX Optional (?Note)
	//note &Note
	note zopt.ZOption<Note> // (?Note)
	//note ?Note
	//instrument int
	// -1 - not set (null)
	inst int // (?int)
	// -1 - not set (null)
	vol int // (?int)
	//track_num int
	// -1 - not set (null)
	effect int // (?int)
}

pub fn new_cell() &Cell {
	c := &Cell{
		//note: &Note(0),
		//vol: c_max_vol,
		inst: -1,
		vol: -1,
		effect: -1,
	}
	return c
}

pub fn (c &Cell) has_note() bool {
	//ret := c.note or { return false }
	//ret := !isnil(c.note) // XXX FIXME use optional type
	ret := c.note.val_set
	return ret
}

pub fn (c &Cell) has_instrument() bool {
	return c.inst != -1
}
