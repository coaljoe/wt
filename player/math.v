module player

import math

// XXX move to math module?

[inline]
pub fn lerp(a f32, b f32, t f32) f32 {
	return (1-t)*a + t*b
}

[inline]
pub fn distance_between(ax f32, ay f32, bx f32, by f32) f32 {
	dx := bx - ax
	dy := by - ay
	dist := math.sqrt(dx*dx + dy*dy)

	return f32(dist)
}

[inline]
pub fn max<T>(a T, b T) T {
	if a > b {
		return a
	}
	return b
}

[inline]
pub fn min<T>(a T, b T) T {
	if a < b {
		return a
	}
}

// Map value from range [a1,a2] to [b1,b2]
[inline]
pub fn map_range<T>(a1 T, a2 T, b1 T, b2 T, s T) T {
	return b1 + ((s - a1) * (b2 - b1) / (a2 - a1))
}

[inline]
pub fn amplitude_to_db(amplitude f32) f32 {
	return f32(20.0 * math.log10(amplitude))
}

[inline]
pub fn db_to_amplitude(db f32) f32 {
	return math.powf(10.0, db / f32(20.0))
}
