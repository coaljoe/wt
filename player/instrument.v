module player

//import os
import common as c

[heap]
pub struct Instrument {
pub mut:
	// Channel -> blocks
	//blocks [][]InstrumentBlock
	name string
	vol int // ?
	panning int // ?
	loop bool
	// Base note (middle C by defauilt) (C-4)
	// This is used for transpose ?
	// XXX per instrument?
	base_note Note
	// XXX not needed, since the frequency can
	// be specified in freq (in samples-per-second?) ?
	// XXX per instrument?
	//tune int
	gens []&InstrumentGen
	//envelopes map[string]&Envelope
	env_vol &Envelope
	env_pitch &Envelope
module:
	ctx &Context
}

pub fn new_instrument(ctx &Context) &Instrument {
	mut i := &Instrument{
		base_note: get_note("C-4"),
		gens: [],
		//envelopes: map[string]&Envelope,
		env_vol: new_envelope(ctx),
		env_pitch: new_envelope(ctx),
		ctx: ctx,
	}
	
	return i
}

// XXX rename to add_new_gen?
pub fn (mut i Instrument) add_new_gen() &InstrumentGen {
	g := new_instrument_gen(i)
	i.gens << g

	// XXX TODO
	// notify InstrumentPlayer(s)?
	// for rebuilding their instrument players ?
	
	return g
}

// Create new instrument player
pub fn (i &Instrument) new_player() &InstrumentPlayer {
	println("Instrument new_player")

	ip := new_instrument_player(i)

	return ip
}


/*
pub fn (i &Instrument) render_base_wave(len_samples int) []f32 {
	return i.render_wave(1.0, len_samples)
}
*/
/*
pub fn (i &Instrument) render_buf_cont(pitch f32, pos int, len int) []f32 {
	env_pos :=
}
*/

pub fn (i &Instrument) render_wave(pitch f32, len_samples int) []f32 {
	println("Instrument render_wave pitch: ${pitch}, len_samples: ${len_samples}")

	ip := new_instrument_player(i)
	ret := ip.render_buf_cont(1.0, len_samples)

	// XXX
	//return []f32{}
	return ret
}

pub fn (i &Instrument) render_to_file(path string, len_samples int) {
	println("Instrument render_to_file path: ${path}, len_samples: ${len_samples}")

	data := i.render_wave(1.0, len_samples)

	c.dump_raw(path, data)

	/*
	mut f := os.create(path) or {
		println("can't create file: {path}")
		return
	}

	for v in data {
		f.write_bytes(v, 4)
	}

	f.close()
	*/
}
