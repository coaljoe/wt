module player

//import player.debug { pp }
//import main.debug { pp }
//import wt.debug { pp }
//import ../debug { pp }
import debug { pp }
import math
//import common as c

[heap]
pub struct InstrumentGenPlayer {
pub mut:
	ig &InstrumentGen

	// XXX per player / play
	gen &Gen

	// XXX per play
	//cont_gen_inc int
	cont_gen_sine_t f32


	// XXX per play (?)
	cont_env_vol_pos int
	cont_env_pitch_pos int


	// XXX ?
	cont_play_pos int


	// Playing position (in samples)
	//sample_pos int
	play_pos int
	//maxSamplePos int
	playing   bool
}

pub fn new_instrument_gen_player(ig &InstrumentGen) &InstrumentGenPlayer {
	igp := &InstrumentGenPlayer{
		ig: ig,
		gen: new_gen(),
	}

	return igp
}

// StreamerI like
pub fn (mut igp InstrumentGenPlayer) reset() {
	println("InstrumentGenPlayer reset")
	
	//igp.cont_gen_inc = 0
	igp.cont_gen_sine_t = 0

	// XXX
	//igp.gen.reset()

	igp.cont_play_pos = 0
}

/*
// Render wave of base pitch?
pub fn (mut igp InstrumentGenPlayer) render_base_wave(len_samples int) []f32 {
	println("InstrumentGenPlayer render_base_wave")
	
	data := igp.render_wave(1.0, len_samples, false)

	return data
}
*/

pub fn (mut igp InstrumentGenPlayer) render_buf_cont(pitch f32, len_samples int,
	disable_global_env bool) []f32 {
	println("InstrumentGenPlayer render_buf_cont pitch: ${pitch}," +
		"len_samples: ${len_samples}")

	return igp.render_buf_cont_ex(pitch, len_samples, disable_global_env, false)
}

pub fn (mut igp InstrumentGenPlayer) render_buf_cont_ex(pitch f32, len_samples int,
	disable_global_env bool, disable_envs bool) []f32 {
	//println("InstrumentGenPlayer render_buf_cont pitch: {pitch}," +
	//		"len_samples: {len_samples}, disable_global_env: {disable_global_env}," +
	//		"disable_envs: {disable_envs}")


	// XXX cont code

	println("cont_play_pos: ${igp.cont_play_pos}")
	defer {
		igp.cont_play_pos += len_samples
	}

	if igp.cont_play_pos > 0 {
		//panic("2")
	}


	// XXX get starting playing position
	pos := igp.cont_play_pos
	
	
	mut data := []f32{}

	//data = igp.gen_render_wave(pitch, len_samples)

	// XXX update gen

	//igp.gen.inc = igp.cont_gen_inc
	igp.gen.sine_t = igp.cont_gen_sine_t


	//mut disable_envs := false

	// XXX apply envelope for data
	{
		inst_vol_env := igp.ig.inst.env_vol
		vol_env := igp.ig.env_vol

		inst_pitch_env := igp.ig.inst.env_pitch
		pitch_env := igp.ig.env_pitch
		
		res_vol_env := igp.ig.join_envelopes(inst_vol_env, vol_env, false)
		res_pitch_env := igp.ig.join_envelopes(inst_pitch_env, pitch_env, true)
	
		println("res_vol_env: ${res_vol_env}")
		println("res_pitch_env: ${res_pitch_env}")

		println("res_pitch_env.data_range: ${res_pitch_env.data_range}")
		//pp(5)

		mut use_vol := false
		mut use_pitch := false

		if inst_vol_env.enabled() || vol_env.enabled() {
			use_vol = true
		}

		if inst_pitch_env.enabled() || pitch_env.enabled() {
			use_pitch = true
		}

		//if true && disable_envs {
		if disable_envs {
			println("warning: disabling envs...")
			
			use_vol = false
			use_pitch = false
		}

		//if inst_env.enabled() || env.enabled() {
		if true {

			//vol_env_data := mix_envelope_data(inst_env.data, env.data)
			//println("vol_env_data: {vol_env_data}")

			// XXX TODO render gen wave by blocks / envelope-blocks

			// XXX ???
			//mut time_samples := 0
			mut time_samples := pos
			// FIXME: use a fixed val/const for all env ticks?
			//step := inst_vol_env.tick_size
			//step := c_env_tick_size
			step := vol_env.tick_size()
			//count := 3
			//count := 10
			// Overflow / overlap
			count := int(math.ceil(f32(len_samples)/f32(step)))
			overflow := (count * step) - len_samples
			println("count: ${count}")
			println("overflow: ${overflow}")
			//panic("2")
			
			for i in 0..count {
				mut frame_size := step

				if i == count - 1 && overflow > 0 {
					println("last frame: i: ${i}; count: ${count}")
					// XXX
					//frame_size = overflow
					frame_size = step - overflow
					//panic("2")
				}
				//time_samples := step * i
				time_samples += frame_size

				println("count: ${count}, frame_size: ${frame_size}")
				println("time_samples: ${time_samples}")

				mut p_coeff := f32(1.0)

				// Pitch
				if use_pitch {
					ev := res_pitch_env.get_value_at_time(time_samples)
					println("ev: ${ev}")

					//base_note := get_note("C-4")
					//base_note := i.igp.base_note
					base_note := igp.ig.base_note
					base_freq := base_note.freq
					base_num := base_note.num

					println("base_note: ${base_note}")
					println("base_freq: ${base_freq}")
					println("base_num: ${base_num}")

					// Channel central value
					center_val := res_pitch_env.data_range/2
					//center_val := c_env_max_y / 2
					println("center_val: ${center_val}")

					// XXX difference from center (central value)
					ev_diff := (int(ev) - center_val)

					println("ev_diff: ${ev_diff}")
					//panic("2")

					note_num := base_num + ev_diff
					note_diff := ev_diff
					
					println("note_num: ${note_num}")

					if note_num >= 0 {
						note := get_note_by_num(note_num)
						//note_freq := note.freq
						println("-> note: ${note}")
						println("-> note.freq: ${note.freq}")
					}
					
					note_freq := note_diff_to_freq(base_freq, note_diff)
					println("note_freq: ${note_freq}")
					//panic("2")

					//println("note: {note}")

					rate := f32(note_freq) / f32(base_freq)

					println("rate: ${rate}")
					//panic("2")
					
					//p_coeff = f32(ev)/c_max_vol
					p_coeff = rate
					//pp(2)
				}

				mut v_coeff := f32(1.0)

				// Volume
				if use_vol {
					ev := res_vol_env.get_value_at_time(time_samples)
					println("ev: ${ev}")
					v_coeff = f32(ev) / f32(res_vol_env.data_range)
					//pp(2)
				}

				pitch_v := pitch * p_coeff
				println("pitch_v: ${pitch_v}")
				
				mut d := igp.gen_render_wave(pitch_v, frame_size)
				//println("d: {d}")
				
				// Proc d
				if use_vol {
					//d = d.map(it * v_coeff)
					for j, x in d {
						d[j] = x * v_coeff
					}
					//println("proc d: {d}")
				}
				
				data << d
				//panic("2")
			}

			//panic("2")
			//pp(3)

			//block_size := ?
			
		} else {
			println("vol envelopes are not enabled")
			pp(4)
		}

	}

	//println("z inc {igp.gen.inc}")
	println("z sine_t ${igp.gen.sine_t}")
	//igp.cont_gen_inc = igp.gen.inc
	//igp.cont_gen_sine_t = igp.gen.sine_t
	

	//println("blocks: {ic.blocks}")
	//println("len blocks: {ic.blocks.len}")
	//mut cur_length := 0
	//println(data[0..100])
	//println(data)
	//pp(5)
	for i, v in data {
		//println("v: {v}, ic.vol: {ic.vol}")
		data[i] = v * igp.ig.mix_vol()
	}
	//pp(4)
	//println(data)
	//pp(3)
	
	return data
}

/*
// Render wave of specified pitch
// ? generator's settings apply
// ? generator's envelopes apply?
// ? instrument's envelopes apply?
//   - whats the point of separate function from
//     instrument? if it applyes settings from instrument?
//   - where is this function be used?
//pub fn (mut ig InstrumentGen) render_wave(pitch f32, len_samples int) []f32 {
pub fn (mut igp InstrumentGenPlayer) render_wave(pitch f32, len_samples int,
	disable_global_env bool) []f32 {
//pub fn (mut ig InstrumentGen) render_wave(pitch f32, len_samples int,
//		env_vol &Envelope, env_pitch &Envelope) []f32 {
	println("InstrumentGenPlayer render_wave len_samples: ${len_samples}")
	
	mut data := []f32{}

	//data = igp.gen_render_wave(pitch, len_samples)

	// XXX update gen

	//igp.gen.inc = igp.cont_gen_inc
	igp.gen.sine_t = igp.cont_gen_sine_t


	mut disable_envs := false

	// XXX apply envelope for data
	{
		inst_vol_env := igp.ig.inst.env_vol
		vol_env := igp.ig.env_vol

		inst_pitch_env := igp.ig.inst.env_pitch
		pitch_env := igp.ig.env_pitch
		
		res_vol_env := igp.ig.join_envelopes(inst_vol_env, vol_env, false)
		res_pitch_env := igp.ig.join_envelopes(inst_pitch_env, pitch_env, true)
	
		println("res_vol_env: $res_vol_env")
		println("res_pitch_env: $res_pitch_env")

		println("res_pitch_env.data_range: $res_pitch_env.data_range")
		//pp(5)

		mut use_vol := false
		mut use_pitch := false

		if inst_vol_env.enabled() || vol_env.enabled() {
			use_vol = true
		}

		if inst_pitch_env.enabled() || pitch_env.enabled() {
			use_pitch = true
		}

		if disable_envs {
			println("warning: disabling envs...")
			
			use_vol = false
			use_pitch = false
		}

		//if inst_env.enabled() || env.enabled() {
		if true {

			//vol_env_data := mix_envelope_data(inst_env.data, env.data)
			//println("vol_env_data: $vol_env_data")

			// XXX TODO render gen wave by blocks / envelope-blocks

			// XXX ???
			mut time_samples := 0
			// FIXME: use a fixed val/const for all env ticks?
			//step := inst_vol_env.tick_size
			//step := c_env_tick_size
			step := vol_env.tick_size()
			//count := 3
			//count := 10
			// Overflow / overlap
			count := int(math.ceil(f32(len_samples)/f32(step)))
			overflow := (count * step) - len_samples
			println("count: $count")
			println("overflow: $overflow")
			//panic("2")
			
			for i in 0..count {
				mut frame_size := step

				if i == count - 1 && overflow > 0 {
					println("last frame: i: $i; count: $count")
					// XXX
					//frame_size = overflow
					frame_size = step - overflow
					//panic("2")
				}
				//time_samples := step * i
				time_samples += frame_size

				println("count: $count, frame_size: $frame_size")

				mut p_coeff := f32(1.0)

				// Pitch
				if use_pitch {
					ev := res_pitch_env.get_value_at_time(time_samples)
					println("ev: $ev")

					//base_note := get_note("C-4")
					//base_note := i.igp.base_note
					base_note := igp.ig.base_note
					base_freq := base_note.freq
					base_num := base_note.num

					println("base_note: $base_note")
					println("base_freq: $base_freq")
					println("base_num: $base_num")

					// Channel central value
					center_val := res_pitch_env.data_range/2
					//center_val := c_env_max_y / 2
					println("center_val: $center_val")

					// XXX difference from center (central value)
					ev_diff := (int(ev) - center_val)

					println("ev_diff: $ev_diff")
					//panic("2")

					note_num := base_num + ev_diff
					note_diff := ev_diff
					
					println("note_num: $note_num")

					if note_num >= 0 {
						note := get_note_by_num(note_num)
						//note_freq := note.freq
						println("-> note: $note")
						println("-> note.freq: $note.freq")
					}
					
					note_freq := note_diff_to_freq(base_freq, note_diff)
					println("note_freq: $note_freq")
					//panic("2")

					//println("note: $note")

					rate := f32(note_freq) / f32(base_freq)

					println("rate: $rate")
					//panic("2")
					
					//p_coeff = f32(ev)/c_max_vol
					p_coeff = rate
					//pp(2)
				}

				mut v_coeff := f32(1.0)

				// Volume
				if use_vol {
					ev := res_vol_env.get_value_at_time(time_samples)
					println("ev: $ev")
					v_coeff = f32(ev) / f32(res_vol_env.data_range)
					//pp(2)
				}

				pitch_v := pitch * p_coeff
				println("pitch_v: $pitch_v")
				
				mut d := igp.gen_render_wave(pitch_v, frame_size)
				println("d: $d")
				
				// Proc d
				if use_vol {
					//d = d.map(it * v_coeff)
					for j, x in d {
						d[j] = x * v_coeff
					}
					println("proc d: $d")
				}
				
				data << d
				//panic("2")
			}

			//panic("2")
			//pp(3)

			//block_size := ?
			
		} else {
			println("vol envelopes are not enabled")
			pp(4)
		}

	}

	//println("z inc $igp.gen.inc")
	println("z sine_t $igp.gen.sine_t")
	//igp.cont_gen_inc = igp.gen.inc
	//igp.cont_gen_sine_t = igp.gen.sine_t
	

	//println("blocks: ${ic.blocks}")
	//println("len blocks: ${ic.blocks.len}")
	//mut cur_length := 0
	//println(data[0..100])
	//println(data)
	//pp(5)
	for i, v in data {
		//println("v: ${v}, ic.vol: ${ic.vol}")
		data[i] = v * igp.ig.mix_vol()
	}
	//pp(4)
	//println(data)
	//pp(3)
	
	return data
}
*/

// Render generator's wave
// ? no envelopes and instrument generator setting's applied?
pub fn (mut igp InstrumentGenPlayer) gen_render_wave(pitch f32, len_samples int) []f32 {
	println("InsturmentGenPlayer gen_render_wave pitch: ${pitch:0f}, len_samples: ${len_samples}")
	
	mut data := []f32{}
	nsamples := len_samples
	//freq := ib.freq //440
	//freq := igp.freq * pitch //440
	freq_shift := igp.ig.freq_shift()
	mut freq := (igp.ig.base_note.freq + freq_shift) * pitch
	amp := igp.ig.mix_vol()

	println("igp.ig.base_note: ${igp.ig.base_note}")
	println("igp.ig.base_note.freq: ${igp.ig.base_note.freq}")

	println("freq_shift: ${freq_shift}")
	println("freq: ${freq}")
	println("amp: ${amp}")

	base_freq := igp.ig.freq
	base_note_freq_diff := igp.ig.base_note_freq_diff()

	println("base_freq: ${base_freq}")
	println("base_note_freq_diff: ${base_note_freq_diff}")

	new_freq := (base_freq + base_note_freq_diff) * pitch

	println("new_freq: ${new_freq}")

	freq = new_freq

	// XXX this won't allow instruments with 0 hz freq parameters ?
	if false {
		// XXX here we just clamp ?
		if freq < c_inst_min_play_freq {
			// 1.0 (hz)
			freq = c_inst_min_play_freq
		}
	}

	// XXX ? allows instruments with 000000 zeroes as hz parameter
	if freq < c_inst_min_play_freq {
		// XXX return silence / don't play
		
		// XXX if key diff (freq) is too low, it wont be heard at all ?
		// XXX not tested
		// XXX it will create gaps in playback / waveform ?
		// XXX the signal can dissapear if a pitch down effect is too low freq ?
		return silence(nsamples)
	}
	
	//pp(2)
	
	match igp.ig.typ {
		.square {
			//data = square_wave(freq, amp, nsamples)
			data = igp.gen.square_wave_cont(freq, amp, nsamples)
		}
		.noise_square {
			data = igp.gen.wave_cont(OscType.noise_square, freq, amp, nsamples)
		}
		//.silence {
		//	data = silence(nsamples)
		//}
		.test_sine {
			data = igp.gen.wave_cont(OscType.sine, freq, amp, nsamples)
		}
		else {
			panic("error, unknown typ")
		}
	}

	//println("x inc {igp.gen.inc}")
	println("x sine_t ${igp.gen.sine_t}")
	//igp.cont_gen_inc = igp.gen.inc
	igp.cont_gen_sine_t = igp.gen.sine_t


	return data
}

// Render generator's wave at specified gen's freq (instead of pitch)
pub fn (mut igp InstrumentGenPlayer) gen_render_wave_freq(freq f32, len_samples int) []f32 {
	println("InsturmentGenPlayer gen_render_wave_freq freq: ${freq}, len_samples: ${len_samples}")

	//rate := freq / igp.freq
	rate := freq / (igp.ig.base_note.freq + igp.ig.freq_shift())
	println("rate: ${rate}")
	
	return igp.gen_render_wave(rate, len_samples)
}
