module player

import math

/*
enum InterpType {
	xnone
	linear
}
*/

pub struct EnvPoint {
pub mut:
	x int
	y int
}

// Scale x coord by value
pub fn (ep EnvPoint) x_scale(x_scale f32) int {
	return int(x_scale * f32(ep.x))
}

// Scale y coord by value
pub fn (ep EnvPoint) y_scale(y_scale f32) int {
	return int(y_scale * f32(ep.y))
}

// Scale both coords by x- and y- scales
pub fn (ep EnvPoint) xy_scale(x_scale f32, y_scale f32) (int, int) {
	return ep.x_scale(x_scale), ep.y_scale(y_scale)
}

pub fn (ep EnvPoint) distance_to(b &EnvPoint) f32 {
	return distance_between(f32(ep.x), f32(ep.y), f32(b.x), f32(b.y))
}

// Return float a lerp tuple (technitally vec2f)
// XXX can't return float EnvPoint
pub fn (ep EnvPoint) lerp_coords(b EnvPoint, t f32) (f32, f32) {
	return lerp(f32(ep.x), f32(b.x), t),
			lerp(f32(ep.y), f32(b.y), t)
}

pub struct Envelope {
	// XXX Tick block size multiplier?
	//block_step int // block_size_step
	// XXX Tick/Frame size in samples,
	// env data resolution, hardcoded
	//tick_size int // Read-only
	// XXX max values
	max_x int
	max_y int
mut:
	//typ EnvelopeType
	// Tick length (samples * multiplier(?))
	// block_step multiplier
	//res int
	// ?
	//mag int
	//length int
//pub:
	// XXX sorted (by x) env points
	// XXX data is in (0..1) [0,64] range
	// XXX ?these are values like in interface (decimal integer with sign)
	// XXX volume values: (dec.) 0-64
	// XXX - stored: (int) 0-64 (0..0x40)
	// XXX panning values: (dec.) 0 center, -32 (max left), +32 (max right) (dec.);
	// XXX - stored: (int) 0 = max left, 32 = center, 64? (0x40?) = max right
	data []EnvPoint
	enabled bool
	//interp_type InterpType
pub mut:
	// Envelope data range (y-axis) resolution
	// it doesn't represent value range in .data
	// but works rather as a magnitude
	data_range int
	// Debug
	d_name string
module:
	ctx &Context
}

fn new_envelope(ctx &Context) &Envelope {
	e := &Envelope{
		//block_step: 16, // 16 samples
		//tick_size: 16, // 16 samples
		//tick_size: c_env_tick_size, // 16 samples
		max_x: c_max_env_x, // XXX max length?
		max_y: c_max_env_y, // XXX max height?
		//res: 16, // Every n-th sample block
		//res: 1, // Every n-th sample block
		//interp_type: .xnone,
		//mag: 1,
		//data_range: c_max_vol, // XXX default same as max volume
		data_range: c_env_default_data_range, // XXX default same as max volume
		ctx: ctx,
	}
	
	return e
}

// XXX shortcut
// Tick size in samples
fn (e &Envelope) tick_size() int {
	return e.ctx.m.tick_size()
}

/*
// XXX add/mix-in envelope data from another envelope
// and return it as array
fn (e &Envelope) mix_data(e2 &Envelope) []f32 {
	mut res_data := []f32{}

	for i, v2 in e2.data {
		v := e.data[i]
		rv := v + v2
		res_data << rv
	}

	return res_data
}
*/

pub fn (mut e Envelope) build() {
	println("Envelope build")

	// XXX sort points
	e.data = e.sorted_data()

	// XXX Verify
	println("verify data...")
	
	//have_map := map[EnvPoint]bool()
	mut have_map := map[int]bool

	for p in e.data {
		println("p: ${p}")

		x := p.x

		if !(x in have_map) {
			have_map[x] = true
		} else {
			panic("error: can't have two env points with the same x coord")
		}
	}

	//println("have_map")
	//println(have_map)

	//println("e.data:")
	//println(e.data)
	//panic("2")

	/*
	for p in e.data {
		println("p: $p")
		
		// XXX count same X values
		for z in e.data {
			println("z: $z")
			
			// XXX Same addr
			if &z == &p {
				println("skip, same addr")
				continue
			}
			if z.x == p.x {
				println(z == p)
				panic("error: can't have two env points with the same x coord")
			}
		}
	}
	*/
}

// check if envelope has points
pub fn (e &Envelope) empty() bool {
	return e.data.len == 0
}

pub fn (e &Envelope) data() []EnvPoint {
	return e.data
}

pub fn (mut e Envelope) set_data(d []EnvPoint) {
	println("Envelope set_data d: ${d}")

	e.clear_data()
	for p in d {
		e.add_point(p)
	}
}

// XXX get envelope data scale (y-axis) (from default value)
pub fn (e &Envelope) data_range_scale() f32 {
	return e.data_range / c_env_default_data_range
}

pub fn (mut e Envelope) clear_data() {
	e.data = []EnvPoint{}
}

// XXX get sorted data, from left to right. remove?
fn (e &Envelope) sorted_data() []EnvPoint {
	mut r := e.data.clone()

	r.sort(a.x < b.x)

	return r
}

pub fn (mut e Envelope) has_point(p EnvPoint) bool {
	println("Envelope has_point p: ${p}")

	for v in e.data {
		if v == p {
			return true
		}
	}

	return false
}

pub fn (mut e Envelope) add_point(p EnvPoint) {
	println("Envelope add_point p: ${p}")

	// XXX check first point?
	if e.size() == 0 {
		if p.x != 0 {
			panic("wrong envelope data: first point should be at zero time (x=0): p: ${p}")
		}
	}

	if e.has_point(p) {
		panic("already have point: ${p}")
	}

	e.data << p

	// XXX?
	e.build()
}

// Return right-most point
pub fn (e &Envelope) first_point() EnvPoint {
	return e.data.first()
}

// XXX return right-most point
pub fn (e &Envelope) last_point() EnvPoint {
	return e.data.last()
}

// Return index of the point, otherwise -1
pub fn (e &Envelope) idx_point(p &EnvPoint) int {
	for i, x in e.data {
		if x == p {
			return i
		}
	}

	return -1
}

pub fn (e &Envelope) size() int {
	return e.data.len
}

pub fn (mut e Envelope) set_size(v int) {
	e.data = []EnvPoint{len: v}
}

// Envelope length in ticks
pub fn (e &Envelope) length_ticks() int {
	return e.data.len
}

// Envelope length in samples
pub fn (e &Envelope) length_samples() int {
	return e.last_point().x * e.tick_size()
}

// XXX?
pub fn (e &Envelope) enabled() bool {
	return e.enabled
}

pub fn (mut e Envelope) enable() {
	e.enabled = true
}

pub fn (mut e Envelope) disable() {
	e.enabled = false
}

// Get envelope value (y) at given time (t) (x-axis), XXX sample version
pub fn (e &Envelope) get_value_at_time(time_samples_ int) f32 {
	println("Envelope get_value_at_time time_samples_: ${time_samples_}")

	if e.d_name != "" {
		println("d_name: ${e.d_name}")
	}

	mut time_samples := time_samples_

	if time_samples < 0 {
		panic("time samples is negative: ${time_samples}")
	}

	if time_samples > e.length_samples() {
		e_length := e.length_samples()
		println("warning: time_samples value: (${time_samples}) exceeds envelope length: (${e_length})")
		println("clamping time_samples value...")
		time_samples = e_length
		println("new_time_samples: ${time_samples}")
		//panic("2")
	}

	println("e.data: ${e.data}")

	// Time point (x) (in ticks)
	tpoint := time_samples / e.tick_size() // ?
	println("tpoint: ${tpoint}")


	// XXX fixme?
	mut buf := []EnvPoint{}

	println("points:")

	prev_point := e.data.filter(it.x <= tpoint).last()
	println("prev_point: ${prev_point}")
	
	buf = e.data.filter(it.x > tpoint)

	// XXX special case, prev_point == last_point
	// XXX fixme: optimize the code?
	if buf.len == 0 && prev_point == e.last_point() {
		// Just return end point value
		//return f32(prev_point.y)
		end_point := e.last_point()
		//pp(2)
		return f32(end_point.y)
	}

	next_point := if buf.len == 0 {
		panic("no next point")
		EnvPoint{}
	} else {
		buf.first()
	}

	println("next_point: ${next_point}")


	// Segment number
	seg_num := e.idx_point(prev_point)
	println("seg_num: ${seg_num}")


	// Segment length
	seg_length := next_point.x - prev_point.x
	println("seg_length: ${seg_length}")

	// Segment length samples
	seg_length_samples := seg_length * e.tick_size()
	println("seg_length_samples: ${seg_length_samples}")

	// (Segment) Time point (x) in samples,
	// since the beginning of the segment (tpoint)
	//seg_tpoint_samples := time_samples - (tpoint * e.tick_size)
	//seg_tpoint_samples := seg_length_samples - time_samples
	seg_tpoint_samples := time_samples - (prev_point.x * e.tick_size())
	println("seg_tpoint_samples: ${seg_tpoint_samples}")

	// (Segment) Time point (x) in t [0,1]
	//seg_tpoint_t := f32(seg_tpoint_samples) / f32(e.tick_size)
	seg_tpoint_t := f32(seg_tpoint_samples) / f32(seg_length_samples)
	println("seg_tpoint_t: ${seg_tpoint_t}")


	mut res := f32(0.0)

	// XXX geometric solution
	if false {
		// Find segment vector angle
		y_diff := next_point.y - prev_point.y
		println("y_diff: ${y_diff}")
		ar := f32(math.atan2(next_point.y, next_point.x))
		//ar := math.atan2(y_diff, next_point.x)
		a := math.degrees(ar)

		println("ar: ${ar}, a: ${a}")

		// Solve triangle
		// knowing the angle (a) and one side (adjacent) (x)

		// x
		//adj_side := 1
		//adj_side := 0.5
		//adj_side := seg_tpoint_t
		// XXX fixme?
		//adj_side := prev_point.x + (seg_length * seg_tpoint_t)
		adj_side := seg_length * seg_tpoint_t
		// y
		opp_side := adj_side * math.tanf(ar)

		println("adj_side: ${adj_side}")
		println("opp_side: ${opp_side}")


		//mut res := 0.
		//if ar != 0 {
		if y_diff == 0 {
			// If there's no angle (flat horizontal line)
			println("flat")
			res = prev_point.y
		} else {
			//res = opp_side
			res = prev_point.y + opp_side
		}
	
		println("res: ${res}")
	}
	
	//block_idx := (time_samples / (e.block_step * e.res))
	//println("block_idx: $block_idx")


	// XXX Lerp solution
	if true {
		// Ortho x-axis projection distance
		//axis_distance := next_point.x - prev_point.x
		axis_distance := f32(seg_length)
		println("axis_distance: ${axis_distance}")
		//pp(2)

		// Euclidian distance between points
		point_distance := prev_point.distance_to(next_point)
		println("point_distance: ${point_distance}")
		//pp(2)

		// Find distance scale amount [0,1]
		//scale := point_distance / axis_distance
		scale := axis_distance / point_distance
		println("scale: ${scale}")
		//pp(2)

		//v := e.data[block_idx]
		//mut t := f32(0.)
		mut t := f32(seg_tpoint_t * scale)
		println("t: ${t}")
		//v := lerp(prev_point.x, next_point.y, t)
		v := lerp(prev_point.y, next_point.y, t)
		//zx, zy := prev_point.lerp_coords(p2, t)
		println("v: ${v}")
		//pp(2)

		res = v

		println("res: ${res}")
	}

	//pp(2)

	return res
}

// Get envelope value (y) at given time (t), and clamped x value, XXX sample version
pub fn (e &Envelope) get_xy_value_at_time(time_samples_ int) (f32, f32) {
	println("Envelope get_xy_value_at_time time_samples_: ${time_samples_}")

	mut time_samples := time_samples_

	y := e.get_value_at_time(time_samples)

	// Clamp x
	// XXX TODO: use clamp function?
	if time_samples_ > e.length_samples() {
		time_samples = e.length_samples()
		//pp(2)
	}

	//y := e.get_value_at_time(time_samples)
	x := f32(time_samples) / f32(e.tick_size())

	return x, y
}

// Point scale helper function
pub fn (e &Envelope) xy_scale(x f32, y f32, x_scale f32, y_scale f32) (f32, f32) {
	println("Envelope xy_scale x: ${x}, y: ${y}, x_scale: ${x_scale}, y_scale: ${y_scale}")

	return x * x_scale, y * y_scale
}

pub fn (e &Envelope) xy_scale_int(x f32, y f32, x_scale f32, y_scale f32) (int, int) {
	println("Envelope xy_scale_int x: ${x}, y: ${y}, x_scale: ${x_scale}, y_scale: ${y_scale}")

	rx, ry := e.xy_scale(x, y, x_scale, y_scale)

	return int(rx), int(ry)
}

pub fn (e &Envelope) str() string {
	// Data as an array
	mut a := ""
	for i, p in e.data {
		a += "(" + p.x.str() + ", " + p.y.str() + ")"
		if i < e.data.len - 1 {
			a += ", "
		}
	}

	s := "Envelope<" + a + ">"

	return s
}

// XXX add/mix-in envelope data from another envelope
// and return it as array
// XXX array is always as long as the longest envelope
//pub fn mix_envelope_data(e1 &Envelope, e2 &Envelope) []f32 {
[inline]
pub fn mix_envelope_data(d1 []EnvPoint, d2 []EnvPoint) []EnvPoint {
	//println("mix_evenlope_data: e1: $e1, e2: $e2")
	//println("mix_evenlope_data d1: $d1, d2: $d2")
	println("mix_evenlope_data d1: [d1], d2: [d2]")

	//mut res_data := []EnvPoint{}
	mut res_data := []EnvPoint{cap: max(d1.len, d2.len)}

	long, short := if d1.len >= d2.len {
		d1, d2
	} else {
		d2, d1
	}

	// Add/mix data from the short
	for i, v1 in short {
		v2 := long[i]
		//rv := v1 + v2
		rv := EnvPoint{v1.x + v2.x, v1.y + v2.y}
		res_data << rv
	}

	// XXX Append the rest
	res_data << long[res_data.len..]

	return res_data
}
